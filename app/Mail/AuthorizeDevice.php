<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class AuthorizeDevice extends Mailable
{
    use Queueable, SerializesModels;

    protected $code;
    protected $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $code)
    {
        $this->code = $code;
        $this->email = Auth::user()->email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->email)
                    ->subject('Device Authorization')
                    ->view('mail.authorize-device')
                    ->with(['code' => $this->code]);
    }
}
