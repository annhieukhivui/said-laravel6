<?php 

namespace App\Services;

use Carbon\Carbon;
use Auth;
use Mail;
use App\Mail\AuthorizeDevice as AuthorizeDeviceMailable;

class DeviceAuthorization {

    const EMAIL_CODE_VALID_TIME = 15;
    const AUTHORIZED_CHECK_TIME = 15;

    private $user;
    private $deviceId = null;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function setDeviceId(?string $deviceId)
    {   
        if($deviceId) {
            $this->deviceId = $deviceId;
        }
    }

    public function isCurrentDeviceAuthorized()
    {
        return cache()->remember($this->userDeviceCacheKey(), now()->addMinutes(self::AUTHORIZED_CHECK_TIME), function(){
            return $this->user->isDeviceAuthorized($this->deviceId);
        });
    }

    public function userDeviceCacheKey() : string
    {
        $this->checkIfInstantiated();
        $deviceIdMD5 = md5($this->deviceId);
        return "user_{$this->user->id}_device_{$deviceIdMD5}";
    }

    public function getDeviceEmailCode() : string
    {
        $this->checkIfInstantiated();
        return cache()->remember('email_code_{$this->user->id}', now()->addMinutes(self::EMAIL_CODE_VALID_TIME), function(){
            return strtoupper(substr(str_shuffle(str_repeat("23456789abcdefghjkmnpqrstuvwxyz", 6)), 0, 6));
        });
    }

    public function generateDeviceEmailCode() : ?string
    {
        if(Auth::user()->canAddDevice()) {
            $this->checkIfInstantiated();
            return $this->cacheEmailCode();
        }
        return null;
    }

    private function sendEmail()
    {
        Mail::send(new AuthorizeDeviceMailable($this->getDeviceEmailCode()));
    }

    private function cacheEmailCode() : string
    {
        $emailCode = $this->getDeviceEmailCode();
        if(!cache()->has($emailCode)) {
            cache()->put($emailCode, $this->deviceId, now()->addMinutes(self::EMAIL_CODE_VALID_TIME));
            $this->sendEmail();
        }
        return $emailCode;
    }

    public function getCachedDeviceId($emailCode)
    {
        return cache()->get($emailCode);
    }

    public function authorizeEmailCode($emailCode)
    {
        if($deviceId = $this->getCachedDeviceId($emailCode)) {
            Auth::user()->device_relation()->firstOrCreate(['device_hash' => $deviceId]);
            $this->setDeviceId($deviceId);
            cache()->forget($this->userDeviceCacheKey());
            cache()->forget("email_code_{$this->user->id}");
        }
    }

    private function checkIfInstantiated()
    {
        if($this->deviceId === null || $this->user === null) {
            throw new \Exception('Device ID not set or no logged in user');
        }
    }
}