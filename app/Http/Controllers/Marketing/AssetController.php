<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Marketing\Asset;
use App\Models\Marketing\InventoryTransaction;

class AssetController extends Controller
{
	public function view($id){
		$asset = Asset::with([
        	'transaction_relation.action_relation',
        	'transaction_relation.out_to_relation',
        	'inventory_relation.product_relation',
            'transaction_relation.changes_relation.changes_detail_relation',
            'transaction_relation.changes_relation.user_relation'
        ])->find($id);

		$inventory_id = $asset->InventoryID;
		$serial = $asset->serial;
        $transactions = InventoryTransaction::with([
        	'action_relation',
        	'out_to_relation'
        ])->where([
        	['InventoryID', '=', $inventory_id],
            ['serial', '=', $serial],
        ])->orderBy('id', 'desc')->get();
		return view('marketing.asset.view', ['asset' => $asset, 'transactions' => $transactions]);
	}

   	public function getData(){
    	$data = Asset::with([
        	'transaction_relation',
        	'inventory_relation', 
        	'inventory_relation.product_relation'
        ])->where('active', '1')->orderBy('id', 'desc')->get();
        return $data;
    }
}
