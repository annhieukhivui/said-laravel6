<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Models\Marketing\TEWaitList;

// use App\Models\Marketing\TEProgram;
// use App\Models\Marketing\Inventory;

class WaitListController extends Controller
{
	public function getAdd(){
		$user = Auth::user()->firstname.' '.Auth::user()->lastname; 
		return view('marketing/program/waitlist/add', ['user' => $user]);
	}

	public function postAdd(Request $request){
    	$user = Auth::user()->id; 

        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'program_id' => 'required',
                'contact_id' => 'required',
                'owner' => 'required',
                'inventory_ids' => 'required',
            ],[
                'program_id.required' => 'Program field is required.',
                'contact_id.required' => 'Contact field is required.',
                'owner.required' => 'Owner field is required.',
                'inventory_ids.required' => 'Product field is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        $lastQueue = 0;
        $w = TEWaitList::where([['ProgramID', '=', $request->program_id], ['active', '=', '1']])->orderBy('queue', 'desc')->first();
        if($w){
        	$lastQueue = $w->queue;
        }

        $waitlist = new TEWaitList;
        $waitlist->ProgramID 	= $request->program_id;
        $waitlist->ContactID 	= $request->contact_id;
        $waitlist->owner 		= $request->owner;
        $waitlist->quantity 	= $request->quantity;
        $waitlist->term 		= $request->term;
        $waitlist->queue 		= $lastQueue+1;
        $waitlist->status 		= 207;
        $waitlist->note 		= $request->note;
        $waitlist->uid_created 	= $user;
        $waitlist->uid_modified = $user;
        $waitlist->save();

        $waitlist->waitlist_inventory_relation()->sync(explode(',', $request->inventory_ids));
        $lastID = $waitlist->id;
        return "Waitlist Added.";
    }

    public function getUpdate($id){
		$data = TEWaitList::with([
        	'waitlist_inventory_relation',
        	'waitlist_inventory_relation.product_relation',
        	'program_relation',
        	'contact_relation'
        ])->find($id);
		return view('marketing/program/waitlist/update', ['waitlist' => $data]);
	}

	public function postUpdate(Request $request, $id){
		$user = Auth::user()->id; 
        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'program_id' => 'required',
                'contact_id' => 'required',
                'owner' => 'required',
                'inventory_ids' => 'required',
            ],[
                'program_id.required' => 'Program field is required.',
                'contact_id.required' => 'Contact field is required.',
                'owner.required' => 'Owner field is required.',
                'inventory_ids.required' => 'Product field is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

       	$waitlist = TEWaitList::find($id);
        $waitlist->ProgramID 	= $request->program_id;
        $waitlist->ContactID 	= $request->contact_id;
        $waitlist->owner 		= $request->owner;
        $waitlist->quantity 	= $request->quantity == "null" ? NULL : $request->quantity;
        $waitlist->term 		= $request->term == "null" ? NULL : $request->term;
        $waitlist->note 		= $request->note == "null" ? NULL : $request->note;
        $waitlist->uid_modified = $request->user;
        $waitlist->save();
        $waitlist->waitlist_inventory_relation()->sync(explode(',', $request->inventory_ids));
        return "Waitlist Updated.";
	}

	public function delete($id){
		$user = Auth::user()->id;
        $waitlist = TEWaitList::find($id);

        // update queue
        $queue = $waitlist->queue;
        $program = $waitlist->ProgramID;
        $this->updateQueue($program, $queue);

        // delete relationship
        $waitlist->waitlist_inventory_relation()->detach();

        // delete waitlist
        TEWaitList::destroy($id);
        return "Deleted.";   
	}

	public function updateQueue($program, $queue){
		$waitlists = TEWaitList::where([
			['ProgramID', '=', $program],
			['queue', '>', $queue]
		])->get();
		if($queue > 0){
			foreach ($waitlists as $w) {
				$waitlist = TEWaitList::find($w->id);
				$waitlist->queue = $waitlist->queue - 1;
				$waitlist->save();
			}
		}
	}

	public function getData(){
    	$data = TEWaitList::with([
        	'waitlist_inventory_relation',
        	'contact_relation', 
        	'contact_relation.general_contact_relation'
        ])->where('active', '1')->orderBy('id', 'desc')->get();
        return $data;
    }

    public function postData(Request $request){
    	$data = TEWaitList::with([
        	'waitlist_inventory_relation',
        	'waitlist_inventory_relation.product_relation',
        	'contact_relation', 
        	'contact_relation.general_contact_relation', 
        	'status_relation'
        ])->where([['active', '=', '1'], ['ProgramID', '=', $request->ProgramID]])->orderBy('queue')->get();
        return $data;
    }

    public function getUpdateQueue($id){
        $data = TEWaitList::with([
            'contact_relation',
            'contact_relation.general_contact_relation'
        ])->where([
            ['active', '=', '1'],
            ['ProgramID', '=', $id],
            ['status', '=', 207]
        ])->orderBy('queue')->get();
        return view('marketing/program/waitlist/update-queue', ['waitlists' => $data, 'program_id' => $id]);
    }

    public function postUpdateQueue(Request $request, $id){
        $count = 1;
        foreach (json_decode($request->waitlistArr) as $waitlist) {
            $wait = TEWaitList::find($waitlist->id);
            $wait->queue = $count;
            $wait->save();
            $count++;
        }
        return "Updated.";
    }



    // public function test(){
    //     $data = TEWaitList::select(['id', 'ProductID'])->get();
    //     foreach ($data as $wait) {
    //         $waitlist = TEWaitList::find($wait->id);
    //         $program_id = $waitlist->ProgramID;
    //         // $program_id = 3;
    //         // $inventory_ids = TEProgram::select('value')->where('pid', '=', $program_id)->get();

    //         // echo gettype($waitlist->ProductID);
    //         if (strpos($waitlist->ProductID, 'any') !== false){
    //             // foreach($inventory_ids as $inventory){
    //             //     // echo $inventory;
    //             //     if($inventory){
    //             //         $waitlist->waitlist_inventory_relation()->sync($inventory);
    //             //     }
                    
    //             // }
    //             // if($inventory_ids){
    //             //     $waitlist->waitlist_inventory_relation()->sync($inventory_ids);
    //             // }
                
    //         }
    //         else{
    //             $product_ids = explode(',', $wait->ProductID);
    //             foreach ($product_ids as $product) {
    //                 $inventory_id = Inventory::select('id')->where('ProductID', '=', $product)->first();
    //                 $waitlist->waitlist_inventory_relation()->sync($inventory_id);
    //             }
    //         }


    //         // echo $wait->id.'<br>';
    //         // echo $wait->ProductID;
    //     }
    //     // $waitlist->waitlist_inventory_relation()->sync(explode(',', $request->inventory_ids));
    //     // $lastID = $waitlist->id;
    //     // return "Waitlist Added.";
    //     // return $data;
    // }
}
