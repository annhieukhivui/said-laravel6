<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Models\Marketing\TEProgram;
use App\Models\Marketing\Inventory;
use App\Models\Marketing\TEWaitList;

class TEProgramController extends Controller
{
    public function index() {
    	$data = TEProgram::with([
    		'waitinglist_relation',
    		'child_relation',
    		'child_relation.inventory_relation',
    		'child_relation.inventory_relation.product_relation'
    	])->where([
    		['active', '1'],
    		['pid', '0']
    	])->orderBy('id', 'desc')->get();
    	return view('marketing.program.index', ['data' => $data]);
    }

    public function getAdd() {
    	return view('marketing.program.add');
    }

    public function postAdd(Request $request){
    	$user = Auth::user()->id; 

        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'program_name' => 'required|unique:te_program,value',
                'inventory_ids' => 'required',
            ],[
                'program_name.required' => 'Program Name field is required.',
                'program_name.unique' => 'Program Name has already been taken.',
                'inventory_ids.required' => 'Product field is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }
        
        $program = new TEProgram;
        $program->value = $request->program_name;
        $program->pid = 0;
        $program->uid_created = $user;
        $program->uid_modified = $user;
        $program->save();
        $lastID = $program->id;
        foreach(explode(',', $request->inventory_ids) as $inventory_id){
        	$program = new TEProgram;
	        $program->value = $inventory_id;
	        $program->pid = $lastID;
	        $program->uid_created = $user;
	        $program->uid_modified = $user;
	        $program->save();

	        // update in_program field in inventory table
	        $this->inventory_update_program($inventory_id, $lastID);

        }
        return "Program Added.";

	}

	public function getUpdate($id) {
		$program = TEProgram::with([
    		'child_relation',
    		'child_relation.inventory_relation',
    		'child_relation.inventory_relation.product_relation'
    	])->find($id);
    	return view('marketing.program.update', ['program' => $program]);
    }

    public function postUpdate(Request $request, $id){

        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'program_name' => 'required|unique:te_program,value,'.$id,
                'inventory_ids' => 'required',
            ],[
                'program_name.required' => 'Program Name field is required.',
                'program_name.unique' => 'Program Name has already been taken.',
                'inventory_ids.required' => 'Product field is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        $user = Auth::user()->id; 

        $childrenArr = json_decode(json_encode(TEProgram::select('value')->where('pid', $id)->get()->map(function($child) {return $child->value; })), true);
        $newArr = array_map('intval', explode(',', $request->inventory_ids));
	    $lostArr = array_diff($childrenArr,$newArr);

	    foreach($newArr as $value){
	    	if(!TEProgram::where([['pid', '=', $id],['value', '=', $value],])->exists()){
	    		$newProgram = new TEProgram;
	    		$newProgram->value = $value;
	    		$newProgram->pid = $id;
	    		$newProgram->uid_created = $user;
	    		$newProgram->uid_modified = $user;
	    		$newProgram->save();
	    	}

	    	// update in_program field in inventory table
	    	$this->inventory_update_program($value, $id);
	    }
	    foreach ($lostArr as $value) {
	    	$oldProgram = TEProgram::where([['pid', '=', $id],['value', '=', $value]])->first();
	    	$oldProgram->delete();

	    	// set an inventory is not in_program 
	    	$this->inventory_update_program($value, 0);
	    }

	    return "Program Updated.";

	}

	public function view($id){
    	$program = TEProgram::with([
    		'waitinglist_relation',
        	'child_relation',
    		'child_relation.inventory_relation',
    		'child_relation.inventory_relation.product_relation'
        ])->find($id);
    	return view('marketing.program.view', ['program' => $program]);
    }

	public function delete($id){
		$inventory_ids = TEProgram::select('value')->where('pid', $id)->get()->map(function($child) {return $child->value; });
		foreach($inventory_ids as $inventory){
			$this->inventory_update_program($inventory, 0);
		}
        TEProgram::destroy($id);
        TEProgram::where('pid', $id)->delete();

        $waitlist_ids = TEWaitList::select('id')->where('ProgramID', $id)->get()->map(function($child) {return $child->id; });
        foreach($waitlist_ids as $wait_id){
            $waitlist = TEWaitList::find($wait_id);
            $waitlist->waitlist_inventory_relation()->detach();
        }
        TEWaitList::where('ProgramID', $id)->delete();
        return "Deleted.";
	}

    public function getData() {
        return TEProgram::where([
            ['active', '1'],
            ['pid', '0']
        ])->orderBy('id', 'desc')->get();
    }

    public function postData(Request $request) {
        return TEProgram::with([
            'inventory_relation',
            'inventory_relation.product_relation'
        ])->where([
            ['active', '1'],
            ['pid', $request->program]
        ])->orderBy('id', 'desc')->get();
    }

	public function inventory_update_program($id, $in_program){
		$inventory = Inventory::find($id);
        $inventory->in_program = $in_program;
        $inventory->save();
	}

}
