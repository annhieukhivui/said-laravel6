<?php

namespace App\Http\Controllers\Marketing;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests\TradeshowOccurrenceRequest;
use App\Http\Controllers\Controller;

use App\Models\Marketing\TradeshowOccurrence;
use App\Models\Marketing\TradeshowOccurrenceDistribution;
use App\Models\Shared\Document;
use App\Models\Shared\Note;

class TradeshowOccurrenceController extends Controller
{
    public function index() {
        return view('marketing.occurrence.index');
    }   

    public function getAdd(){
        return view('marketing.occurrence.form');
    }

    public function postAdd(TradeshowOccurrenceRequest $request){

        $user = Auth::user()->id;

        $occurrence = new TradeshowOccurrence;
        $occurrence->show_id                = $request->show_id;
        $occurrence->identifier             = $request->identifier;
        $occurrence->location               = $request->location;
        $occurrence->startdate              = $request->startdate;
        $occurrence->enddate                = $request->enddate;
        $occurrence->status                 = $request->status;
        $occurrence->address                = $request->address;
        $occurrence->bluemine               = $request->bluemine;
        $occurrence->website                = $request->website;
        $occurrence->booth_number           = $request->booth;
        $occurrence->labor                  = $request->labor;
        $occurrence->exibitor_resources     = $request->exibitor_resources;
        $occurrence->login_user             = $request->login_user;
        $occurrence->login_pass             = $request->login_password;
        $occurrence->poc_sales_lead         = $request->poc_sales_lead;
        $occurrence->poc_mktg_lead          = $request->poc_mktg_lead;
        $occurrence->poc_show_rep_name      = $request->poc_show_rep_name;
        $occurrence->poc_show_rep_number    = $request->poc_show_rep_number;
        $occurrence->poc_show_rep_email     = $request->poc_show_rep_email;
        $occurrence->sales                  = $request->post_sales;
        $occurrence->roi                    = $request->post_roi;
        $occurrence->meetings               = $request->post_meetings;
        $occurrence->leads                  = $request->post_leads;
        $occurrence->photo_album            = $request->post_album;
        $occurrence->uid_created            = $user;
        $occurrence->uid_modified           = $user;

        $occurrence->save();
        $lastOccurrenceID = $occurrence->id;

        /************brand distribution*************/
        if(count(json_decode($request->brand_id)) > 0){
            foreach (json_decode($request->brand_id) as $brand) {
                $distribution = new TradeshowOccurrenceDistribution;
                $distribution->occurrence_id    = $lastOccurrenceID;
                $distribution->brand_id         = $brand->id;
                $distribution->percent          = $brand->percent;
                $distribution->save();
            }
        }
        if($request->is100 != 0){
            $distribution = new TradeshowOccurrenceDistribution;
            $distribution->occurrence_id = $lastOccurrenceID;
            $distribution->brand_id = 1;
            $distribution->percent = $request->is100;
            $distribution->save();
        }

        /************upload documents*************/
        if(!empty($request->file('occurrence_document'))){
            foreach ($request->file('occurrence_document') as $file) {
                $name = $file->getClientOriginalName();
                $file_name = 'occurrence-'.$lastOccurrenceID.'-'.$name;
                if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                    $document = new Document;
                    $document->table_name = 123;
                    $document->document_name = $file_name;
                    $document->table_id = $lastOccurrenceID;
                    $document->save();
                }
            }
        }
        
        /************create bluemine tasks*************/
        // if($request->generate_bluemine == 'true'){
        //     $this->generate_bluemine($request->bluemine_project_link, $request->identifier);
        // }

        // /************update bluemine after creation*************/
        // $update_bm_sql = "UPDATE TradeShow_Occurrence SET bluemine=? WHERE id=?";
        // $update_bm_stmt = $dbconnect->prepare($update_bm_sql);
        // $update_bm_stmt->execute([$initial_bm, $occurrenceID]);
                
        return "Occurrence Added.";
        
    }

    public function getUpdate($id){
        $occurrence = TradeshowOccurrence::with([
            'tradeshow_relation', 
            'occurrence_status_relation',
            'document_relation',
            'note_relation',
            'note_relation.action_relation',
            'note_relation.user_relation',
            'occurrence_distribution_relation', 
            'occurrence_distribution_relation.distribution_brand_relation'
        ])->find($id);
        return view('marketing.occurrence.form', ['occurrence' => $occurrence]);
    }

    public function postUpdate(TradeshowOccurrenceRequest $request, $id){

        $user = Auth::user()->id;
        $occurrence = TradeshowOccurrence::find($id);
        $occurrence->show_id                = $request->show_id;
        $occurrence->identifier             = $request->identifier;
        $occurrence->location               = $request->location == "null" ? NULL : $request->location;
        $occurrence->startdate              = $request->startdate;
        $occurrence->enddate                = $request->enddate;
        $occurrence->status                 = $request->status == "null" ? NULL : $request->status;
        $occurrence->address                = $request->address == "null" ? NULL : $request->address;
        $occurrence->bluemine               = $request->bluemine == "null" ? NULL : $request->bluemine;
        $occurrence->website                = $request->website == "null" ? NULL : $request->website;
        $occurrence->booth_number           = $request->booth == "null" ? NULL : $request->booth;
        $occurrence->labor                  = $request->labor == "null" ? NULL : $request->labor;
        $occurrence->exibitor_resources     = $request->exibitor_resources == "null" ? NULL : $request->exibitor_resources;
        $occurrence->login_user             = $request->login_user == "null" ? NULL : $request->login_user;
        $occurrence->login_pass             = $request->login_password == "null" ? NULL : $request->login_password;
        $occurrence->poc_sales_lead         = $request->poc_sales_lead == "null" ? NULL : $request->poc_sales_lead;
        $occurrence->poc_mktg_lead          = $request->poc_mktg_lead == "null" ? NULL : $request->poc_mktg_lead;
        $occurrence->poc_show_rep_name      = $request->poc_show_rep_name == "null" ? NULL : $request->poc_show_rep_name;
        $occurrence->poc_show_rep_number    = $request->poc_show_rep_number == "null" ? NULL : $request->poc_show_rep_number;
        $occurrence->poc_show_rep_email     = $request->poc_show_rep_email == "null" ? NULL : $request->poc_show_rep_email;
        $occurrence->sales                  = $request->post_sales == "null" ? NULL : $request->post_sales;
        $occurrence->roi                    = $request->post_roi == "null" ? NULL : $request->post_roi;
        $occurrence->meetings               = $request->post_meetings == "null" ? NULL : $request->post_meetings;
        $occurrence->leads                  = $request->post_leads == "null" ? NULL : $request->post_leads;
        $occurrence->photo_album            = $request->post_album == "null" ? NULL : $request->post_album;
        $occurrence->uid_modified           = $user;
        $occurrence->save();

        /************brand distribution*************/
        TradeshowOccurrenceDistribution::where('occurrence_id', $id)->delete();
        if(count(json_decode($request->brand_id)) > 0){
            foreach (json_decode($request->brand_id) as $brand) {
                $distribution = new TradeshowOccurrenceDistribution;
                $distribution->occurrence_id    = $id;
                $distribution->brand_id         = $brand->id;
                $distribution->percent          = $brand->percent;
                $distribution->save();
            }
        }            
        if($request->is100 != 0){
            $distribution = new TradeshowOccurrenceDistribution;
            $distribution->occurrence_id = $id;
            $distribution->brand_id = 1;
            $distribution->percent = $request->is100;
            $distribution->save();
        }

        /************upload documents*************/
        if(!empty($request->file('occurrence_document'))){
            foreach ($request->file('occurrence_document') as $file) {
                $name = $file->getClientOriginalName();
                $file_name = 'occurrence-'.$id.'-'.$name;
                if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                    $document = new Document;
                    $document->table_name = 123;
                    $document->document_name = $file_name;
                    $document->table_id = $id;
                    $document->save();
                }
            }
        }

        /************add note*************/
        if($request->note != ''){
            $note = new Note;
            $note->note_on_table = 123;
            $note->note_on_table_id = $id;
            $note->action = 72; // 72 is general note
            $note->detail = $request->note;
            $note->uid_created = $user;
            $note->save();
        }

        return "Occurrence Updated.";

    }

    public function view($id){
        $occurrence = TradeshowOccurrence::with([
            'tradeshow_relation', 
            'occurrence_status_relation',
            'document_relation',
            'note_relation',
            'note_relation.action_relation',
            'note_relation.user_relation',
            'occurrence_distribution_relation', 
            'occurrence_distribution_relation.distribution_brand_relation'
        ])->find($id);
        return view('marketing.occurrence.view', ['occurrence' => $occurrence]);
    }

    public function delete($id){
        $user = Auth::user()->id;
        $occurrence = TradeshowOccurrence::find($id);
        $occurrence->active = 0;
        $occurrence->uid_modified = $user;
        if($occurrence->save()){
            return "Deleted.";
        }
        else{
            return "Failed.";
        }    
    }

    public function getData(){
        return TradeshowOccurrence::with(['tradeshow_relation', 'occurrence_status_relation'])->where('active', '1')->orderBy('id', 'desc')->get();
    }


    public function generate_bluemine($project_link, $identifier){
        $project_url = $project_link.'.json';
        $project_obj = json_decode($this->get_json_of_url($project_url));
        $project_id = $project_obj->project->id;
        $project_identifier = $project_obj->project->identifier;
        
        /******************** reusable data **************************/
        $assign_to = 136; //autobot 158
        $show_identifier = $identifier;
        $create_issue_url = "http://10.0.1.25/issues.json";
        $get_new_bm_url = "http://10.0.1.25/projects/".$project_identifier."/issues.json?author_id=158";

        /******************* task - initial template copy setup ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Initial Template Copy Setup - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d");
        $data['description'] = "
        This is the task to do the initial setup and control the start dates of all of the tasks.
        Rename all tasks with the tradeshow appended to the task
        ensure everything is assigned
        Do initial project overview setup to include any known sub-tasks for primary tasks, setting show date tasks date on the date the show is running, etc...
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $initial_bm = $this->get_newest_issue_id($get_new_bm_url);

        /******************* task - booth and show materials ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Booth and Show Materials - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['status_id'] = 7; // parent task = 7 - default 1
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $booth_show_material_bm = $this->get_newest_issue_id($get_new_bm_url);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $initial_bm; // bluemine ID to be added relation
        $relation_data['relation_type'] = 'follows'; // change to match type of relation [relates, precedes, follows, ...]
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$booth_show_material_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);

        /******************* task - booth and show materials - book booth ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Book Booth - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+2 days"));
        $data['description'] = "
        Reserve and pay for the booth for the show year. Booth reservation may have already been previously made. Ensure related tasks are tagged and Document all booth reservation related items to this task to include invoices, e-mails, logins, contacts.. etc..
        This due date and start date is movable and task is to be used as a followup until the next year's tasks are created and will carry on for the next year's show if we continue.

        Append task title for status. For example Book Booth - Reserve, Book booth - Next payment... etc....

        Direction from strategy meeting:
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $book_booth_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $booth_show_material_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$book_booth_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        /******************* task - booth and show materials - show services order ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Show Services Order - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+2 days"));
        $data['description'] = "
        This task is to track ordering of all show provided resources to include but not limited to carpet, electrical, furniture rental, labor and badges.

        Any other items that may have come from the Overview and Coordination Sheet Meeting concerning show provided resources should be tracked here. Document in outline format in main description and attach/link any documentation or scans that may be relevant to this task.

        this task can move or be subbed out if needed.

        Deadlines:

        Needs Identified:

        Contact/Logins for ordering:
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $show_services_order_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $booth_show_material_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$show_services_order_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        /******************* task - booth and show materials - product ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Product - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+2 days"));
        $data['description'] = "
        This task is to track the product order. This task is intended to be completed before shipping of the booth and materials. Document all orders, meetings, etc.. that may have come from this. Do not sub this task out but only relate other tasks to it as they are created. This task can be closed once the order is completed and on its way to the show.

        Shipping Deadline:

        Order product needed for show
        QA: perform quality check
        Marketing: Unpack every product to ensure all materials required are included
        Repack exactly as originally packed when done
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $product_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $booth_show_material_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$product_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        /******************* task - booth and show materials - print materials ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Print Materials - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+2 days"));
        $data['description'] = "
        This task can be standalone or subbed out if there is too much separation in the tasks in start and due dates and the subbed items are not similar enough to keep consolidated.

        Shipping Deadline:

        Needs Identified in Overview and Coordination Sheet

        Standard Needs:

        Order any boards / graphics needed
        Specials boards
        New product boards
        Custom swag / displays
        Pamphlets
        Booth Cards
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $print_material_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $booth_show_material_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$print_material_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);


        /******************* task - booth and show materials - Inventory and Pack ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Inventory and Pack - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+2 days"));
        $data['description'] = "
        If at all possible, set booth up inside building or warehouse to ensure everything that is needed is included.

        Setup and Inventory the booth
        Repair, fix, replace any damages or shortages
        Any tool used to setup ensure it is included in the toolkit
        Take pictures of pre-setup
        Pack booth and prep it for shipping
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $inventory_pack_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $booth_show_material_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$inventory_pack_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        /******************* task - booth and show materials - Ship Booth Return to Sellmark ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Ship Booth Return to Sellmark - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+2 days"));
        $data['description'] = "
        This task is to track all coordination of shipping booth Back to the office. The deadline for this needs to be setup for the target move-out date All necessary information should be documented here

        If it is a single crate or small enough the return shipping labels can be generated before even going to the show.

        Early shipping date:
        Absolute latest the booth can ship:
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $ship_booth_return_to_sellmark_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $booth_show_material_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$ship_booth_return_to_sellmark_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);


        /******************* task - booth and show materials - Booth Payment ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Booth Payment - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+2 days"));
        $data['description'] = "
        Use this task to track any payments due incremental or final due date, etc..

        This is a movable task that is to ensure payment is not forgotten.
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $booth_payment_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $booth_show_material_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$booth_payment_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);


        /******************* task - booth and show materials - Ship Booth TO Venue ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Ship Booth TO Venue - ".$show_identifier;
        $data['due_date'] = date("Y-m-d", strtotime("+4 days"));
        $data['assigned_to_id'] = $assign_to;
        $data['description'] = "
        This task is to track all coordination of shipping booth to the show. The deadline for this needs to be setup for the initial early shipping date. All necessary information should be documented here

        Early shipping date:
        Absolute latest the booth can ship:

        Shipping to address:
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $ship_booth_venue_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $booth_show_material_bm;
        $update_issue['issue'] = $update_data;
        $bluemine_update_url = "http://10.0.1.25/issues/".$ship_booth_venue_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $inventory_pack_bm; // bluemine ID to be added relation
        $relation_data['relation_type'] = 'follows'; // change to match type of relation [relates, precedes, follows, ...]
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$ship_booth_venue_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);

        /******************* task - booth and show materials - Receive Booth POST Show ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Receive Booth POST Show - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+4 days"));
        $data['description'] = "
        Receive booth at office

        Unpack and organize for next use

        Return any product via RA process

        Remove swag, promotional materials, catalogs etc and put in their corresponding locations.

        put away booth materials in warehouse or marketing room
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $receive_booth_post_show_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $booth_show_material_bm;
        $update_issue['issue'] = $update_data;
        $bluemine_update_url = "http://10.0.1.25/issues/".$receive_booth_post_show_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $ship_booth_return_to_sellmark_bm; // bluemine ID to be added relation
        $relation_data['relation_type'] = 'follows'; // change to match type of relation [relates, precedes, follows, ...]
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$receive_booth_post_show_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);

        /******************* task - booth and show materials - Booth Listing ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Booth Listings - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+4 days"));
        $data['description'] = "
        Ensure standard listing and profile is completed according to the brands, etc...
        Consider upgrades and ad packages for the listing boost/promotion. Ensure it is cross reported for the advertising and if a attendee, media, or exibitor list is purchased create tasks for communication plan to add them to the contact lists.
        This task can move and be assigned to whoever necessary as to what step this is in.
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $booth_listing_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $booth_show_material_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$booth_listing_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $book_booth_bm; // bluemine ID to be added relation
        $relation_data['relation_type'] = 'follows'; // change to match type of relation [relates, precedes, follows, ...]
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$booth_listing_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);

        /******************* task - administrative ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Administrative - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+14 days"));
        $data['status_id'] = 7;
        $data['description'] = "This is the parent task for all administrative tasks such as meetings, justification sheets, strategy sessions, creative brief meetings, etc..";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $administrative_bm = $this->get_newest_issue_id($get_new_bm_url);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $initial_bm;
        $relation_data['relation_type'] = 'follows';
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$administrative_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);


        /******************* task -> administrative -> Justification and Strategy ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Justification and Strategy - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['tracker_id'] = 2;
        $data['description'] = "
        Schedule meetings with key people, research, pull data from previous shows, build initial justification etc..
        Framing
        Establish justification for participation
        Establish clear goals
        Outline plan to achieve those goals
        General template - might need to change per show but its a start:

        Tradeshow Strategy Planning
        2. Initial Discussion
        What is it?
        When is it?
        Why does Sellmark need to go to it?
        What do we expect to achieve by going?
        Primary Goal
        Secondary Goal
        Tertiary Goal
        What are the benchmarks to determine success or failure of reaching our goals?
        1
        2
        3
        Other things to consider
        1. Planning
        How are we going to meet our goals (Be broad, then specific)?
        1
        2
        3
        What resources are we going to need to meet our goals (can be broad but need to get more specific as planning progresses)?
        People
        Money
        Time
        Materials
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $justification_strategy_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $administrative_bm;
        $update_issue['issue'] = $update_data;
        $bluemine_update_url = "http://10.0.1.25/issues/".$justification_strategy_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        /******************* task -> administrative -> Overview and Coordination ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Overview and Coordination - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['tracker_id'] = 5;
        $data['description'] = "
        Overview and Coordination Sheet Meeting / Completion
        People and Logistics
        Invitations
        Booth needs
        Additional Spaces
        Marketing Support
        Show Engagement
        Final Considerations
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $overview_coordination_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $administrative_bm;
        $update_issue['issue'] = $update_data;
        $bluemine_update_url = "http://10.0.1.25/issues/".$overview_coordination_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);


        /******************* task -> administrative -> Campaign Evaluation Report ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Campaign Evaluation Report - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['description'] = "
        Overview and Coordination Sheet Meeting / Completion
        People and Logistics
        Invitations
        Booth needs
        Additional Spaces
        Marketing Support
        Show Engagement
        Final Considerations
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $campaign_evaluation_report_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $administrative_bm;
        $update_issue['issue'] = $update_data;
        $bluemine_update_url = "http://10.0.1.25/issues/".$campaign_evaluation_report_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);


        /******************* task -> administrative -> Task Breakout and Setup Show ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Task Breakout and Setup Show - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['description'] = "
        Go through each task and set dates and relationships
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $task_breakout_setup_show_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $administrative_bm;
        $update_issue['issue'] = $update_data;
        $bluemine_update_url = "http://10.0.1.25/issues/".$task_breakout_setup_show_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        /******************* task -> administrative -> Tracker Updates ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Tracker Updates - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['description'] = "
        Update Trackers
        Collect attendee expense report
        Add to expense log and show tracker
        *Collect attendee contacts
        Add to master contact list
        Add to mailing list if applicable
        Identify press contacts
        Reach out with press template letter
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $tracker_update_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $administrative_bm;
        $update_issue['issue'] = $update_data;
        $bluemine_update_url = "http://10.0.1.25/issues/".$tracker_update_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);


        /******************* task -> administrative -> AAR  ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "AAR - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['tracker_id'] = 5;
        $data['description'] = "
        Post show/ after action review

        Complete AAR with attendees and management within 1 week post show.
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $aar_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $administrative_bm;
        $update_issue['issue'] = $update_data;
        $bluemine_update_url = "http://10.0.1.25/issues/".$aar_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        /******************* task - People and Logistics ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "People and Logistics - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['status_id'] = 7;
        $data['description'] = "This is the parent task for all People and Logistics related tasks such as hotel and flight bookings, car rentals, etc...";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $people_logistics_bm = $this->get_newest_issue_id($get_new_bm_url);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $initial_bm;
        $relation_data['relation_type'] = 'follows';
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$people_logistics_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);


        /******************* task -> People and Logistics -> Lodging   ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Lodging - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['description'] = "
        This is the task to track lodging for the show. This task is movable and lodging sometimes is reserved almost a year in advance.

        Use this task to track reservation codes, numbers, etc.. Adjust due date to the next date necessary for followup.
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $lodging_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $people_logistics_bm;
        $update_issue['issue'] = $update_data;
        $bluemine_update_url = "http://10.0.1.25/issues/".$lodging_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);


        /******************* task -> People and Logistics -> Transportation   ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Transportation - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['description'] = "
        This is the task to track flight/train/car rental purchases. Document all reservation numbers, correspondence, confirmations, etc... on this task.

        This task can move as needed until all transportation is 100% covered for the duration of the show from travel to, during and after the show for return.
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $transportation_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $people_logistics_bm;
        $update_issue['issue'] = $update_data;
        $bluemine_update_url = "http://10.0.1.25/issues/".$transportation_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        /******************* task -> People and Logistics -> Finalize Attendee List ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Finalize Attendee List - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['tracker_id'] = 2;
        $data['description'] = "
        Get attendee list finalized and approved by management and Executive.
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $finalize_attendee_list_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $people_logistics_bm;
        $update_issue['issue'] = $update_data;
        $bluemine_update_url = "http://10.0.1.25/issues/".$finalize_attendee_list_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $lodging_bm;
        $relation_data['relation_type'] = 'blocks';
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$finalize_attendee_list_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $transportation_bm;
        $relation_data['relation_type'] = 'blocks';
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$finalize_attendee_list_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);

        /******************* task - Communication Plan ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Communication Plan - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['status_id'] = 7;
        $data['description'] = "
        This is the parent task to contain all of the sub task related to the communication plan such as media outreach, press releases, social media, etc...

        Reference 2019 Communication Plan
        \sms2\Marketing\Operations\Show Resources and Tracking - 2019 Event Communication Plan 02132019
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $communication_plan_bm = $this->get_newest_issue_id($get_new_bm_url);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $initial_bm;
        $relation_data['relation_type'] = 'follows';
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$communication_plan_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);


        /******************* task -> Communication Plan -> Invitations ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Invitations - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['description'] = "
        This task is intended to ensure that the invitations for the show is completed, added to social media events, updated on brand websites, added to Sales Resources, pre-work is completed etc..

        AS the task follows the various needs done change assignee as necessary

        Please use discretion and understand full intent of this task. Depending on the show, this could require alot more attention, or some things could be omitted.

        Design creative for invitation
        If a logo or graphic for the show exists it needs to be incorporated with the invitation
        1 invitation for each brand in attendance and one for Sellmark and 1200x628 for Facebook event
        600 x 475
        475 x 376
        1200 x 628
        Go through a quick proofing process and ensure invitation is good enough for website, social, e-mail, etc.. This may require different sizes depending on needs of team.
        Once design is complete post link to final rendered files in creative directory to this task and assign to the traffic coordinator to assign to communications for distribution and website updates and mark task as 30% complete
        Distribute invitation
        Create Facebook Event per brand \"{Brand} at {Show} {Year}\" and add graphic. Invite all members of FB page to attend.
        If in Texas, ensure Texas Hog Hunters Association is invited by posting in their group
        Update websites per brand - Currently this is done through Facebook Events Wordpress plugin which should pipe through to the blogs. Also add to the CMS - Eventually the CMS option will be removed as the Facebook to Wordpress sync is more efficient.
        Add graphics to Sales resource folder
        Send e-mail to sales team with Branded and Sellmark graphics to be used for e-mail signatures and link to sales resource folder that contains the various graphics. Include in the email links to the social events.
        Build target list and email campaign for the official invite from Sellmark/Brand via MailChimp. For multiple brands try to make them unique - not cookie cutter.
        Set task due date to 1 month before show date and mark task 90% complete
        1 Month out
        Send e-mails with graphics and links to sales and CS with the invite again reminding them to add the show to their signature if they want.
        Send the mailchimp e-mail/s - space out of multiple.
        All outgoing emails should be from email@brand.com. Do not us the same @sellmark.net email for other brands.
        Mark 100% and close
        Reference 2019 Communication Plan
        \sms2\Marketing\Operations\Show Resources and Tracking - 2019 Event Communication Plan 02132019
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $invitations_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $communication_plan_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$invitations_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        /******************* task -> Communication Plan -> Social Media ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Social Media - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['description'] = "
        This is a moving task with a start date and due date to be changed each time until the days of the tradeshow in which only the due date changes per day.

        Adjust start and due date for initial posts accordingly with max 1 week to due date.

        Depending on the size of the show - build out a social media plan that can include giveaways, interviews, list building strategies, and just general engagement efforts.

        All write-off needs justification written and signed off on.

        Basics
        Social Posts Minimum
        At booking
        6 months before show
        1 Month before show
        Day before show
        First day of show
        Booth setup
        Crowd pictures
        Team picture
        Anything you deem shareworthy
        Every day of show
        Last day of show
        Breakdown
        Goodbye
        Day after show
        See you next year
        Any continuing specials
        Extras
        Special activities
        Dealer large purchases with tag (tell them to like our page to be tagged)
        End user purchases with tag (tell them to like our page)
        Anything you deem shareworthy
        Reference 2019 Communication Plan
        \sms2\Marketing\Operations\Show Resources and Tracking - 2019 Event Communication Plan 02132019
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $social_media_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $communication_plan_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$social_media_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);


        /******************* task -> Communication Plan -> Advertising ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Advertising - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['description'] = "
        Use this task to track and plan ad buys as determined by framing and standard social media promotion, what is available by budget per brand, etc etc ..

        Sub task out as needed or keep it all in this.

        Ads
        Announcement ad
        Digital
        Print
        E-blast
        New Product Ads
        Digital
        Print
        E-blast
        Presence / Promotion Ads
        Digital
        Print
        E-blast
        Social
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $advertising_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $communication_plan_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$advertising_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        /******************* task -> Communication Plan -> Press Release ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Press Release - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+7 days"));
        $data['description'] = "
        This task is for the show PR creation and distribution.

        2 Press releases minimum:

        Immediately after booking book
        Write and send for proof, mark 20%
        Receive feedback and edit, then distribute; mark 50% and move to 1 month before show to write second PR
        2 weeks before show
        Write and send for proof, mark 70%; receive feedback and make edits, schedule to distribute 2 weeks before show, mark 100%, tag to close
        Need to include:
        CLEAR CALL TO ACTION based on show messaging
        Include photo and video
        All venue info and booth number
        Products to be featured
        Release per brand
        Any celebrity appearances
        Any really good deals
        Show landing page
        Goal: Highest possible reach. To be written by Communication Specialist. Resources available include notes from messaging meeting, product setup information, show overview in bluemine, external resources, show specials and show landing page. Resources for best practices can be found here:

        Here is an article on timing: http://www.vporoom.com/the-best-time-to-send-your-trade-show-press-release
        Listicle on show PR Success: https://www.prdaily.com/8-ways-to-issue-successful-trade-show-press-releases/
        How-To for show PR: https://fitsmallbusiness.com/event-press-release-template/
        Show PR Tips: https://www.exhibitoronline.com/topics/article.asp?ID=784
        How-To on getting attention: https://en.prnasia.com/blog/2017/05/how-to-write-a-trade-show-news-release-that-turns-heads/

        More Tips:
        https://www.thetradeshownetwork.com/trade-show-blog/6-tips-for-trade-show-press-releases
        https://www.axiapr.com/blog/3-ways-to-prepare-a-news-release-for-a-trade-show
        https://maestrodisplays.com/tips-for-trade-show-press-releases/
        https://www.entrepreneur.com/article/228044
        http://www.shopforexhibits.com/blog/how-write-your-trade-show-press-release
        https://sspr.com/trade-shows/media-coverage-for-trade-show/
        https://www.blastmedia.com/2010/04/14/what-to-include-in-a-press-kit-and-other-helpful-trade-show-tips/

        Distribute to:
        • Wires
        • Press email list
        • Dealer email list
        • Show dashboard or listing

        KPIs:
        • CTA response
        • Notable Writeups
        • Traffic to show landing page increase within 1 week of release (use UTM if able)
        • Potential Reach
        • Media outreach (# of media who follow up with us or request meetings)
        • UTM incoming data
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $press_release_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $communication_plan_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$press_release_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $book_booth_bm; // bluemine ID to be added relation
        $relation_data['relation_type'] = 'follows'; // change to match type of relation [relates, precedes, follows, ...]
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$press_release_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);

        /******************* task - Timeline ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Timeline - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+1 day"));
        $data['status_id'] = 7;
        $data['description'] = "
        Key deadline dates - these do not move!
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $timeline_bm = $this->get_newest_issue_id($get_new_bm_url);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $initial_bm;
        $relation_data['relation_type'] = 'follows';
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$timeline_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);


        /******************* task -> Timeline -> Show Time ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Show Time - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+1 day"));
        $data['description'] = "
        This is the task that is start date of the day the show starts and the due date is the day the show ends.
        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $show_time_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $timeline_bm;
        $update_issue['issue'] = $update_data;
        $bluemine_update_url = "http://10.0.1.25/issues/".$show_time_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $initial_bm;
        $relation_data['relation_type'] = 'follows';
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$show_time_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $aar_bm;
        $relation_data['relation_type'] = 'precedes';
        $relation_data['delay'] = 5;
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$show_time_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);

        $relation = array();
        $relation_data = array();
        $relation_data['issue_to_id'] = $tracker_update_bm;
        $relation_data['relation_type'] = 'precedes';
        $relation['relation'] = $relation_data;
        $set_relation_url = "http://10.0.1.25/issues/".$show_time_bm."/relations.json";
        $this->execute_bluemine($set_relation_url, $relation);

        /******************* task -> Timeline -> Advanced Shipping ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Advanced Shipping - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+1 day"));
        $data['description'] = "

        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $advance_shipping_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $timeline_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$advance_shipping_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);


        /******************* task -> Timeline -> Show Services Deadline ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Show Services Deadline - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+1 day"));
        $data['description'] = "

        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $show_services_deadline_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $timeline_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$show_services_deadline_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);


        /******************* task -> Timeline -> Listings Deadline ********************/
        $issue = array();
        $data = array();
        $data['project_id'] = $project_id;
        $data['subject'] = "Listings Deadline - ".$show_identifier;
        $data['assigned_to_id'] = $assign_to;
        $data['due_date'] = date("Y-m-d", strtotime("+1 day"));
        $data['description'] = "

        ";
        $issue['issue'] = $data;
        $this->execute_bluemine($create_issue_url, $issue);
        $listings_deadline_bm = $this->get_newest_issue_id($get_new_bm_url);

        $update_issue = array();
        $update_data = array();
        $update_data['parent_issue_id'] = $timeline_bm;
        $update_issue['issue'] = $update_data;

        $bluemine_update_url = "http://10.0.1.25/issues/".$listings_deadline_bm.".json";
        $this->update_bluemine($bluemine_update_url, $update_issue);
    }

    public function get_json_of_url($url){
        //sample url: "http://10.0.1.25/projects/test.json";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "X-Redmine-Api-Key: 109be91ef1978ebf9e4fa4e109cefb037d6a4846";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $output = 'Error';
        }
        else{
            $output = $result;
        }
        curl_close ($ch);
        return $output;
    }

    /****************** create new bluemine task / create new related task********************/
    public function execute_bluemine($url, $data){
        // sample url for creating bluemine: http://10.0.1.25/issues.json
        // sample url for adding relation: http://10.0.1.25/issues/".$bluemine."/relations.json
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_POST, 1);
        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "X-Redmine-Api-Key: 109be91ef1978ebf9e4fa4e109cefb037d6a4846";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $output = 'Error';
        }
        else{
            $output = "Successful";
        }
        curl_close ($ch);
        return $output;
    }

    /******************* update bluemine task include parent task ********************/
    public function update_bluemine($url, $data){
        // sample url for updating bluemine: http://10.0.1.25/issues/".$bluemine.".json
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        $headers = array();
        $headers[] = "X-Redmine-Api-Key: 109be91ef1978ebf9e4fa4e109cefb037d6a4846";
        $headers[] = "Content-Type: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $output = 'Error';
        }
        else{
            $output = "Updated";
        }
        curl_close ($ch);
        return $output;
    }

    /******************** get newest bluemine task************************/
    public function get_newest_issue_id($url){
        //sample url: "http://10.0.1.25/projects/a10068/issues.json?author_id=158";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "X-Redmine-Api-Key: 109be91ef1978ebf9e4fa4e109cefb037d6a4846";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $output = 'Error';
        }
        else{
            $json = json_decode($result);
            $output = $json->issues[0]->id;
        }
        curl_close ($ch);
        return $output;
    }
}
