<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Marketing\Adbuy;

class AdbuyController extends Controller
{
    public function index(){
        $data = Adbuy::with(
        	'vendor_relation',
            'status_relation'
        )->where('active', '1')->orderBy('id', 'desc')->get();
        return view('marketing.adbuy.index', ['adbuys' => $data]);
    }

    public function getAdd(){
        return view('marketing.adbuy.form');
    }

    public function postAdd(Request $request){

        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'VendorID' => 'required',
                'description' => 'required'
            ],[
                'VendorID.required' => 'Vendor field is required.',
                'description.required' => 'Description field is required.'
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        $user = Auth::user()->id;
        $adbuy = new Adbuy;
        $adbuy->VendorID = $request->VendorID;
        $adbuy->description = $request->description;
        $adbuy->date_quoted = $request->date_quoted;
        $adbuy->date_signed = $request->date_signed;
        $adbuy->status = $request->status;
        $adbuy->total_commit = $request->total_commit;
        $adbuy->total_invoiced = $request->total_invoiced;
        $adbuy->amount_remaining = $request->amount_remaining;
        $adbuy->bluemine = $request->bluemine;
        $adbuy->uid_created = $user;
        $adbuy->uid_modified = $user;
        $adbuy->save();
        return "Adbuy Added.";

    }

    public function postUpdate(Request $request, $id){

        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'VendorID' => 'required'
            ],[
                'VendorID.required' => 'Vendor field is required.'
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        $user = Auth::user()->id;
        $adbuy = Adbuy::find($id);
        $adbuy->VendorID            = $request->VendorID;
        $adbuy->description         = $request->description == "null" ? NULL : $request->description;
        $adbuy->date_quoted         = $request->date_quoted == "null" ? NULL : $request->date_quoted;
        $adbuy->date_signed         = $request->date_signed == "null" ? NULL : $request->date_signed;
        $adbuy->status              = $request->status == "null" ? NULL : $request->status;
        $adbuy->total_commit        = $request->total_commit == "null" ? NULL : $request->total_commit;
        $adbuy->total_invoiced      = $request->total_invoiced == "null" ? NULL : $request->total_invoiced;
        $adbuy->amount_remaining    = $request->amount_remaining == "null" ? NULL : $request->amount_remaining;
        $adbuy->bluemine            = $request->bluemine == "null" ? NULL : $request->bluemine;
        $adbuy->uid_modified        = $user;
        $adbuy->save();
        return "Adbuy Updated.";

    }

    public function getUpdate($id){
    	$adbuy = Adbuy::with(
        	'vendor_relation',
            'status_relation'
        )->find($id);
        return view('marketing.adbuy.form', ['adbuy' => $adbuy]);
    }

    public function delete($id){
        $user = Auth::user()->id; 
        $adbuy = Adbuy::find($id);
        $adbuy->active = 0;
        $adbuy->uid_modified = $user;
        $adbuy->save();
        return 'Deleted.';
    }

    public function view($id){
        $adbuy = Adbuy::with(
            'vendor_relation',
            'status_relation',
            'adspot_relation.channel_relation',
            'adspot_relation.status_relation',
            'changes_relation.changes_detail_relation',
            'changes_relation.user_relation'
        )->find($id);
        return view('marketing.adbuy.view', ['adbuy' => $adbuy]);
    }

    public function getData(){
        return Adbuy::where('active', '1')->orderBy('id', 'desc')->get();
    }
}
