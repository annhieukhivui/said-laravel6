<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

use App\Mail\ProductOrderSendMail;

use App\Http\Controllers\Controller;

use App\Models\Marketing\TEProductOrder;
use App\Models\Marketing\Placement;
use App\Models\Marketing\Asset;
use App\Models\Marketing\TEWaitList;
use App\Models\Shared\Document;
use App\Models\Shared\Note;


class ProductOrderController extends Controller
{
    public function index() {  
        $user = Auth::user()->firstname.' '.Auth::user()->lastname; 
    	return view('marketing.product-order.index', ['user' => $user]);
    }

    public function getAdd(){
        $user = Auth::user()->firstname.' '.Auth::user()->lastname; 
        return view('marketing.product-order.add', ['user' => $user]);
    }

    public function postAdd(Request $request){

        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'contact' => 'required',
                'owner' => 'required',
                'term' => 'required',
            ],[
                'contact.required' => 'Contact field is required.',
                'owner.required' => 'Owner field is required.',
                'term.required' => 'Term field is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        /************main*************/        
        $user = Auth::user()->id; 
        $contactObj = json_decode($request->contact);
        
        if(count(json_decode($request->inventory_ids)) > 0){
            foreach (json_decode($request->inventory_ids) as $inventory) {
                $order = new TEProductOrder;
                $order->ContactID               = $contactObj->value;
                $order->relationship            = $request->relationship;
                $order->owner                   = $request->owner;
                $order->InventoryID             = $inventory->id;
                $order->serial                  = $inventory->serial == '' ? NULL : $inventory->serial;
                $order->quantity                = $inventory->qty == '' ? NULL : $inventory->qty;
                $order->sale_order              = $request->saleorder;
                $order->invoice                 = $request->invoice;
                $order->date_out                = $request->dateout;
                $order->term                    = $request->term;
                $order->expect_return_date      = $request->expect_return;
                $order->sale_return_date        = $request->salereturndate;
                $order->ra                      = $request->ra;
                $order->note                    = $request->note;
                $order->fulfillment_tracking    = $request->fulfillment_tracking;
                $order->addon                   = $inventory->addon;
                $order->status                  = $request->status;
                $order->location                = $request->location;
                $order->return_late             = $request->return_late == 'true' ? 1 : 0;
                $order->return_damaged          = $request->return_damaged == 'true' ? 1 : 0;
                $order->follow_up_date          = $request->followup;
                $order->uid_created             = $user;
                $order->uid_modified            = $user;
                $order->save();
                $lastID = $order->id;

                /************upload documents*************/
                if(!empty($request->file('documents'))){
                    foreach ($request->file('documents') as $file) {
                        $name = $file->getClientOriginalName();
                        $file_name = 'product-order-'.$lastID.'-'.uniqid().'-'.$name;
                        if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                            $document = new Document;
                            $document->table_name = 125;
                            $document->document_name = $file_name;
                            $document->table_id = $lastID;
                            $document->save();
                        }
                    }
                }
            }
        }
        else{
            return response()->json(['errors'=>['Product field is required']],422);
        }


        if($request->product_order_id){
            $group_order = TEProductOrder::find($request->product_order_id);
            $pid = $group_order->pid;
        }
        else{
            $pid = $lastID;
        }

        // update pid
        TEProductOrder::orderBy('id', 'desc')->limit(count(json_decode($request->inventory_ids)))->update(['pid' => $pid]);

        $res = "Product Order Added.";
        if($request->send_mail == 'true'){
            Mail::to($request->owner_email)->send(new ProductOrderSendMail($request));
            $res .= " Confirmation Email Sent. Check your inbox.";
        }
        return $res;

    }

    public function getUpdate($id){
        $order = TEProductOrder::with([
            'inventory_relation',
            'inventory_relation.product_relation',
            'contact_relation',
            'contact_relation.general_contact_relation', 
            'status_relation',
            'document_relation'
        ])->find($id);
        return view('marketing.product-order.update', ['order' => $order]);
    }

    public function postUpdate(Request $request, $id){

        $request['post_form'] = 'update';
        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'contact' => 'required',
                'owner' => 'required',
                'term' => 'required',
                'inventory_id' => 'required',
            ],[
                'contact.required' => 'Contact field is required.',
                'owner.required' => 'Owner field is required.',
                'term.required' => 'Term field is required.',
                'inventory_id.required' => 'Product field is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        /************main*************/      
        $user = Auth::user()->id;   
        $contactObj = json_decode($request->contact);

        $order = TEProductOrder::find($id);
        $order->ContactID               = $contactObj->value;
        $order->relationship            = $request->relationship == "null" ? NULL : $request->relationship;
        $order->owner                   = $request->owner == "null" ? NULL : $request->owner;
        $order->InventoryID             = $request->inventory_id;
        $order->serial                  = $request->serial == 'null' ? NULL : $request->serial;
        $order->quantity                = $request->quantity == 'null' ? NULL : $request->quantity;
        $order->sale_order              = $request->saleorder == "null" ? NULL : $request->saleorder;
        $order->invoice                 = $request->invoice == "null" ? NULL : $request->invoice;
        $order->date_out                = $request->dateout == "null" ? NULL : $request->dateout;
        $order->term                    = $request->term == "null" ? NULL : $request->term;

        $expectedreturn = NULL;
        if($request->status == 63 || $request->status == 68){
            $expectedreturn = $request->expect_return == "null" ? NULL : $request->expect_return;
        }
        $order->expect_return_date      = $expectedreturn;


        $order->sale_return_date        = $request->salereturndate == "null" ? NULL : $request->salereturndate;
        $order->ra                      = $request->ra == "null" ? NULL : $request->ra;
        $order->note                    = $request->note == "null" ? NULL : $request->note;
        $order->fulfillment_tracking    = $request->fulfillment_tracking == "null" ? NULL : $request->fulfillment_tracking;
        $order->addon                   = $request->addon == 'true' ? 1 : 0;
        $order->status                  = $request->status == "null" ? NULL : $request->status;
        $order->location                = $request->location == "null" ? NULL : $request->location;
        $order->return_late             = $request->return_late == 'true' ? 1 : 0;
        $order->return_damaged          = $request->return_damaged == 'true' ? 1 : 0;
        $order->follow_up_date          = $request->followup == "null" ? NULL : $request->followup;
        $order->uid_modified            = $user;
        $order->save();

        /************upload documents*************/
        if(!empty($request->file('documents'))){
            foreach ($request->file('documents') as $file) {
                $name = $file->getClientOriginalName();
                $file_name = 'product-order-'.$id.'-'.uniqid().'-'.$name;
                if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                    $document = new Document;
                    $document->table_name = 125;
                    $document->document_name = $file_name;
                    $document->table_id = $id;
                    $document->save();
                }
            }
        }
        
        /************add note*************/
        if($request->note_detail != ''){
            $note = new Note;
            $note->note_on_table = 125;
            $note->note_on_table_id = $id;
            $note->action = $request->note_action; // 72 is general note
            $note->detail = $request->note_detail;
            $note->uid_created = $user;
            $note->save();
        }

        $res = "Product Order Updated.";
        if($request->send_mail == 'true'){
            Mail::to($request->owner_email)->send(new ProductOrderSendMail($request));
            $res .= " Confirmation Email Sent. Check your mailbox.";
        }
        
        return $res;

    }

    public function delete($id){
        $user = Auth::user()->id; 
        $order = TEProductOrder::find($id);
        $order->uid_modified = $user;
        $order->active = 0;
        $order->save();
        return "Deleted.";
    }

    public function view($id){
        $order = TEProductOrder::with([
            'inventory_relation',
            'inventory_relation.product_relation',
            'contact_relation', 
            'contact_relation.general_contact_relation', 
            'location_relation',
            'status_relation',
            'document_relation',
            'placement_relation',
            'placement_relation.channel_relation',
            'placement_relation.type_relation',
            'note_relation.action_relation',
            'note_relation.user_relation',
            'changes_relation.changes_detail_relation',
            'changes_relation.user_relation'
        ])->find($id);
        return view('marketing.product-order.view', ['order' => $order]);
    }

    public function getData(){
        $data = TEProductOrder::with([
        	'inventory_relation',
            'inventory_relation.product_relation',
        	'contact_relation', 
            'contact_relation.general_contact_relation',
        	'status_relation',
        	'placement_relation'
        ])->where('active', '1')->orderBy('id', 'desc')->get();
        return $data;
    }

    public function postData(Request $request){
        $data = TEProductOrder::with([
            'inventory_relation',
            'inventory_relation.product_relation',
        ])->where([['active', '1'], ['ContactID', $request->contact]])->orderBy('id', 'desc')->get();
        return $data;
    }



    public function getProgramAdd($program_id, $wid){
        $user = Auth::user()->firstname.' '.Auth::user()->lastname; 
        return view('marketing.program.order.add', ['user' => $user, 'program_id' => $program_id, 'wait_id' => $wid]);
    }

    public function postProgramAdd(Request $request, $program_id, $wid){

        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'contact' => 'required',
                'owner' => 'required',
                'term' => 'required',
            ],[
                'contact.required' => 'Contact field is required.',
                'owner.required' => 'Owner field is required.',
                'term.required' => 'Term field is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        /************main*************/        
        
        $user = Auth::user()->id; 
        $contactObj = json_decode($request->contact);

        if(count(json_decode($request->inventory_ids)) > 0){
            foreach (json_decode($request->inventory_ids) as $inventory) {
                $order = new TEProductOrder;
                $order->ContactID               = $contactObj->value;
                $order->relationship            = $request->relationship;
                $order->owner                   = $request->owner;
                $order->InventoryID             = $inventory->id;
                $order->serial                  = $inventory->serial == '' ? NULL : $inventory->serial;
                $order->quantity                = $inventory->qty == '' ? NULL : $inventory->qty;
                $order->sale_order              = $request->saleorder;
                $order->invoice                 = $request->invoice;
                $order->date_out                = $request->dateout;
                $order->term                    = $request->term;
                $order->expect_return_date      = $request->expect_return;
                $order->sale_return_date        = $request->salereturndate;
                $order->ra                      = $request->ra;
                $order->note                    = $request->note;
                $order->fulfillment_tracking    = $request->fulfillment_tracking;
                $order->addon                   = $inventory->addon;
                $order->status                  = $request->status;
                $order->location                = $request->location;
                $order->return_late             = $request->return_late == 'true' ? 1 : 0;
                $order->return_damaged          = $request->return_damaged == 'true' ? 1 : 0;
                $order->follow_up_date          = $request->followup;
                $order->uid_created             = $user;
                $order->uid_modified            = $user;
                $order->save();
                $lastID = $order->id;

                /************upload documents*************/
                if(!empty($request->file('documents'))){
                    foreach ($request->file('documents') as $file) {
                        $name = $file->getClientOriginalName();
                        $file_name = 'product-order-'.$lastID.'-'.uniqid().'-'.$name;
                        if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                            $document = new Document;
                            $document->table_name = 125;
                            $document->document_name = $file_name;
                            $document->table_id = $lastID;
                            $document->save();
                        }
                    }
                }
            }
        }
        else{
            return response()->json(['errors'=>['Product field is required']],422);
        }

        if($request->product_order_id){
            $group_order = TEProductOrder::find($request->product_order_id);
            $pid = $group_order->pid;
        }
        else{
            $pid = $lastID;
        }

        // update pid
        TEProductOrder::orderBy('id', 'desc')->limit(count(json_decode($request->inventory_ids)))->update(['pid' => $pid]);

        // reserve asset
        Asset::where([['InventoryID', '=', $inventory->id], ['serial', '=', $inventory->serial]])->update(['reserved' => 1]);

        // update waitlist
        $this->updateWaitList($wid);

        $res = "Product Order Added.";
        if($request->send_mail == 'true'){
            Mail::to($request->owner_email)->send(new ProductOrderSendMail($request));
            $res .= " Confirmation Email Sent. Check your inbox.";
        }
        
        return $res;

    }

    public function kpi_order_count(){
        $data = TEProductOrder::with([
            'inventory_relation',
            'inventory_relation.product_relation',
            'contact_relation', 
            'contact_relation.general_contact_relation',
            'status_relation',
            'placement_relation'
        ])->where('active', '1')->get();

        $data2 = Placement::with([
            'product_order_relation.inventory_relation.product_relation',
        ])->get();
        return view('marketing/product-order/kpi', ['orders' => $data, 'placements' => $data2]);
    }

    public function updateWaitList($wid){
        $user = Auth::user()->id; 
        $waitlist = TEWaitList::find($wid);
        $program = $waitlist->ProgramID;
        $queue = $waitlist->queue;
        $waitlist->status = 208;
        $waitlist->queue = 0;
        $waitlist->uid_modified = $user;
        $waitlist->save();

        $waitlists = TEWaitList::where([
            ['ProgramID', '=', $program],
            ['queue', '>', $queue]
        ])->get();
        if($queue > 0){
            foreach ($waitlists as $w) {
                $waitlist = TEWaitList::find($w->id);
                $waitlist->queue = $waitlist->queue - 1;
                $waitlist->save();
            }
        }
    }
}
