<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request as basicRequest;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\MKContactRequest as Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Models\Marketing\Contact;
use App\Models\Shared\Note;
use App\Models\Shared\Contact as ShareContact;

class ContactController extends Controller
{
	public function index(){
        $user = Auth::user()->firstname.' '.Auth::user()->lastname; 
        $data = Contact::with([
            'general_contact_relation',
            'status_relation',
            'type_relation',
            'note_relation_count'
        ])->where('active', '1')->orderBy('id', 'desc')->get();
        return view('marketing.contact.index', ['allcontacts' => $data, 'user' => $user]);
    }

    public function getAdd(){
        $user = Auth::user()->firstname.' '.Auth::user()->lastname; 
        return view('marketing.contact.add', ['user' => $user]);
    }

    public function postAdd(Request $request){

        $user = Auth::user()->id;

        $contactid = $request->contact_id;
        if($contactid == 0){
            if($request->firstname == '' || $request->lastname == ''){
                return response()->json(['errors'=>['Name fields are required']],422);
            }
            
            $sharecontact = new ShareContact;
            $sharecontact->firstname = $request->firstname;
            $sharecontact->lastname = $request->lastname;
            $sharecontact->email = $request->email;
            $sharecontact->phone1 = $request->phone1;
            $sharecontact->address1 = $request->address1;
            $sharecontact->city = $request->city;
            $sharecontact->state = $request->state;
            $sharecontact->zip = $request->zip;
            $sharecontact->uid_created = $user;
            $sharecontact->uid_modified = $user;
            $sharecontact->save();
            $contactid = $sharecontact->id;
        }

        $contact = new Contact;
        $contact->ContactID                 = $contactid;
        $contact->relationship              = $request->relationship;
        $contact->owner                     = $request->owner;
        $contact->status                    = $request->status;
        $contact->type                      = $request->type;
        $contact->follow_up_date            = $request->follow_up_date;
        $contact->beat                      = $request->beat;
        $contact->note                      = $request->note;
        $contact->grade                     = $request->grade;
        $contact->pl_prostaff               = $request->pl_prostaff == 'true' ? 1 : 0;
        $contact->sm_prostaff               = $request->sm_prostaff == 'true' ? 1 : 0;
        $contact->aglow                     = $request->aglow == 'true' ? 1 : 0;
        $contact->poma                      = $request->poma == 'true' ? 1 : 0;
        $contact->nssf                      = $request->nssf == 'true' ? 1 : 0;
        $contact->nra                       = $request->nra == 'true' ? 1 : 0;
        $contact->yt_reach                  = $request->yt_reach;
        $contact->fb_reach                  = $request->fb_reach;
        $contact->ig_reach                  = $request->ig_reach;
        $contact->tw_reach                  = $request->tw_reach;
        $contact->web_reach                 = $request->web_reach;
        $contact->high_value_influencer     = $request->high_value_influencer == 'true' ? 1 : 0;
        $contact->paid_influencer           = $request->paid_influencer == 'true' ? 1 : 0;
        $contact->uid_created               = $user;
        $contact->uid_modified              = $user;
        $contact->save();

        return "Contact Added.";
        
    }

    public function getUpdate($id){
        $contact = Contact::with([
            'general_contact_relation',
            'status_relation',
            'type_relation'
        ])->find($id);
        return view('marketing.contact.update', ['contact' => $contact]);
    }

    public function postUpdate(Request $request, $id){
        
        $user = Auth::user()->id;
        $contact = Contact::find($id);
        $contact->ContactID                 = $request->contact_id;
        $contact->relationship              = $request->relationship == 'null' ? NULL : $request->relationship;
        $contact->owner                     = $request->owner == 'null' ? NULL : $request->owner;
        $contact->status                    = $request->status == 'null' ? NULL : $request->status;
        $contact->type                      = $request->type == 'null' ? NULL : $request->type;
        $contact->follow_up_date            = $request->follow_up_date == 'null' ? NULL : $request->follow_up_date;
        $contact->beat                      = $request->beat == 'null' ? NULL : $request->beat;
        $contact->note                      = $request->note == 'null' ? NULL : $request->note;
        $contact->grade                     = $request->grade == 'null' ? NULL : $request->grade;
        $contact->pl_prostaff               = $request->pl_prostaff == 'true' ? 1 : 0;
        $contact->sm_prostaff               = $request->sm_prostaff == 'true' ? 1 : 0;
        $contact->aglow                     = $request->aglow == 'true' ? 1 : 0;
        $contact->poma                      = $request->poma == 'true' ? 1 : 0;
        $contact->nssf                      = $request->nssf == 'true' ? 1 : 0;
        $contact->nra                       = $request->nra == 'true' ? 1 : 0;
        $contact->yt_reach                  = $request->yt_reach == 'null' ? NULL : $request->yt_reach;
        $contact->fb_reach                  = $request->fb_reach == 'null' ? NULL : $request->fb_reach;
        $contact->ig_reach                  = $request->ig_reach == 'null' ? NULL : $request->ig_reach;
        $contact->tw_reach                  = $request->tw_reach == 'null' ? NULL : $request->tw_reach;
        $contact->web_reach                 = $request->web_reach == 'null' ? NULL : $request->web_reach;
        $contact->high_value_influencer     = $request->high_value_influencer == 'true' ? 1 : 0;
        $contact->paid_influencer           = $request->paid_influencer == 'true' ? 1 : 0;
        $contact->uid_modified              = $user;
        $contact->save();


        $contactid = $request->contact_id;
        if($contactid != 0){
            if($request->firstname == '' || $request->lastname == ''){
                return response()->json(['errors'=>['Name fields are required']],422);
            }
            
            $sharecontact = ShareContact::find($contactid);
            $sharecontact->firstname = $request->firstname;
            $sharecontact->lastname = $request->lastname;
            $sharecontact->email = $request->email == 'null' ? NULL : $request->email;
            $sharecontact->phone1 = $request->phone1 == 'null' ? NULL : $request->phone1;
            $sharecontact->address1 = $request->address1 == 'null' ? NULL : $request->address1;
            $sharecontact->city = $request->city == 'null' ? NULL : $request->city;
            $sharecontact->state = $request->state == 'null' ? NULL : $request->state;
            $sharecontact->zip = $request->zip == 'null' ? NULL : $request->zip;
            $sharecontact->uid_modified = $user;
            $sharecontact->save();
        }

        /************add note*************/
        if($request->note_detail != ''){
            $note = new Note;
            $note->note_on_table = 126;
            $note->note_on_table_id = $id;
            $note->action = $request->note_action; // 72 is general note
            $note->detail = $request->note_detail;
            $note->uid_created = $user;
            $note->save();
        }
        return "Contact Updated.";
        
    }


    public function delete($id){
        $user = Auth::user()->id; 
        $contact = Contact::find($id);
        $contact->active = 0;
        $contact->uid_modified = $user;
        $contact->save();
        return 'Deleted.';
    }

    public function view($id){
        $contact = Contact::with([
            'general_contact_relation',
            'status_relation',
            'type_relation',
            'product_order_relation.status_relation',
            'product_order_relation.inventory_relation.product_relation',
            'placement_relation.type_relation',
            'note_relation.action_relation',
            'note_relation.user_relation',
            'changes_relation.changes_detail_relation',
            'changes_relation.user_relation'
        ])->find($id);
        return view('marketing.contact.view', ['contact' => $contact]);

    }

    public function addnote(basicRequest $request, $id){
        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'note_detail' => 'required',
            ],[
                'note_detail.required' => 'Note field is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        $user = Auth::user()->id; 

        $contact = Contact::find($id);
        $contact->follow_up_date = $request->follow_up_date == 'null' ? NULL : $request->follow_up_date;
        $contact->uid_modified = $user;
        $contact->save();

        $note = new Note;
        $note->note_on_table = 126;
        $note->note_on_table_id = $id;
        $note->action = $request->note_action;
        $note->detail = $request->note_detail;
        $note->uid_created = $user;
        $note->save();

        return "Note Added.";
    }

    public function getData(){
        // return Contact::with([
        //     'general_contact_relation', 
        // ])->get()->map(function($contact){
        // 	return [
        // 		'id' => $contact->id,
        // 		'ContactID' => $contact->ContactID,
        // 		'firstname' => $contact->general_contact_relation->firstname,
        // 		'lastname' => $contact->general_contact_relation->lastname
        // 	];
        // });
        return Contact::with([
            'general_contact_relation'
        ])->where('active', '1')->orderBy('id', 'desc')->get();

    }
}
