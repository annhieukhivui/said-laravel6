<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Marketing\Channel;

class ChannelController extends Controller
{
    public function getData(){
        return Channel::where('active', '1')->orderBy('id', 'desc')->get();
    }
}
