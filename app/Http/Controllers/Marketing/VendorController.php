<?php

namespace App\Http\Controllers\Marketing;

// use Illuminate\Http\Request;
use App\Http\Requests\VendorRequest as Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Models\Marketing\Vendor;
use App\Models\Marketing\Channel;

class VendorController extends Controller
{
    public function index(){
        $data = Vendor::with(
            'channel_relation'
        )->where('active', '1')->orderBy('id', 'desc')->get();
        return view('marketing.vendor.index', ['vendors' => $data]);
    }

    public function getAdd(){
        return view('marketing.vendor.form');
    }

    public function postAdd(Request $request){

        $user = Auth::user()->id;
        $vendor = new Vendor;
        $vendor->name           = $request->name;
        $vendor->short_name     = $request->short_name;
        $vendor->email          = $request->email;
        $vendor->phone          = $request->phone;
        $vendor->fax            = $request->fax;
        $vendor->website        = $request->website;
        $vendor->address1       = $request->address1;
        $vendor->address2       = $request->address2;
        $vendor->city           = $request->city;
        $vendor->state          = $request->state;
        $vendor->zip            = $request->zip;
        $vendor->uid_created    = $user;
        $vendor->uid_modified   = $user;
        $vendor->save();
        $lastID = $vendor->id;

        if(count(json_decode($request->channelArr)) > 0){
            foreach (json_decode($request->channelArr) as $ch) {
                $channel = new Channel;
                $channel->VendorID      = $lastID;
                $channel->name          = $ch->channel_name;
                $channel->medium        = $ch->channel_medium == '' ? NULL : $ch->channel_medium;
                $channel->frequency     = $ch->channel_frequency == '' ? NULL : $ch->channel_frequency;
                $channel->platform      = $ch->channel_platform;
                $channel->category      = $ch->channel_category;
                $channel->target        = $ch->channel_target;
                $channel->reach         = $ch->channel_reach;
                $channel->uid_created   = $user;
                $channel->uid_modified  = $user;
                $channel->save();
            }
        }

        return "Vendor Added";
    }

    public function getUpdate($id){
        $vendor = Vendor::with(
            'channel_relation'
        )->find($id);
        return view('marketing.vendor.form', ['vendor' => $vendor]);
    }

    public function postUpdate(Request $request, $id){
        $user = Auth::user()->id;
        $vendor = Vendor::find($id);
        $vendor->name           = $request->name;
        $vendor->short_name     = $request->short_name == "null" ? NULL : $request->short_name;
        $vendor->email          = $request->email == "null" ? NULL : $request->email;
        $vendor->phone          = $request->phone == "null" ? NULL : $request->phone;
        $vendor->fax            = $request->fax == "null" ? NULL : $request->fax;
        $vendor->website        = $request->website == "null" ? NULL : $request->website;
        $vendor->address1       = $request->address1 == "null" ? NULL : $request->address1;
        $vendor->address2       = $request->address2 == "null" ? NULL : $request->address2;
        $vendor->city           = $request->city == "null" ? NULL : $request->city;
        $vendor->state          = $request->state == "null" ? NULL : $request->state;
        $vendor->zip            = $request->zip == "null" ? NULL : $request->zip;
        $vendor->uid_created    = $user;
        $vendor->uid_modified   = $user;
        $vendor->save();

        $oldArr = json_decode(Channel::select('id')->where('VendorID', $id)->get()->map(function($ch) {return $ch->id; }), true);
        $newArr = array_map(function($ch){ return $ch->channel_id; }, json_decode($request->channelArr));
        $lostArr = array_diff($oldArr,$newArr);

        foreach (json_decode($request->channelArr) as $ch) {
            if(Channel::where('id', $ch->channel_id)->exists()){
                $channel = Channel::find($ch->channel_id);
            }
            else{
                $channel = new Channel;
            }
            $channel->VendorID      = $id;
            $channel->name          = $ch->channel_name;
            $channel->medium        = $ch->channel_medium == '' ? NULL : $ch->channel_medium;
            $channel->frequency     = $ch->channel_frequency == '' ? NULL : $ch->channel_frequency;
            $channel->platform      = $ch->channel_platform;
            $channel->category      = $ch->channel_category;
            $channel->target        = $ch->channel_target;
            $channel->reach         = $ch->channel_reach;
            $channel->uid_created   = $user;
            $channel->uid_modified  = $user;
            $channel->save();
        }
        foreach ($lostArr as $ch) {
            Channel::destroy($ch);
        }
        return "Vendor Updated.";
    }

    public function view($id){
        $vendor = Vendor::with(
            'channel_relation',
            'channel_relation.medium_relation',
            'channel_relation.frequency_relation'
        )->find($id);
        return view('marketing.vendor.view', ['vendor' => $vendor]);
    }

    public function delete($id){
        $user = Auth::user()->id; 
        $vendor = Vendor::find($id);
        $vendor->active = 0;
        $vendor->uid_modified = $user;
        $vendor->save();
        return 'Deleted.';
    }

    public function getData(){
        return Vendor::where('active', '1')->orderBy('name')->get();
    }
}
