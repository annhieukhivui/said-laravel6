<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Support\Facades\Auth;
// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\TradeshowRequest;
use App\Http\Controllers\Controller;

use App\Models\Marketing\Tradeshow;
use App\Models\Shared\Document;

class TradeshowController extends Controller
{

    public function index() {
        return view('marketing.tradeshow.index');
    }   

    public function getAdd(){
        return view('marketing.tradeshow.form');
    }

    public function postAdd(TradeshowRequest $request){
        $user = Auth::user()->id;
        $tradeshow = new Tradeshow;
        $tradeshow->show_name           = $request->show_name;
        $tradeshow->short_name          = $request->show_short_name;
        $tradeshow->show_description    = $request->show_description;
        $tradeshow->website             = $request->show_website;
        $tradeshow->market_strategy     = $request->show_strategy;
        $tradeshow->show_status         = $request->show_status;
        $tradeshow->vendor              = $request->show_vendor;
        $tradeshow->uid_created         = $user;
        $tradeshow->uid_modified        = $user;
        $tradeshow->save();
        $lastShowID = $tradeshow->id;

        if(!empty($request->file('show_document'))){
            foreach ($request->file('show_document') as $file) {
                $name = $file->getClientOriginalName();
                $file_name = 'tradeshow-'.$lastShowID.'-'.$name;
                if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                    $document = new Document;
                    $document->table_name = 124;
                    $document->table_id = $lastShowID;
                    $document->document_name = $file_name;
                    $document->save();
                }
            }
        }
        return "Tradeshow Added.";
        
    }

    public function getUpdate($id){
        // $tradeshow = Tradeshow::find($id);
        $tradeshow = Tradeshow::with([
            'vendor_relation', 
            'show_occurrence_relation',
            'market_strategy_relation',
            'show_status_relation',
            'document_relation'
        ])->find($id);
        return view('marketing.tradeshow.form', ['tradeshow' => $tradeshow]);
    }

    public function postUpdate(TradeshowRequest $request, $id){
        $user = Auth::user()->id;
        $tradeshow = Tradeshow::find($id);
        $tradeshow->show_name           = $request->show_name;
        $tradeshow->short_name          = $request->show_short_name;
        $tradeshow->show_description    = $request->show_description == "null" ? NULL : $request->show_description;
        $tradeshow->website             = $request->show_website == "null" ? NULL : $request->show_website;
        $tradeshow->market_strategy     = $request->show_strategy == "null" ? NULL : $request->show_strategy;
        $tradeshow->show_status         = $request->show_status == "null" ? NULL : $request->show_status;
        $tradeshow->vendor              = $request->show_vendor;
        $tradeshow->uid_modified        = $user;
        $tradeshow->save();

        if(!empty($request->file('show_document'))){
            foreach ($request->file('show_document') as $file) {
                $name = $file->getClientOriginalName();
                $file_name = 'tradeshow-'.$id.'-'.$name;
                if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                    $document = new Document;
                    $document->table_name = 124;
                    $document->table_id = $id;
                    $document->document_name = $file_name;
                    $document->table_id = $id;
                    $document->save();
                }
            }
        }
        return "Tradeshow Updated.";

    }

    public function view($id){
        $tradeshow = Tradeshow::with([
            'vendor_relation', 
            'show_occurrence_relation',
            'market_strategy_relation',
            'show_status_relation',
            'document_relation'
        ])->find($id);
        return view('marketing.tradeshow.view', ['tradeshow' => $tradeshow]);
    }


    public function delete($id){
        $user = Auth::user()->id;
        $tradeshow = Tradeshow::find($id);
        $tradeshow->active = 0;
        $tradeshow->uid_modified = $user;
        if($tradeshow->save()){
            return "Deleted.";
        }
        else{
            return "Failed.";
        }    
    }

    public function getData(){
        return Tradeshow::with(['market_strategy_relation', 'show_status_relation', 'vendor_relation'])->where('active', '1')->orderBy('id', 'desc')->get();
    }


}

