<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\Controller;
use App\Models\Marketing\Adspot;
use App\Models\Shared\Document;


class AdspotController extends Controller
{
    public function index(){
        $data = Adspot::with(
        	'channel_relation',
            'medium_relation',
            'frequency_relation',
            'size_relation',
            'orientation_relation',
            'status_relation',
            'vendor_relation',
            'adbuy_relation'
        )->where('active', '1')->orderBy('id', 'desc')->get();
        return view('marketing.adspot.index', ['adspots' => $data]);
    }

    public function getAdd(){
        return view('marketing.adspot.form');
    }

    public function postAdd(Request $request){
    	/************validate input*************/
        $validator = Validator::make($request->all(), [
                'AdbuyID' => 'required',
                'ChannelID' => 'required'
            ],[
                'AdbuyID.required' => 'Adbuy field is required.',
                'ChannelID.required' => 'Channel field is required.'
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        $user = Auth::user()->id;
        $adspot = new Adspot;
        $adspot->AdbuyID 			= $request->AdbuyID;
        $adspot->ChannelID 			= $request->ChannelID;
        $adspot->medium 			= $request->medium;
        $adspot->frequency 			= $request->frequency;
        $adspot->size 				= $request->size;
        $adspot->orientation 		= $request->orientation;
        $adspot->status 			= $request->status;
        $adspot->bluemine 			= $request->bluemine;
        $adspot->issue 				= $request->issue;
        $adspot->place 				= $request->place;
        $adspot->startdate 			= $request->startdate;
        $adspot->enddate 			= $request->enddate;
        $adspot->insertion_due 		= $request->insertion_due;
        $adspot->creative_due 		= $request->creative_due;
        $adspot->msrp 				= $request->msrp;
        $adspot->invoice_amount 	= $request->invoice_amount;
        $adspot->audited_reach 		= $request->audited_reach;
        $adspot->cpm 				= $request->cpm;
        $adspot->roi 				= $request->roi;
        $adspot->quality_factor 	= $request->quality_factor;
        $adspot->campaign 			= $request->campaign;
        $adspot->product 			= $request->product;
        $adspot->BrandID 			= $request->BrandID;
        $adspot->phone_used 		= $request->phone_used;
        $adspot->email_used 		= $request->email_used;
        $adspot->link_used 			= $request->link_used;
        $adspot->uid_created 		= $user;
        $adspot->uid_modified 		= $user;
        $adspot->save();
        $lastID = $adspot->id;

        /************upload documents*************/
        if(!empty($request->file('documents'))){
            foreach ($request->file('documents') as $file) {
                $name = $file->getClientOriginalName();
                $file_name = 'adspot-'.$lastID.'-'.uniqid().'-'.$name;
                if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                    $document = new Document;
                    $document->table_name = 246;
                    $document->document_name = $file_name;
                    $document->table_id = $lastID;
                    $document->save();
                }
            }
        }

        return "Spot Added.";
    }

    public function getUpdate($id){
        $adspot = Adspot::with(
        	'adbuy_relation',
            'brand_relation',
        	'channel_relation',
            'medium_relation',
            'frequency_relation',
            'size_relation',
            'orientation_relation',
            'status_relation',
            'document_relation'
        )->find($id);
        return view('marketing.adspot.form', ['adspot' => $adspot]);
    }

    public function postUpdate(Request $request, $id){
    	/************validate input*************/
        $validator = Validator::make($request->all(), [
                'AdbuyID' => 'required',
                'ChannelID' => 'required'
            ],[
                'AdbuyID.required' => 'Adbuy field is required.',
                'ChannelID.required' => 'Channel field is required.'
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        $user = Auth::user()->id;
        $adspot = Adspot::find($id);
        $adspot->AdbuyID 			= $request->AdbuyID == "null" ? NULL : $request->AdbuyID;
        $adspot->ChannelID 			= $request->ChannelID == "null" ? NULL : $request->ChannelID;
        $adspot->medium 			= $request->medium == "null" ? NULL : $request->medium;
        $adspot->frequency 			= $request->frequency == "null" ? NULL : $request->frequency;
        $adspot->size 				= $request->size == "null" ? NULL : $request->size;
        $adspot->orientation 		= $request->orientation == "null" ? NULL : $request->orientation;
        $adspot->status 			= $request->status == "null" ? NULL : $request->status;
        $adspot->bluemine 			= $request->bluemine == "null" ? NULL : $request->bluemine;
        $adspot->issue 				= $request->issue == "null" ? NULL : $request->issue;
        $adspot->place 				= $request->place == "null" ? NULL : $request->place;
        $adspot->startdate 			= $request->startdate == "null" ? NULL : $request->startdate;
        $adspot->enddate 			= $request->enddate == "null" ? NULL : $request->enddate;
        $adspot->insertion_due 		= $request->insertion_due == "null" ? NULL : $request->insertion_due;
        $adspot->creative_due 		= $request->creative_due == "null" ? NULL : $request->creative_due;
        $adspot->msrp 				= $request->msrp == "null" ? NULL : $request->msrp;
        $adspot->invoice_amount 	= $request->invoice_amount == "null" ? NULL : $request->invoice_amount;
        $adspot->audited_reach 		= $request->audited_reach == "null" ? NULL : $request->audited_reach;
        $adspot->cpm 				= $request->cpm == "null" ? NULL : $request->cpm;
        $adspot->roi 				= $request->roi == "null" ? NULL : $request->roi;
        $adspot->quality_factor 	= $request->quality_factor == "null" ? NULL : $request->quality_factor;
        $adspot->campaign 			= $request->campaign == "null" ? NULL : $request->campaign;
        $adspot->product 			= $request->product == "null" ? NULL : $request->product;
        $adspot->BrandID 			= $request->BrandID == "null" ? NULL : $request->BrandID;
        $adspot->phone_used 		= $request->phone_used == "null" ? NULL : $request->phone_used;
        $adspot->email_used 		= $request->email_used == "null" ? NULL : $request->email_used;
        $adspot->link_used 			= $request->link_used == "null" ? NULL : $request->link_used;
        $adspot->uid_modified 		= $user;
        $adspot->save();

        /************upload documents*************/
        if(!empty($request->file('documents'))){
            foreach ($request->file('documents') as $file) {
                $name = $file->getClientOriginalName();
                $file_name = 'adspot-'.$id.'-'.uniqid().'-'.$name;
                if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                    $document = new Document;
                    $document->table_name = 246;
                    $document->document_name = $file_name;
                    $document->table_id = $id;
                    $document->save();
                }
            }
        }

        return "Spot Updated.";
    }

    public function view($id){
        $adspot = Adspot::with(
            'adbuy_relation',
            'brand_relation',
            'channel_relation',
            'medium_relation',
            'frequency_relation',
            'size_relation',
            'orientation_relation',
            'status_relation',
            'document_relation',
            'changes_relation.changes_detail_relation',
            'changes_relation.user_relation'
        )->find($id);
        return view('marketing.adspot.view', ['adspot' => $adspot]);
    }

    public function delete($id){
        $user = Auth::user()->id; 
        $adspot = Adspot::find($id);
        $adspot->active = 0;
        $adspot->uid_modified = $user;
        $adspot->save();
        return 'Deleted.';
    }
}
