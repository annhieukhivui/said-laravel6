<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarketingDashboard extends Controller
{
    public function media_relation(){
    	return view('marketing.dashboard.media-relation');
    }

    public function media_planning(){
    	return view('marketing.dashboard.media-planning');
    }
}
