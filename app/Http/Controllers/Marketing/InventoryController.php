<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Models\Marketing\Inventory;
use App\Models\Marketing\InventoryTransaction;
use App\Models\Marketing\Asset;


class InventoryController extends Controller
{
	

    public function index(){
        $data = Inventory::with([
            'product_relation',
            'asset_relation',
            'asset_relation.transaction_relation'
        ])->where('active', '1')->orderBy('id', 'desc')->get();
        return view('marketing.inventory.index', ['data' => $data]);
    }

    public function checkSerial(Request $request){
    	$asset = Asset::with([
    		'transaction_relation'
    	])->where([
    		['InventoryID', '=', $request->inventory_id],
    		['serial', '=', $request->serial]
    	])->first();
    	if($asset){
    		return $asset;
    	}
    	else{
    		return 0;
    	}
    }

    public function getAdd(){
    	$user = Auth::user()->firstname.' '.Auth::user()->firstname; 
     	return view('marketing.inventory.form', ['user' => $user]);
    }

    public function postAdd(Request $request){
     	
     	/************validate input*************/
        $validator = Validator::make($request->all(), [
                'is_asset' => 'required',
                'inventory_id' => 'required',
                'transaction_action' => 'required',
                'quantity' => 'required'
            ],[
                'is_asset.required' => 'Asset field is required.',
                'inventory_id.required' => 'Product field is required.',
                'transaction_action.required' => 'Transaction field is required.',
                'quantity.required' => 'Quantity field is required.'
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        $user = Auth::user()->id; 
        $transaction = new InventoryTransaction;
        $transaction->InventoryID 		= $request->inventory_id;
        $transaction->is_asset       	= $request->is_asset == 175 ? 1 : 0;
        $transaction->serial        	= $request->serial;
        $transaction->quantity      	= $request->quantity;
        $transaction->ns_location   	= $request->ns_location;
        $transaction->ns_order_number	= $request->ns_order_number;
        $transaction->color          	= $request->color;
        $transaction->kit          		= $request->kit;

        $transaction->TrAction     		= $request->transaction_action;
        $transaction->TrDate     		= $request->transaction_date;
        $transaction->owner          	= $request->owner;
        $transaction->note          	= $request->note;
        if($request->transaction_action == 172){
        	$transaction->location      	= $request->location;
        }
        if($request->transaction_action == 173){
        	$transaction->out_to_choice   	= $request->send_to;
	        $transaction->out_to_value   	= $request->send_to_value;
	        $transaction->out_to_term       = $request->term;
        }
        
        $transaction->uid_created  	= $user;
        $transaction->uid_modified 	= $user;
        $transaction->save();
        $lastID = $transaction->id;
     	
     	if($request->is_asset == 175){
     		$asset = Asset::where([
	    		['InventoryID', '=', $request->inventory_id],
	    		['serial', '=', $request->serial]
	    	])->first();
	    	if(!$asset){
	    		$asset = new Asset;
	    		$asset->InventoryID = $request->inventory_id;
	    		$asset->serial = $request->serial;
	    		$asset->uid_created = $user;
	    	}
	    	
            if($request->transaction_action == 172){
                $asset->reserved = 0;
            }

	    	$asset->transaction_id = $lastID;
	    	$asset->uid_modified = $user;
	    	$asset->save();
     	}

     	$this->fullUpdate($request->inventory_id);

     	return "Transaction Created.";
    }

    public function getUpdate($id){
        $transaction = InventoryTransaction::with([
            'inventory_relation.product_relation',
            'action_relation',
            'out_to_relation'
        ])->find($id);
        return view('marketing.inventory.form', ['transaction' => $transaction]);
    }

    public function postUpdate(Request $request, $id){

        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'is_asset' => 'required',
                'inventory_id' => 'required',
                'transaction_action' => 'required',
                'quantity' => 'required'
            ],[
                'is_asset.required' => 'Asset field is required.',
                'inventory_id.required' => 'Product field is required.',
                'transaction_action.required' => 'Transaction field is required.',
                'quantity.required' => 'Quantity field is required.'
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        $user = Auth::user()->id; 
        $transaction = InventoryTransaction::find($id);
        $transaction->InventoryID       = $request->inventory_id;
        $transaction->is_asset          = $request->is_asset == 175 ? 1 : 0;
        $transaction->serial            = $request->serial == 'null' ? NULL : $request->serial;
        $transaction->quantity          = $request->quantity == 'null' ? NULL : $request->quantity;
        $transaction->ns_location       = $request->ns_location == 'null' ? NULL : $request->ns_location;
        $transaction->ns_order_number   = $request->ns_order_number == 'null' ? NULL : $request->ns_order_number;
        $transaction->color             = $request->color == 'null' ? NULL : $request->color;
        $transaction->kit               = $request->kit == 'null' ? NULL : $request->kit;

        $transaction->TrAction          = $request->transaction_action == 'null' ? NULL : $request->transaction_action;
        $transaction->TrDate            = $request->transaction_date == 'null' ? NULL : $request->transaction_date;
        $transaction->owner             = $request->owner == 'null' ? NULL : $request->owner;
        $transaction->note              = $request->note == 'null' ? NULL : $request->note;
        if($request->transaction_action == 172){
            $transaction->location          = $request->location == 'null' ? NULL : $request->location;
        }
        if($request->transaction_action == 173){
            $transaction->out_to_choice     = $request->send_to == 'null' ? NULL : $request->send_to;
            $transaction->out_to_value      = $request->send_to_value == 'null' ? NULL : $request->send_to_value;
            $transaction->out_to_term       = $request->term == 'null' ? NULL : $request->term;
        }
        
        $transaction->uid_modified  = $user;
        $transaction->save();
        
        if($request->is_asset == 175){
            $asset = Asset::where([
                ['InventoryID', '=', $request->inventory_id],
                ['serial', '=', $request->serial]
            ])->first();
            if(!$asset){
                $asset = new Asset;
                $asset->InventoryID = $request->inventory_id;
                $asset->serial = $request->serial;
                $asset->uid_created = $user;
            }
            if($request->transaction_action == 172){
                $asset->reserved = 0;
            }
            $asset->transaction_id = $id;
            $asset->uid_modified = $user;
            $asset->save();
        }

        $this->fullUpdate($request->inventory_id);

        return "Transaction Updated.";
    }

    public function view($id){
    	$inventory = Inventory::with([
            'product_relation', 
            'asset_relation',
            'asset_relation.transaction_relation',
            'asset_relation.transaction_relation.action_relation',
            'asset_relation.transaction_relation.out_to_relation',
            'transaction_relation',
            'transaction_relation.action_relation',
            'transaction_relation.out_to_relation'
        ])->find($id);
        return view('/marketing/inventory/view', ['inventory' => $inventory]);
    }

    public function getData(){
    	return Inventory::with([
            'product_relation', 
        ])->get();
    }

    public function fullUpdate($id){
    	$serials = InventoryTransaction::distinct('serial')->where([
            ['is_asset', '=', 1],
            ['InventoryID', '=', $id],
        ])->select(['serial'])->get();
        
        $asset = 0;
        foreach ($serials as $serial) {
            $action = InventoryTransaction::where([
                ['serial', '=', $serial->serial],
                ['InventoryID', '=', $id],
            ])->orderBy('id', 'desc')->select(['id', 'TrAction'])->first();
            if($action->TrAction == 172){
                $asset += 1;
            }
        }

        $non_asset_total = InventoryTransaction::where([
            ['is_asset', '=', 0],
            ['InventoryID', '=', $id],
        ])->sum('quantity');

        $non_asset_out = InventoryTransaction::where([
            ['is_asset', '=', 0],
            ['InventoryID', '=', $id],
            ['TrAction', '=', '173']
        ])->sum('quantity');

        $non_asset_in = $non_asset_total - $non_asset_out < 0 ? 0 : $non_asset_total - $non_asset_out;

        $sum = $asset + $non_asset_in;

    	$inventory = Inventory::find($id);
    	$inventory->available = $sum;
    	$inventory->onhand = $sum;
    	$inventory->save();
    }
}
