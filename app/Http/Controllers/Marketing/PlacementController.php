<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Notifications\SlackPlacement;
use App\Http\Controllers\Controller;
use App\Models\Marketing\Placement;
use App\Models\Shared\Document;

class PlacementController extends Controller
{
    public function index() {  
    	$data = Placement::with([
        	'channel_relation',
            'contact_relation.general_contact_relation',
        	'product_order_relation',
        	'product_order_relation.inventory_relation.product_relation',
        	'type_relation'
        ])->where('active', '1')->orderBy('id', 'desc')->get();
    	return view('marketing.placement.index', ['placements' => $data]);
    }

    public function getAdd() {  
    	return view('marketing.placement.add');
    }

    public function postAdd(Request $request){

        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'channel_id' => 'required',
                'contact_id' => 'required',
                'type' => 'required',
            ],[
                'channel_id.required' => 'Channel field is required.',
                'contact_id.required' => 'Contact field is required.',
                'type.required'       => 'Type field is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        /************main*************/        
        $user = Auth::user()->id; 
        
        $placement = new Placement;
        $placement->ChannelID 		= $request->channel_id;
        $placement->ContactID     	= $request->contact_id;
        $placement->platform     	= $request->platform;
        $placement->author       	= $request->author;
        $placement->type        	= $request->type;
        $placement->link      		= $request->link;
        $placement->ave          	= $request->ave;
        $placement->pl_date      	= $request->pl_date;
        $placement->note          	= $request->note;
        $placement->uid_created  	= $user;
        $placement->uid_modified 	= $user;
        $placement->save();
        $lastID = $placement->id;

        /************upload documents*************/
        if(!empty($request->file('documents'))){
            foreach ($request->file('documents') as $file) {
                $name = $file->getClientOriginalName();
                $file_name = 'placement-'.$lastID.'-'.uniqid().'-'.$name;
                if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                    $document = new Document;
                    $document->table_name = 160;
                    $document->document_name = $file_name;
                    $document->table_id = $lastID;
                    $document->save();
                }
            }
        }

        $placement = Placement::find($lastID);
        if(count(json_decode($request->order_ids)) > 0){
            foreach (json_decode($request->order_ids) as $order) {
            	$placement->product_order_relation()->attach($order->id);
            }
        }

        $this->slack($lastID);

        return "Placement Added.";
    }

    public function getUpdate($id){
    	$data = Placement::with([
        	'channel_relation',
        	'contact_relation',
            'contact_relation.general_contact_relation',
        	'product_order_relation',
        	'product_order_relation.inventory_relation.product_relation',
        	'type_relation', 
        	'document_relation'
        ])->find($id);
    	return view('marketing.placement.update', ['placement' => $data]);
    }

    public function postUpdate(Request $request, $id){

        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'channel_id' => 'required',
                'contact_id' => 'required',
                'type' => 'required',
            ],[
                'channel_id.required' => 'Channel field is required.',
                'contact_id.required' => 'Contact field is required.',
                'type.required'       => 'Type field is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        /************main*************/        
        $user = Auth::user()->id; 
        
        $placement = Placement::find($id);
        $placement->ChannelID 		= $request->channel_id;
        $placement->ContactID     	= $request->contact_id == "undefined" ? NULL : $request->contact_id;
        $placement->platform     	= $request->platform == "null" ? NULL : $request->platform;
        $placement->author       	= $request->author == "null" ? NULL : $request->author;
        $placement->type        	= $request->type == "null" ? NULL : $request->type;
        $placement->link      		= $request->link == "null" ? NULL : $request->link;
        $placement->ave          	= $request->ave == "null" ? NULL : $request->ave;
        $placement->pl_date      	= $request->pl_date == "null" ? NULL : $request->pl_date;
        $placement->note          	= $request->note == "null" ? NULL : $request->note;
        $placement->uid_modified 	= $user;
        $placement->save();

        /************upload documents*************/
        if(!empty($request->file('documents'))){
            foreach ($request->file('documents') as $file) {
                $name = $file->getClientOriginalName();
                $file_name = 'placement-'.$id.'-'.uniqid().'-'.$name;
                if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                    $document = new Document;
                    $document->table_name = 160;
                    $document->document_name = $file_name;
                    $document->table_id = $id;
                    $document->save();
                }
            }
        }
        
        if(count(json_decode($request->order_ids)) > 0){
        	$orderIDs = array_map(function($order){ return $order->id; } , json_decode($request->order_ids));
            $placement->product_order_relation()->sync($orderIDs);
        }
        else{
            $placement->product_order_relation()->sync([]);
        }
        
        return "Placement Updated.";

    }

    public function view($id){
    	$data = Placement::with([
        	'channel_relation',
        	'contact_relation',
            'contact_relation.general_contact_relation',
        	'product_order_relation',
        	'product_order_relation.inventory_relation.product_relation',
        	'type_relation', 
        	'document_relation'
        ])->find($id);
    	return view('marketing.placement.view', ['placement' => $data]);
    }

    public function delete(Placement $placement){
    	$user = Auth::user()->id;
        $placement->active = 0;
        $placement->uid_modified = $user;
        if($placement->save()){
            return "Deleted.";
        }
        else{
            return "Failed.";
        }    
    }

    
    
    public function getData(){
    	$data = Placement::with([
        	'channel_relation',
        	'type_relation'
        ])->where('active', '1')->orderBy('id', 'desc')->get();
        return $data;
    }

    public function slack($id){

        $placement = Placement::with([
            'channel_relation',
            'contact_relation',
            'contact_relation.general_contact_relation',
            'type_relation',
        ])->find($id);

        $content = "----------- New Placement -----------\n";
        $content .= "Channel: ".$placement->channel_relation->name."\n";
        $content .= "Platform: ".$placement->platform."\n";
        $content .= "Contact: ".$placement->contact_relation->general_contact_relation->firstname." ".$placement->contact_relation->general_contact_relation->lastname."\n";
        $content .= "Type: ".$placement->type_relation->description."\n";
        $content .= "Date: ".$placement->pl_date."\n";
        $content .= "Link: ".$placement->link;

        $buttons = array();
        $buttons[] = array("text"=>"Webb App Link", "type"=>"button", "url"=>config('app.url')."/marketing/placement/view/".$id);

        $attachment = array();
        $attachment['text'] = "";
        $attachment['actions'] = $buttons;

        $data = array();
        $data['text'] = $content;
        $data['attachments'] = array($attachment);

        $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, 'https://hooks.slack.com/services/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');
        curl_setopt($ch, CURLOPT_URL, 'https://hooks.slack.com/services/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'); //test channel web hook
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        
    }

   
}
