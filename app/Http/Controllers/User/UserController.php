<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Admin\SLMKUser;

class UserController extends Controller
{
    
    public function getProfile($id)
    {	
    	$user = Auth::user();
        return view('/user/profile', ['user' => $user]);
    }

    public function postProfile(Request $request, $id)
    {	
    	/************validate input*************/
    	$validator = Validator::make($request->all(), [
            	'firstname' => 'required',
            	'lastname' => 'required',
        	],[
        		'firstname.required' => 'First Name field is required.',
	            'lastname.required' => 'Last Name field is required.',
        	]
    	);
    	if($validator->fails()){

            return response()->json(['errors'=>$validator->errors()],422);
        }

        /************check match password*************/
        if($request->create_password == 'true' && $request->password != $request->confirmpassword){
        	return response()->json(['errors'=> ['Password not Match']], 422);
        }

        $user = SLMKUser::find($id);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->title = $request->title;
        if($request->create_password == 'true'){
        	$user->password = Hash::make($request->password);
        }
        $user->save();
        
    	return "User Updated.";
    }
}
