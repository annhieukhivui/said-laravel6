<?php

namespace App\Http\Controllers\Accounting;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\Controller;
use App\Models\Accounting\Expense;
use App\Models\Accounting\ExpenseDistribution;
use App\Models\Accounting\JMPPeriod;
use App\Models\Accounting\JMPExpense;
use App\Models\Shared\Document;
use App\Models\Shared\Note;



class ExpenseController extends Controller
{
    public function index() {    
        $allexpense = Expense::with([
            'category_relation', 
            'vendor_relation', 
            'occurrence_relation',
            'expense_jmp_relation',
        ])->where('active', '1')->orderBy('id', 'desc')->get();
        return view('accounting.expense.index', ['allexpense' => $allexpense]);
    }

    public function getAdd(){
        return view('accounting.expense.add');
    }

    public function postAdd(Request $request){
        $user = Auth::user()->id;

        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'vendor_id' => 'required',
                'category' => 'required',
            ],[
                'vendor_id.required' => 'Vendor is required.',
                'category.required' => 'Category is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        /************add new expense*************/
        $expense = new Expense;
        $expense->vendor_id         = $request->vendor_id;
        $expense->category          = $request->category;
        $expense->occurrence_id     = $request->occurrence_id;
        $expense->invoice           = $request->invoice;
        $expense->invoice_date      = $request->invoice_date;
        $expense->ns_bill           = $request->ns_bill;
        $expense->who_paid          = $request->who_paid;
        $expense->from_account      = $request->from_account;
        $expense->amount_paid       = $request->amount_paid;
        $expense->tax_paid          = $request->tax_paid;
        $expense->tip_paid          = $request->tip_paid;
        $expense->description       = $request->description;
        $expense->uid_created       = $user;
        $expense->uid_modified      = $user;
        $expense->save();
        $lastID = $expense->id;      

        /************brand distribution*************/
        if(count(json_decode($request->brand_id)) > 0){
            foreach (json_decode($request->brand_id) as $brand) {
                $distribution = new ExpenseDistribution;
                $distribution->expense_id   = $lastID;
                $distribution->brand_id     = $brand->id;
                $distribution->percent      = $brand->percent;
                $distribution->save();
            }
        }
        if($request->is100 != 0){
            $distribution = new ExpenseDistribution;
            $distribution->expense_id   = $lastID;
            $distribution->brand_id     = 1;
            $distribution->percent      = $request->is100;
            $distribution->save();
        }

        /************associate expense to JMP*************/
        if($request->occurrence_id && $request->invoice_date ){
            if(count(json_decode($request->brand_id)) > 0){
                foreach (json_decode($request->brand_id) as $brand) {
                    $jmp = JMPPeriod::where('brand_id', '=', $brand->id)
                                    ->whereDate('from_date', '<=', $request->invoice_date)
                                    ->whereDate('to_date', '>=', $request->invoice_date)
                                    ->first();
                    if($jmp){
                        $jmpExpense = new JMPExpense;
                        $jmpExpense->expense_id = $lastID;
                        $jmpExpense->period_id = $jmp->id;
                        $jmpExpense->percent_submit = $brand->percent;
                        $jmpExpense->uid_created = $user;
                        $jmpExpense->uid_modified = $user;
                        $jmpExpense->save();
                    }
                }   
            }
        }

        /************upload documents*************/
        if(!empty($request->file('documents'))){
            foreach ($request->file('documents') as $file) {
                $name = $file->getClientOriginalName();
                $file_name = 'expense-'.$lastID.'-'.uniqid().'-'.$name;
                if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                    $document = new Document;
                    $document->table_name = 121;
                    $document->document_name = $file_name;
                    $document->table_id = $lastID;
                    $document->save();
                }
            }
        }

        return "Expense Added.";
        
    }

    public function getUpdate($id){
        $expense = Expense::with([
            'category_relation',
            'document_relation',
            'expense_distribution_relation',
            'expense_distribution_relation.distribution_brand_relation', 
        ])->find($id);
        // dd($expense);
        return view('accounting.expense.update', ['expense' => $expense]);
    }

    public function postUpdate(Request $request, $id){

        $user = Auth::user()->id;

        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'vendor_id' => 'required',
                'category' => 'required',
            ],[
                'vendor_id.required' => 'Vendor is required.',
                'category.required' => 'Category is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        /************add new expense*************/
        $expense = Expense::find($id);

        $expense->vendor_id         = $request->vendor_id;
        $expense->category          = $request->category;
        $expense->occurrence_id     = $request->occurrence_id == "null" ? NULL : $request->occurrence_id;
        $expense->invoice           = $request->invoice == "null" ? NULL : $request->invoice;
        $expense->invoice_date      = $request->invoice_date == "null" ? NULL : $request->invoice_date;
        $expense->ns_bill           = $request->ns_bill == "null" ? NULL : $request->ns_bill;
        $expense->who_paid          = $request->who_paid == "null" ? NULL : $request->who_paid;
        $expense->from_account      = $request->from_account == "null" ? NULL : $request->from_account;
        $expense->amount_paid       = $request->amount_paid == "null" ? NULL : $request->amount_paid;
        $expense->tax_paid          = $request->tax_paid == "null" ? NULL : $request->tax_paid;
        $expense->tip_paid          = $request->tip_paid == "null" ? NULL : $request->tip_paid;
        $expense->description       = $request->description == "null" ? NULL : $request->description;
        $expense->uid_modified      = $user;
        $expense->save();
  

        /************brand distribution*************/
        ExpenseDistribution::where('expense_id', $id)->delete();
        if(count(json_decode($request->brand_id)) > 0){
            foreach (json_decode($request->brand_id) as $brand) {
                $distribution = new ExpenseDistribution;
                $distribution->expense_id   = $id;
                $distribution->brand_id     = $brand->id;
                $distribution->percent      = $brand->percent;
                $distribution->save();
            }
        }
        if($request->is100 != 0){
            $distribution = new ExpenseDistribution;
            $distribution->expense_id   = $id;
            $distribution->brand_id     = 1;
            $distribution->percent      = $request->is100;
            $distribution->save();
        }

        /************associate expense to JMP*************/
        if($request->occurrence_id && $request->invoice_date ){
            if(count(json_decode($request->brand_id)) > 0){
                foreach (json_decode($request->brand_id) as $brand) {
                    $jmp = JMPPeriod::where('brand_id', '=', $brand->id)
                                    ->whereDate('from_date', '<=', $request->invoice_date)
                                    ->whereDate('to_date', '>=', $request->invoice_date)
                                    ->first();
                    if($jmp){

                        /***********update existing JMP-Expense***********/
                        $jmpExpense = JMPExpense::where([
                            ['expense_id', '=', $id], 
                            ['period_id', '=', $jmp->id],
                        ])->first();
                        if($jmpExpense){
                            $jmpExpense->percent_submit = $brand->percent;
                            $jmpExpense->uid_modified = $user;
                            $jmpExpense->save();
                        }
                        else{
                            $jmpExpense = new JMPExpense;
                            $jmpExpense->expense_id = $id;
                            $jmpExpense->period_id = $jmp->id;
                            $jmpExpense->percent_submit = $brand->percent;
                            $jmpExpense->uid_created = $user;
                            $jmpExpense->uid_modified = $user;
                            $jmpExpense->save();
                        }
                    }
                }   
            }
        }

        /************upload documents*************/
        if(!empty($request->file('documents'))){
            foreach ($request->file('documents') as $file) {
                $name = $file->getClientOriginalName();
                $file_name = 'expense-'.$id.'-'.uniqid().'-'.$name;
                if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                    $document = new Document;
                    $document->table_name = 121;
                    $document->document_name = $file_name;
                    $document->table_id = $id;
                    $document->save();
                }                
            }
        }

        /************add note*************/
        if($request->note != ''){
            $note = new Note;
            $note->note_on_table = 121;
            $note->note_on_table_id = $id;
            $note->action = 72; // 72 is general note
            $note->detail = $request->note;
            $note->uid_created = $user;
            $note->save();
        }

        return "Expense Updated.";
        
    }

    public function delete($id){
        $user = Auth::user()->id;
        $expense = Expense::find($id);
        $expense->active = 0;
        $expense->uid_modified = $user;
        if($expense->save()){
            return "Deleted.";
        }
        else{
            return "Failed.";
        }    
    }

    public function view($id){
        $expense = Expense::with([
            'vendor_relation', 
            'category_relation',
            'occurrence_relation',
            'expense_jmp_relation',
            'document_relation',
            'expense_distribution_relation',
            'expense_distribution_relation.distribution_brand_relation', 
            'note_relation',
            'note_relation.action_relation',
            'note_relation.user_relation',
        ])->find($id);
        return view('accounting.expense.view', ['expense' => $expense]);
    }

}