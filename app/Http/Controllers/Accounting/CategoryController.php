<?php

namespace App\Http\Controllers\Accounting;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Accounting\Category;

class CategoryController extends Controller
{
    public function index(){
        $data = Category::all();
        return view('accounting.category.index', ['data' => $data]);
    }

    public function submitForm(Request $request){

        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'description' => 'required',
            ],[
                'description.required' => 'Name field is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        $user = Auth::user()->id; 

        if(Category::where('id', $request->id)->exists()){
            $category = Category::find($request->id);
        }
        else{
            $category = new Category;
        }
        $category->description  = $request->description;
        $category->pid          = $request->pid;
        $uid_created            = $user;
        $uid_modified           = $user;
        $category->save();
        return "Item Added/Updated";
    }

    public function delete($id){
        $category = Category::find($id);
        $pid = $category->pid;
        Category::where([
            ['pid', '=', $id],
        ])->update(['pid' => $pid]);
        Category::destroy($id);
        return "Deleted.";
    }

    public function getData(){
        return Category::all();
    }
}