<?php

namespace App\Http\Controllers\Accounting;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use App\Models\Accounting\JMPExpense;


class JMPExpenseController extends Controller
{
    public function index() {    
    	$jmpexpense = JMPExpense::with([
        	'expense_relation', 
        	'expense_relation.category_relation', 
        	'expense_relation.vendor_relation', 
        	'jmpperiod_relation', 
        ])->where('active', '1')->orderBy('id', 'desc')->get(['id', 'expense_id', 'period_id', 'percent_submit', 'percent_approval']);
        return view('accounting.jmp-expense.index', ['jmpexpense' => $jmpexpense]);
    }

    public function getUpdate($id){
    	$jmpexpense = JMPExpense::with([
    		'expense_relation', 
        	'expense_relation.category_relation', 
        	'expense_relation.vendor_relation', 
        	'jmpperiod_relation', 
        ])->find($id);
        return view('accounting.jmp-expense.update', ['jmpexpense' => $jmpexpense]);
    }

    public function postUpdate(Request $request, $id){
    	$user = Auth::user()->id;

    	/************validate input*************/
    	$validator = Validator::make($request->all(), [
            	'approval' => 'required',
        	],[
        		'approval.required' => 'Approval field is required.',
        	]
    	);
    	if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

    	$jmpexpense = JMPExpense::find($id);
    	$jmpexpense->percent_approval = $request->approval;
    	$jmpexpense->uid_modified = $user;
    	$jmpexpense->save();
    	return "JMP Expense Updated.";
    }

    public function delete($id){
    	$user = Auth::user()->id;
        $jmpperiod = JMPExpense::find($id);
        $jmpperiod->active = 0;
        $jmpperiod->uid_modified = $user;
        if($jmpperiod->save()){
            return "Deleted.";
        }
        else{
            return "Failed.";
        }    
    }
}
