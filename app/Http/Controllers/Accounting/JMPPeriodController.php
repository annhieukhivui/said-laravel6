<?php

namespace App\Http\Controllers\Accounting;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\Controller;
use App\Models\Accounting\JMPPeriod;
use App\Models\Shared\Document;

class JMPPeriodController extends Controller
{
    public function index() {    
    	$jmpperiod = JMPPeriod::with([
        	'brand_relation', 
        	'status_relation', 
        ])->where('active', '1')->orderBy('id', 'desc')->get(['id', 'period', 'period_name', 'from_date', 'to_date', 'submission_date', 'brand_id', 'status']);
        return view('accounting.jmp-period.index', ['jmpperiod' => $jmpperiod]);
    }

    public function getAdd(){
        return view('accounting.jmp-period.add');
    }

    public function postAdd(Request $request){
        $user = Auth::user()->id;

        /************validate input*************/
    	$validator = Validator::make($request->all(), [
            	'brand_id' => 'required',
            	'period' => 'required',
            	'period_name' => 'required',
        	],[
        		'brand_id.required' => 'Brand field is required.',
	            'period.required' => 'Period field is required.',
	            'period_name.required' => 'Period Name field is required.',
        	]
    	);
    	if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        $period = new JMPPeriod;
        $period->period				= $request->period;
        $period->period_name    	= $request->period_name;
        $period->from_date    		= $request->from_date;
        $period->to_date           	= $request->to_date;
        $period->submission_date 	= $request->submission_date;
        $period->brand_id         	= $request->brand_id;
        $period->status       		= $request->status;
        $period->uid_created    	= $user;
        $period->uid_modified    	= $user;
        $period->save();
        $lastID = $period->id;

        /************upload documents*************/
        if(!empty($request->file('documents'))){
            foreach ($request->file('documents') as $file) {
                $name = $file->getClientOriginalName();
                $file_name = 'jmp-period-'.$lastID.'-'.uniqid().'-'.$name;
                if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                    $document = new Document;
                    $document->table_name = 122;
                    $document->document_name = $file_name;
                    $document->table_id = $lastID;
                    $document->save();
                }
            }
        }

        return "Period Added.";

    }

    public function getUpdate($id){
    	$period = JMPPeriod::with([
    		'brand_relation',
    		'status_relation',
    		'document_relation',
        ])->find($id);
        return view('accounting.jmp-period.update', ['jmpperiod' => $period]);
    }

    public function postUpdate(Request $request, $id){
        $user = Auth::user()->id;

        /************validate input*************/
    	$validator = Validator::make($request->all(), [
            	'brand_id' => 'required',
            	'period' => 'required',
            	'period_name' => 'required',
        	],[
        		'brand_id.required' => 'Brand field is required.',
	            'period.required' => 'Period field is required.',
	            'period_name.required' => 'Period Name field is required.',
        	]
    	);
    	if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        $period = JMPPeriod::find($id);
        $period->period				= $request->period;
        $period->period_name    	= $request->period_name;
        $period->from_date    		= $request->from_date == "null" ? NULL : $request->from_date;
        $period->to_date           	= $request->to_date == "null" ? NULL : $request->to_date;
        $period->submission_date 	= $request->submission_date == "null" ? NULL : $request->submission_date;
        $period->brand_id         	= $request->brand_id;
        $period->status       		= $request->status == "null" ? NULL : $request->status;
        $period->uid_created    	= $user;
        $period->uid_modified    	= $user;
        $period->save();

        /************upload documents*************/
        if(!empty($request->file('documents'))){
            foreach ($request->file('documents') as $file) {
                $name = $file->getClientOriginalName();
                $file_name = 'jmp-period-'.$id.'-'.uniqid().'-'.$name;
                if(Storage::disk('s3')->put('documents/'.$file_name, file_get_contents($file))){
                    $document = new Document;
                    $document->table_name = 122;
                    $document->document_name = $file_name;
                    $document->table_id = $id;
                    $document->save();
                }
            }
        }

        return "Period Updated.";

    }

    public function view($id){
    	$period = JMPPeriod::with([
    		'brand_relation',
    		'status_relation',
    		'document_relation',
        ])->find($id);
        return view('accounting.jmp-period.view', ['jmpperiod' => $period]);
    }

    public function delete($id){
    	$user = Auth::user()->id;
        $jmpperiod = JMPPeriod::find($id);
        $jmpperiod->active = 0;
        $jmpperiod->uid_modified = $user;
        if($jmpperiod->save()){
            return "Deleted.";
        }
        else{
            return "Failed.";
        }    
    }
}
