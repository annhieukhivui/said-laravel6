<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index()
    {
        return view('cms/product/index');
    }

    public function getUpdate($brand, $nsid)
    {
        return view('cms/product/form', ['brand' => $brand, 'nsid' => $nsid]);
    }
    
}
