<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Models\Admin\SLMKUser;
use Illuminate\Support\Facades\DB;

use App\Models\Marketing\Inventory;
use App\Models\Marketing\InventoryTransaction;
use App\Mail\ProductOrderSendMail;


class UserController extends Controller
{

    public function index()
    {
        return view('admin/user/index');
    }

    public function test()
    {         
        /*$data = array();
        $data['text'] = "You have Contact(s) need to follow up today";
        $data['channel'] = 'UB2GSD9UP';
        $data['username'] = "Friendly Reminder";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://slack.com/api/chat.postMessage');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = 'Authorization: Bearer xoxp-38358716340-376570451975-646541127031-7fd537a36450600fce7fcbd61abce257';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);*/

        // $buttons = array();
        // $buttons[] = array("text"=>"Webb App Link", "type"=>"button", "url"=>"http://said.home/marketing/placement/view/");

        // $attachment = array();
        // $attachment['text'] = "";
        // $attachment['actions'] = $buttons;

        // $data = array();
        // $data['text'] = 'test';
        // $data['attachments'] = array($attachment);

        // $ch = curl_init();
        // // curl_setopt($ch, CURLOPT_URL, 'https://hooks.slack.com/services/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');
        // curl_setopt($ch, CURLOPT_URL, 'https://hooks.slack.com/services/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'); //test channel web hook
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        // curl_setopt($ch, CURLOPT_POST, 1);

        // $headers = array();
        // $headers[] = 'Content-Type: application/json';
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // $result = curl_exec($ch);
        // if (curl_errno($ch)) {
        //     echo 'Error:' . curl_error($ch);
        // }
        // curl_close ($ch);

        // $results = DB::select( DB::raw("SELECT t.id, concat(c.CFirstName,' ',c.CLastName) as name FROM te_product_order t LEFT JOIN te_contacts c ON c.CID = t.ContactID") );
        // $contact = DB::select(DB::raw("SELECT mc.id, concat(c.firstname,' ',c.lastname) as name FROM mk_contact mc LEFT JOIN contact c ON c.id = mc.ContactID"));

        // $arraycontact = array();
        // foreach ($contact as $c) {
        //     $arraycontact[$c->id] = $c->name;
        // }
        // foreach ($results as $order) {
        //     $cid = array_search($order->name,$arraycontact);
        //     $test = DB::update('update te_product_order set newcontact = ? where id = ?', [$cid, $order->id]);
        // }
        
    }

    // public function getData(){
    //     $data = SLMKUser::with([
    //         'user_department_relation',
    //     ])->where('Status', '1')->get();
    //     return $data;
    // }
    // public function getData(){
    //     $data = SLMKUser::with([
    //         'user_marketing_department_relation',
    //     ])->where('Status', '1')->get();
    //     dd($data);
    //     return $data;
    // }


    
}
