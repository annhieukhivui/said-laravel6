<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin\SLMKUser;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     // $this->middleware('guest')->except('logout');
    // }

    public function getLogin()
    {
        return view('login');
    }

    public function postLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
            // return redirect('/login')->withErrors($validator);
        }
        else{
            $credentials = [
                'username' => $request->username,
                'password' => $request->password,
                'active' => 1,
            ];

            if(Auth::attempt($credentials)){
                return redirect('/');
            }
            else{
                return redirect()->back()->with('login_failed', 'Login failed. Check your credentials.');
            }
        }
    }
    
    public function getLogout()
    {
        Auth::logout();
        return redirect('/login');
    }

    
}
