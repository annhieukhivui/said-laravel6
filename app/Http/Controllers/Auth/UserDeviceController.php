<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DeviceAuth;
use Session;
use Auth;

class UserDeviceController extends Controller
{
    const MAX_AUTHORIZATION_TRIES = 3;

    public function getEmailSent()
    {
    	return view('auth.device.email-sent');
    }

    public function postAuthorizeDevice(Request $request) {
        if($request->has('code')) {
            if($this->canTry()) {
                $this->incrementTryCount();
                $code = strtoupper($request->code);
                DeviceAuth::authorizeEmailCode($code);
                return redirect('/');
            }
            cache()->pull($this->tryKey());
            return redirect('/logout');
        }
    }

    private function canTry()
    {
        return cache($this->tryKey()) <= self::MAX_AUTHORIZATION_TRIES;   
    }

    private function tryKey() 
    {
        return Auth::user()->id.'_email_code_try_count';
    }

    private function incrementTryCount()
    {
        if(!cache()->has($this->tryKey())) {
            cache()->put($this->tryKey(), 1, now()->addMinutes(15));
        } else {
            cache()->increment($this->tryKey());
        }
        // dd(cache($this->tryKey()));
    }

    public function getAuthorizeDevice(Request $request, $code)
    {
    	DeviceAuth::authorizeEmailCode($code);
    	return redirect('/');
    }
}
