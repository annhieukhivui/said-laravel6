<?php

namespace App\Http\Controllers\Shared;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Models\Shared\ListList;

class ListListController extends Controller
{
    public function index(){
        $data = ListList::all();
        // dd($data);
        return view('shared.listlist.index', ['data' => $data]);
    }

    public function submitForm(Request $request){
        /************validate input*************/
        $validator = Validator::make($request->all(), [
                'description' => 'required',
            ],[
                'description.required' => 'Name field is required.',
            ]
        );
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],422);
        }

        $user = Auth::user()->id; 

        if(ListList::where('id', $request->id)->exists()){
            $listlist = ListList::find($request->id);
        }
        else{
            $listlist = new ListList;
        }
        $listlist->description  = $request->description;
        $listlist->pid          = $request->pid;
        $uid_created            = $user;
        $uid_modified           = $user;
        $listlist->save();
        return "Item Added/Updated";
    }

    public function delete($id){
    	$listlist = ListList::find($id);
        $pid = $listlist->pid;
        ListList::where([
            ['pid', '=', $id],
        ])->update(['pid' => $pid]);
        ListList::destroy($id);
        return "Deleted.";
    }

    public function getData(){
        return ListList::all();
    }

    // public function getAllMarketStrategy(){
    //     return ListList::where('pid', '43')->get();
    // }

    // public function getAllShowStatus(){
    //     return ListList::where('pid', '27')->get();
    // }
}
