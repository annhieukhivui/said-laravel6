<?php

namespace App\Http\Controllers\Shared;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\Shared\Document;

class DocumentController extends Controller
{
    public function getDocument($filename){
        if(Storage::disk('s3')->exists('documents/'.$filename)){
            if(!Storage::disk('local')->exists('public/'.$filename)){
                Storage::disk('local')->put(
                    'public/'.$filename, 
                Storage::disk('s3')->get('documents/'.$filename));
            }
            return redirect('storage/'.$filename);
        }
        else{
            return 'File not exist';
        }
    }

    public function delete($id){
        if(Document::where('id', '=', $id)->delete()){
            return "File Deleted.";
        }
        else{
            return "Failed to Delete.";
        }
    }

}
