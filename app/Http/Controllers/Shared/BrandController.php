<?php

namespace App\Http\Controllers\Shared;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Shared\Brand;

class BrandController extends Controller
{
    public function getData(){
        return Brand::where('active', '1')->get();
    }
}
