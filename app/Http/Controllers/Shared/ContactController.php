<?php

namespace App\Http\Controllers\Shared;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Shared\Contact;

class ContactController extends Controller
{
    public function getData(){
        return Contact::all();
    }
}
