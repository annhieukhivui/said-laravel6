<?php

namespace App\Http\Controllers\Shared;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use App\Notifications\SlackTaskDue;

use App\Models\Marketing\Contact;
use App\Models\Marketing\TEProductOrder;

class SlackController extends Controller
{
	private $domain = "http://said.slmk.dev";

    public function taskdue(){

    	$this->product_order_reminder_7days_of_return();
    	$this->product_order_follow_up_today();
    	$this->contact_follow_up_today();
	}


	public function slack_direct_message($data){
	    $ch = curl_init();

	    curl_setopt($ch, CURLOPT_URL, 'https://slack.com/api/chat.postMessage');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_POST, 1);

	    $headers = array();
	    $headers[] = 'Authorization: Bearer xoxp-38358716340-376570451975-646541127031-7fd537a36450600fce7fcbd61abce257';
	    $headers[] = 'Content-Type: application/json';
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	    $result = curl_exec($ch);
	    if (curl_errno($ch)) {
	        echo 'Error:' . curl_error($ch);
	    }
	    curl_close ($ch);
	}

	public function product_order_reminder_7days_of_return(){
		$today = date('Y-m-d');
		$weekafter = date('Y-m-d', strtotime('+7 days'));

		$result = DB::table('te_product_order')
						->leftJoin('user', 'te_product_order.owner', '=', DB::raw('CONCAT(user.firstname," ", user.lastname)'))
						->select(DB::raw('GROUP_CONCAT(te_product_order.id) as TEIDs'), 'user.slack_id')
						->where('te_product_order.expect_return_date', '=', $weekafter)
						->groupBy('user.slack_id')
						->get();
		
		foreach ($result as $item) {
			$teids = explode(',', $item->TEIDs);
    		$buttons = array();
    		for($i = 0; $i < count($teids); $i++){
		        $buttons[] = array("text"=>$teids[$i], "type"=>"button", "url"=>$this->domain."/marketing/product-order/view/".$teids[$i]);
		    }

		    $attachment = array();
		    $attachment['text'] = "";
		    $attachment['actions'] = $buttons;

		    $data = array();
		    $data['text'] = "You have T&E(s) should be returned in a week";
		    $data['channel'] = $item->slack_id;
		    $data['attachments'] = array($attachment);
		    $data['username'] = "Friendly Reminder";
			
			$this->slack_direct_message(json_encode($data));
		}
	}

	public function product_order_follow_up_today(){
		$today = date('Y-m-d');

		$result = DB::table('te_product_order')
						->leftJoin('user', 'te_product_order.owner', '=', DB::raw('CONCAT(user.firstname," ", user.lastname)'))
						->select(DB::raw('GROUP_CONCAT(te_product_order.id) as TEIDs'), 'user.slack_id')
						->where('te_product_order.follow_up_date', '=', $today)
						->groupBy('user.slack_id')
						->get();

		foreach ($result as $item) {
			$teids = explode(',', $item->TEIDs);
    		$buttons = array();
    		for($i = 0; $i < count($teids); $i++){
		        $buttons[] = array("text"=>$teids[$i], "type"=>"button", "url"=>$this->domain."/marketing/product-order/view/".$teids[$i]);
		    }

		    $attachment = array();
		    $attachment['text'] = "";
		    $attachment['actions'] = $buttons;

		    $data = array();
		    $data['text'] = "You have follow up T&E(s) today";
		    $data['channel'] = $item->slack_id;
		    $data['attachments'] = array($attachment);
		    $data['username'] = "Friendly Reminder";
			
			$this->slack_direct_message(json_encode($data));
		}
	}

	public function contact_follow_up_today(){
		$today = date('Y-m-d');

		$result = DB::table('mk_contact')
						->leftJoin('contact', 'mk_contact.ContactID', '=', 'contact.id')
						->leftJoin('user', 'mk_contact.owner', '=', DB::raw('CONCAT(user.firstname," ", user.lastname)'))
						->select(DB::raw('GROUP_CONCAT(mk_contact.id) as CIDs'), 'user.slack_id')
						->where('mk_contact.follow_up_date', '=', $today)
						->groupBy('user.slack_id')
						->get();

		foreach ($result as $item) {
			$cids = explode(',', $item->CIDs);
    		$buttons = array();
    		for($i = 0; $i < count($cids); $i++){
		        $buttons[] = array("text"=>$cids[$i], "type"=>"button", "url"=>$this->domain."/marketing/contact/view/".$cids[$i]);
		    }

		    $attachment = array();
		    $attachment['text'] = "";
		    $attachment['actions'] = $buttons;

		    $data = array();
		    $data['text'] = "You have Contact(s) need to follow up today";
		    $data['channel'] = $item->slack_id;
		    $data['attachments'] = array($attachment);
		    $data['username'] = "Friendly Reminder";
			
			$this->slack_direct_message(json_encode($data));
		}
	}
}
