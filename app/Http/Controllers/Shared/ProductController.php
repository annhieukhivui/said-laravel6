<?php

namespace App\Http\Controllers\Shared;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Shared\Product;

class ProductController extends Controller
{
    public function getData(){
        return Product::where('active', '1')->get();
    }
}
