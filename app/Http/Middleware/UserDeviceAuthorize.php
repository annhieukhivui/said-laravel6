<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;
use Crypt;
use Auth;
use DeviceAuth;

class UserDeviceAuthorize
{
    const COOKIE_KEY = 'SLMK-DID';
    const EMAIL_SENT_ROUTE = 'device-email-sent';
    const CANNOT_AUTHORIZE_ROUTE = 'device-cannot-authorize';
    const UNAUTHORIZED_MESSAGE = 'This device has not been approved to use this application.';

    public function __construct()
    {
        DeviceAuth::setDeviceId($this->getDeviceId());
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$this->deviceAuthorized($request)) {
            return $this->redirectUser();
        }

        return $next($request);
    }

    private function redirectUser()
    {
        DeviceAuth::generateDeviceEmailCode();
        return redirect()->route(self::EMAIL_SENT_ROUTE);        
    }

    private function getDeviceId() : ?string
    {
        return $this->hasCookie() ? $_COOKIE[self::COOKIE_KEY] : null;
    }

    private function hasCookie() : bool 
    {
        return array_key_exists(self::COOKIE_KEY, $_COOKIE);
    }

    private function deviceAuthorized($request): bool
    {
        return DeviceAuth::isCurrentDeviceAuthorized();
    }
}
