<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MKContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact_id' => 'required|unique:mk_contact,ContactID,'.$this->id,
            'owner' => 'required',
            'type' => 'required',
            'status' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'contact_id.required' => 'Contact is required.',
            'contact_id.unique' => 'Contact already existed.',
            'owner.required' => 'Owner is required.',
            'type.required' => 'Type is required.',
            'status.required' => 'Status is required.',
        ];
    }
}
