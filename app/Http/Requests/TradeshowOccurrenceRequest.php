<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TradeshowOccurrenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'show_name' => 'required|unique:Tradeshow,show_name,'.$this->id,
            'show_id' => 'required',
            'identifier' => 'required',
            'startdate' => 'required',
            'enddate' => 'required',
        ];

    }

    public function messages()
    {
        return [
            'show_id.required' => 'Tradeshow field is required.',
            'identifier.required' => 'Identifier is required.',
            'startdate.required' => 'Start date field is required.',
            'enddate.required' => 'End date field is required.',
        ];
    }
}
