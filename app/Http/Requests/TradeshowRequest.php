<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TradeshowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'show_name' => 'required|unique:tradeshow,show_name,'.$this->id,
            'show_short_name' => 'required',
            'show_vendor' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'show_name.required' => 'Show Name field is required.',
            'show_name.unique' => 'Show Name has already been taken.',
            'show_short_name.required' => 'Short Name field is required.',
            'show_vendor.required' => 'Vendor field is required.',
        ];
    }
}
