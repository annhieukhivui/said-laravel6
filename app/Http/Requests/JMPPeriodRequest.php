<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JMPPeriodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'show_name' => 'required|unique:Tradeshow,show_name,'.$this->id,
            'brand_id' => 'required',
            'period' => 'required',
            'period_name' => 'required|unique:JMPPeriod,period_name',
        ];
    }

    public function messages()
    {
        return [
            'brand_id.required' => 'Brand field is required.',
            'period.required' => 'Period field is required.',
            'period_name.required' => 'Period Name field is required.',
            'period_name.unique' => 'Period Name has already been taken.',
        ];
    }
}
