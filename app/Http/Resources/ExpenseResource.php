<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ExpenseResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'vendor' => $this->vendor_relation->VName;
        ];
    }
}
