<?php

namespace App\Models\Shared;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\SLMKUser;

class Changes extends Model
{
    protected $table = 'changes';
    protected $fillable = ['table_id', 'row_id', 'uid_created'];
    
    public function changes_detail_relation(){
    	return $this->hasMany(ChangesDetail::class, 'change_id', 'id');
    }

    public function user_relation(){
        return $this->hasOne(SLMKUser::class, 'id', 'uid_created');
    }
}
