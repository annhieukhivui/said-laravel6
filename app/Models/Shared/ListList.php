<?php

namespace App\Models\Shared;

use Illuminate\Database\Eloquent\Model;

class ListList extends Model
{
    protected $table = 'list_list';
    protected $fillable = ['description', 'in_order', 'pid', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    public $timestamps = false;

    // add return_status foreign to ProductOrder table
    // public function TReturnStatusRelation(){
    // 	// return $this->belongsTo(ListList::class, 'TReturnStatus', 'id');
    //     return $this->belongsTo(ListList::class, 'TReturnStatus', 'id');
    // }

    // // add return_status foreign to ProductOrder table
    // public function TStatusRelation(){
    // 	return $this->belongsTo(ListList::class, 'TStatus', 'id');
    // }

    // // add return_status foreign to ProductOrder table
    // public function LocationRelation(){
    // 	return $this->belongsTo(ListList::class, 'Location', 'id');
    // }
}
