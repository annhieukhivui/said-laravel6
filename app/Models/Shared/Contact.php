<?php

namespace App\Models\Shared;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';
    // protected $fillable = ['firstname', 'lastname', 'email', 'phone1', 'phone2', 'address1', 'address2', 'city', 'state', 'zip', 'bluemine', 'ns_id', 'owner', 'relationship', 'contact_for', 'type', 'status', 'follow_up_date', 'active', 'uid_created', 'uid_modified'];
    protected $fillable = ['firstname', 'lastname', 'email', 'phone1', 'address1', 'city', 'state', 'zip', 'active', 'uid_created', 'uid_modified'];

    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

}
