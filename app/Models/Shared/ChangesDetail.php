<?php

namespace App\Models\Shared;

use Illuminate\Database\Eloquent\Model;

class ChangesDetail extends Model
{
    protected $table = 'changes_detail';
    protected $fillable = ['change_id', 'column_name', 'old_value', 'new_value'];
    public $timestamps = false;

    public function user_relation(){
        return $this->hasOne(SLMKUser::class, 'id', 'uid_created');
    }
}
