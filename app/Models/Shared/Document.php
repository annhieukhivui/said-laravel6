<?php

namespace App\Models\Shared;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'document';
    protected $fillable = ['table_name', 'table_id', 'document_name', 'description', 'type', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    // add return_status foreign to ProductOrder table
    // public function ProductOrderRelation(){
    // 	return $this->belongsTo(ProductOrder::class, 'TEID', 'table_id')->where('table_name','=', 125);
    // }
}
