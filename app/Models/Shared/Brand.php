<?php

namespace App\Models\Shared;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brand';
    protected $fillable = ['pid', 'name', 'short_name', 'uid_created', 'uid_modified', 'active'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
