<?php

namespace App\Models\Shared;

use Illuminate\Database\Eloquent\Model;

use App\Models\Shared\ListList;
use App\Models\Admin\SLMKUser;

class Note extends Model
{
    protected $table = 'notes';
    protected $fillable = ['note_on_table', 'note_on_table_id', 'action', 'detail', 'uid_created'];
    protected $guarded = ['id'];
    public $timestamps = false;

    public function action_relation(){
        return $this->hasOne(ListList::class, 'id', 'action');
    }

    public function user_relation(){
        return $this->hasOne(SLMKUser::class, 'id', 'uid_created');
    }

}
