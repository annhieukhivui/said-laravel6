<?php

namespace App\Models\Shared;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = ['ID', 'SKU', 'BrandID', 'NSID', 'Manufacture', 'Name', 'Feature_name', 'Feature_Desc', 'Store_Desc', 'Product_Category_Id', 'Main_Img_Id', 'Keywords', 'UPS', 'Start_Date', 'Date_Created', 'active' ];
    protected $guarded = ['ID'];
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function brand_relation(){
        return $this->hasOne(Brand::class, 'id', 'BrandID')->select(['id', 'name']);
    }
}
