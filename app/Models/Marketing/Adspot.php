<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;
use App\Models\Shared\ListList;
use App\Models\Shared\Document;
use App\Models\Shared\Brand;
use App\Models\Shared\Changes;

class Adspot extends Model
{
    protected $table = 'mk_adspot';
    protected $fillable = ['AdbuyID', 'VendorID', 'ChannelID', 'medium', 'frequency', 'size', 'orientation', 'status', 'bluemine', 'issue', 'issue_theme', 'place', 'note', 'startdate', 'enddate', 'insertion_due', 'creative_due', 'msrp', 'invoice_amount', 'audited_reach', 'cpm', 'roi', 'quality_factor', 'subject', 'campaign', 'BrandID', 'product', 'phone_used', 'email_used', 'link_used', 'file_path', 'active', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function brand_relation(){
        return $this->hasOne(Brand::class, 'id', 'BrandID');
    }

    public function adbuy_relation(){
        return $this->hasOne(Adbuy::class, 'id', 'AdbuyID');
    }

    public function channel_relation(){
        return $this->hasOne(Channel::class, 'id', 'ChannelID')->select(['id', 'name']);
    }

    public function vendor_relation(){
        return $this->hasOne(Vendor::class, 'id', 'VendorID')->select(['id', 'name']);
    }

    public function medium_relation(){
        return $this->hasOne(ListList::class, 'id', 'medium')->select(['id', 'description']);
    }

    public function frequency_relation(){
        return $this->hasOne(ListList::class, 'id', 'frequency')->select(['id', 'description']);
    }

    public function size_relation(){
        return $this->hasOne(ListList::class, 'id', 'size')->select(['id', 'description']);
    }

    public function orientation_relation(){
        return $this->hasOne(ListList::class, 'id', 'orientation')->select(['id', 'description']);
    }

    public function status_relation(){
        return $this->hasOne(ListList::class, 'id', 'status')->select(['id', 'description']);
    }

    public function document_relation(){
    	return $this->hasMany(Document::class, 'table_id', 'id')->where('table_name','=', 246);
    }

    public function changes_relation(){
        return $this->hasMany(Changes::class, 'row_id', 'id')->where('table_id','=', 246)->orderBy('id', 'desc');
    }

}
