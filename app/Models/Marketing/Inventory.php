<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

use App\Models\Shared\Product;

class Inventory extends Model
{
    protected $table = 'mk_inventory';
    protected $fillable = ['ProductID', 'available', 'onhand', 'addon', 'in_program', 'active', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function product_relation(){
        return $this->hasOne(Product::class, 'ID', 'ProductID')->select(['ID', 'SKU', 'Name', 'BrandID'])->where('active', '1');
    }

    public function asset_relation(){
        return $this->hasMany(Asset::class, 'InventoryID', 'id')->select('id', 'InventoryID', 'serial', 'transaction_id')->where('active', '1')->orderBy('id', 'desc');
    }

    public function transaction_relation(){
        return $this->hasMany(InventoryTransaction::class, 'InventoryID', 'id')->orderBy('id', 'desc');
    }
    
}
