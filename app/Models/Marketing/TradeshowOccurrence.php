<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

use App\Models\Shared\Document;
use App\Models\Shared\ListList;
use App\Models\Shared\Note;

class TradeshowOccurrence extends Model
{
    protected $table = 'tradeshow_occurrence';
    protected $fillable = ['show_id', 'identifier', 'location', 'startdate', 'enddate', 'status', 'address', 'bluemine', 'website', 'booth_number', 'labor', 'exibitor_resources', 'login_user', 'login_pass', 'poc_sales_lead', 'poc_mktg_lead', 'poc_show_rep_name', 'poc_show_rep_number', 'poc_show_rep_email', 'sales', 'roi', 'meetings', 'leads', 'photo_album', 'uid_created', 'uid_modified', 'active'];
    protected $guarded = ['id'];
    public $timestamps = false;

    public function tradeshow_relation(){
    	// these 2 return same result
    	return $this->belongsTo(Tradeshow::class,  'show_id', 'id');
    	//return $this->hasOne(Tradeshow::class, 'id', 'show_id');
    }

    public function occurrence_status_relation(){
    	return $this->hasOne(ListList::class, 'id', 'status')->select(['id', 'pid', 'description']);
    }

    public function occurrence_distribution_relation(){
    	return $this->hasMany(TradeshowOccurrenceDistribution::class, 'occurrence_id', 'id');
    }

    public function document_relation(){
    	return $this->hasMany(Document::class, 'table_id', 'id')->where('table_name','=', 123);
    }

    public function note_relation(){
        return $this->hasMany(Note::class, 'note_on_table_id', 'id')->where('note_on_table','=', 123);
    }

}
