<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;
use App\Models\Shared\ListList;
use App\Models\Shared\Changes;

class Adbuy extends Model
{
    protected $table = 'mk_adbuy';
    protected $fillable = ['VendorID', 'description', 'date_quoted', 'date_signed', 'file_path', 'status', 'total_commit', 'amount_remaining', 'bluemine', 'active', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function vendor_relation(){
        return $this->hasOne(Vendor::class, 'id', 'VendorID')->select(['id', 'name']);
    }

    public function status_relation(){
        return $this->hasOne(ListList::class, 'id', 'status')->select(['id', 'description']);
    }

    public function adspot_relation(){
        return $this->hasMany(Adspot::class, 'AdbuyID', 'id');
    }

    public function changes_relation(){
        return $this->hasMany(Changes::class, 'row_id', 'id')->where('table_id','=', 245)->orderBy('id', 'desc');
    }
}
