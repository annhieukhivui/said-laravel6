<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;
use App\Models\Shared\Contact as SharedContact;
use App\Models\Shared\ListList;
use App\Models\Shared\Note;
use App\Models\Shared\Changes;

class Contact extends Model
{
    protected $table = 'mk_contact';
    protected $fillable = ['id', 'ContactID', 'relationship', 'status', 'type', 'follow_up_date', 'beat', 'note', 'grade', 'pl_prostaff', 'sm_prostaff', 'aglow', 'poma', 'nssf', 'nra', 'yt_reach', 'fb_reach', 'ig_reach', 'tw_reach', 'web_reach', 'high_value_influencer', 'paid_influencer', 'active', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function general_contact_relation(){
    	return $this->belongsTo(SharedContact::class, 'ContactID', 'id')->select(['id', 'firstname', 'lastname', 'address1', 'address2', 'city', 'state', 'zip', 'phone1', 'phone2', 'email']);
    }

    public function status_relation(){
        return $this->hasOne(ListList::class, 'id', 'status')->select(['id', 'description']);
    }

    public function type_relation(){
        return $this->hasOne(ListList::class, 'id', 'type')->select(['id', 'description']);
    }

    public function product_order_relation(){
        return $this->hasMany(TEProductOrder::class, 'ContactID', 'id');
    }

    public function placement_relation(){
        return $this->hasMany(Placement::class, 'ContactID', 'id');
    }

    public function note_relation(){
        return $this->hasMany(Note::class, 'note_on_table_id', 'id')->where('note_on_table','=', 126);
    }

    public function note_relation_count(){
        return $this->hasMany(Note::class, 'note_on_table_id', 'id')->select(['id', 'note_on_table', 'note_on_table_id', 'action', 'date_created'])->where('note_on_table','=', 126);
    }

    public function changes_relation(){
        return $this->hasMany(Changes::class, 'row_id', 'id')->where('table_id','=', 126)->orderBy('id', 'desc');
    }
}
