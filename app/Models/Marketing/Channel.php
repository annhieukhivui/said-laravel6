<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;
use App\Models\Shared\ListList;

class Channel extends Model
{
    protected $table = 'mk_channel';
    protected $primaryKey = 'id';
    protected $fillable = ['VendorID', 'name', 'medium', 'frequency', 'platform', 'category', 'target', 'reach', 'active', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    public $timestamps = false;

    public function medium_relation(){
    	return $this->hasOne(ListList::class, 'id', 'medium')->select(['id', 'description']);
    }
    
    public function frequency_relation(){
    	return $this->hasOne(ListList::class, 'id', 'frequency')->select(['id', 'description']);
    }
}
