<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'vendor';
    protected $primaryKey = 'id';
    protected $fillable = ['pid', 'name', 'short_name', 'email', 'phone', 'fax', 'website', 'address1', 'address2', 'city', 'state', 'zip', 'active', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    public $timestamps = false;

    public function channel_relation(){
    	return $this->hasMany(Channel::class, 'VendorID', 'id');
    }

}
