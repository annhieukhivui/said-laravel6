<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;
use App\Models\Shared\ListList;

class TEWaitList extends Model
{
    protected $table = 'te_waitinglist';
    protected $primaryKey = 'id';
    protected $fillable = ['ProgramID', 'ContactID', 'quantity', 'owner', 'term', 'queue', 'status', 'note', 'uid_created', 'uid_modified', 'active', 'ProductID'];
    protected $guarded = ['id'];
    public $timestamps = false;

    public function waitlist_inventory_relation(){
    	return $this->belongsToMany(Inventory::class, 'te_waitinglist_inventory', 'WaitID', 'InventoryID');
    }

    public function contact_relation(){
        return $this->hasOne(Contact::class, 'id', 'ContactID');
    }

    public function program_relation(){
        return $this->hasOne(TEProgram::class, 'id', 'ProgramID');
    }

    public function status_relation(){
        return $this->hasOne(ListList::class, 'id', 'status');
    }
}
