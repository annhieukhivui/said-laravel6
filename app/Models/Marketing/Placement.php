<?php

namespace App\Models\Marketing;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use App\Models\Shared\ListList;
use App\Models\Shared\Document;

class Placement extends Model
{
    use Notifiable;

    protected $table = 'te_placement';
    protected $primaryKey = 'id';
    protected $fillable = ['ChannelID', 'ContactID', 'platform', 'author', 'type', 'link', 'ave', 'pl_date', 'note', 'active', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    public $timestamps = false;

    public function channel_relation(){
    	return $this->hasOne(Channel::class, 'id', 'ChannelID');
    }

    public function contact_relation(){
        return $this->hasOne(Contact::class, 'id', 'ContactID');
    }

    public function product_order_relation(){
        return $this->belongsToMany(TEProductOrder::class, 'te_placement_having', 'PlID', 'TEID');
    }

    public function type_relation(){
    	return $this->hasOne(ListList::class, 'id', 'type')->select(['id', 'description']);
    }

    public function document_relation(){
        return $this->hasMany(Document::class, 'table_id', 'id')->where('table_name','=', 160);
    }

}
