<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;
use App\Models\Shared\ListList;
use App\Models\Shared\Changes;

class InventoryTransaction extends Model
{
    protected $table = 'mk_inventory_transaction';
    protected $fillable = ['InventoryID', 'TrAction', 'TrDate', 'is_asset', 'serial', 'quantity', 'owner', 'location', 'out_to_choice', 'out_to_value', 'out_term', 'ns_location', 'ns_order_number', 'color', 'kit', 'note', 'uid_created', 'uid_modified', 'active'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function inventory_relation(){
        return $this->hasOne(Inventory::class, 'id', 'InventoryID');
    }
    
    public function action_relation(){
        return $this->hasOne(ListList::class, 'id', 'TrAction')->select(['id', 'description']);
    }

    public function out_to_relation(){
        return $this->hasOne(ListList::class, 'id', 'out_to_choice')->select(['id', 'description']);
    }

    public function changes_relation(){
        return $this->hasMany(Changes::class, 'row_id', 'id')->where('table_id','=', 247)->orderBy('id', 'desc');
    }
}
