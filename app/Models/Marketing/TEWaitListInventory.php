<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class TEWaitListInventory extends Model
{
    protected $table = 'te_waitinglist_inventory';
    protected $primaryKey = 'id';
    protected $fillable = ['WaitID', 'InventoryID'];
    protected $guarded = ['id'];
    public $timestamps = false;

    // public function inventory_relation(){
    //     return $this->hasOne(Inventory::class, 'id', 'InventoryID');
    // }
}
