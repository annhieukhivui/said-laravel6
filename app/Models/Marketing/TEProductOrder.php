<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

use App\Models\Shared\Document;
use App\Models\Shared\ListList;
use App\Models\Shared\Note;
use App\Models\Shared\Changes;

class TEProductOrder extends Model
{
    protected $table = 'te_product_order';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'pid', 'ContactID', 'relationship', 'owner', 'InventoryID', 'serial', 'quantity', 'sale_order', 'invoice', 'date_out', 'term', 'expect_return_date', 'sale_return_date', 'ra', 'note', 'fulfillment_tracking', 'addon', 'status', 'location', 'return_late', 'return_damged', 'follow_up_date', 'active', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    
    public $timestamps = false;

    public function inventory_relation(){
        return $this->hasOne(Inventory::class, 'id', 'InventoryID')->select(['id', 'ProductID', 'available', 'addon', 'in_program']);
    }

    public function contact_relation(){
    	return $this->hasOne(Contact::class, 'id', 'ContactID')->select(['id', 'ContactID']);
    }    

    public function status_relation(){
    	return $this->hasOne(ListList::class, 'id', 'status')->select(['id', 'description']);
    }

    public function location_relation(){
    	return $this->hasOne(ListList::class, 'id', 'location')->select(['id', 'description']);
    }

    public function document_relation(){
    	return $this->hasMany(Document::class, 'table_id', 'id')->where('table_name','=', 125);
    }

    public function placement_relation(){
        return $this->belongsToMany(Placement::class, 'te_placement_having', 'TEID', 'PlID');
    }

    public function note_relation(){
        return $this->hasMany(Note::class, 'note_on_table_id', 'id')->where('note_on_table','=', 125);
    }

    public function changes_relation(){
        return $this->hasMany(Changes::class, 'row_id', 'id')->where('table_id','=', 125)->orderBy('id', 'desc');
    }

}
