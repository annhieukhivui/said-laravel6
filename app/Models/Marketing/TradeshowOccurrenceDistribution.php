<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

use App\Models\Shared\Brand;

class TradeshowOccurrenceDistribution extends Model
{
    protected $table = 'tradeshow_occurrence_distribution';
    protected $fillable = ['occurrence_id', 'brand_id', 'percent'];
    protected $guarded = ['id'];
    public $timestamps = false;

    public function distribution_brand_relation(){
    	return $this->hasOne(Brand::class, 'id', 'brand_id')->select(['id', 'pid', 'name', 'short_name']);
    }
}
