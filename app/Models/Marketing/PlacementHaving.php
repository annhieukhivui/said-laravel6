<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class PlacementHaving extends Model
{
    protected $table = 'te_placement_having';
    protected $primaryKey = 'id';
    protected $fillable = ['PlID', 'TEID'];
    protected $guarded = ['id'];
    public $timestamps = false;
}
