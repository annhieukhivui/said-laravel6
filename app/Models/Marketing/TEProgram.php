<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class TEProgram extends Model
{
    protected $table = 'te_program';
    protected $primaryKey = 'id';
    protected $fillable = ['value', 'pid', 'uid_creatd', 'uid_modified', 'active'];
    protected $guarded = ['id'];
    public $timestamps = false;

    public function waitinglist_relation(){
    	return $this->hasMany(TEWaitList::class, 'ProgramID', 'id')->where([['status', '=', '207'], ['active', '=', '1']]);
    }

    public function child_relation(){
    	return $this->hasMany(TEProgram::class, 'pid', 'id');
    }

    public function inventory_relation(){
    	return $this->hasOne(Inventory::class, 'id', 'value');
    }



    // public function parent_relation(){
    // 	return $this->belongsTo(TEProgram::class, 'pid', 'id');
    // }
}
