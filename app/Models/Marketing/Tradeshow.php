<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;
use App\Models\Shared\Document;
use App\Models\Shared\ListList;

class Tradeshow extends Model
{
    protected $table = 'tradeshow';
    protected $fillable = ['show_name', 'short_name', 'show_description', 'website', 'market_strategy', 'show_status', 'vendor', 'uid_created', 'uid_modified', 'active'];
    protected $guarded = ['id'];
    public $timestamps = false;
    
    public function vendor_relation(){
    	return $this->hasOne(Vendor::class, 'id', 'vendor');
    }

    public function show_occurrence_relation(){
    	return $this->hasMany(TradeshowOccurrence::class, 'show_id', 'id');
    }

    public function market_strategy_relation(){
        return $this->hasOne(ListList::class, 'id', 'market_strategy');
    }

    public function show_status_relation(){
        return $this->hasOne(ListList::class, 'id', 'show_status');
    }

    public function document_relation(){
    	return $this->hasMany(Document::class, 'table_id', 'id')->where('table_name','=', 124);
    }
}
