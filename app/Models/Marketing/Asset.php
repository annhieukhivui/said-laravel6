<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $table = 'mk_asset';
    protected $fillable = ['InventoryID', 'serial', 'transaction_id', 'reserved', 'active', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function transaction_relation(){
        return $this->hasOne(InventoryTransaction::class, 'id', 'transaction_id');
    }

    public function inventory_relation(){
        return $this->hasOne(Inventory::class, 'id', 'InventoryID');
    }
}
