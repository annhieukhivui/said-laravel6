<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Shared\ListList;

class SLMKUser extends Authenticatable
{
	use Notifiable;

    const MAX_DEVICE_COUNT = 2;

    protected $table = 'user';
    protected $primaryKey = 'id';
    protected $fillable = ['firstname', 'lastname', 'title', 'username', 'MD5Password', 'password', 'role', 'SlackID', 'active', 'uid_created', 'uid_modified' ];
    protected $guarded = ['id'];
    protected $hidden = ['MD5Password', 'password', 'remember_token'];
    public $timestamps = false;


    public function device_relation()
    {
        return $this->hasMany(UserDevice::class, 'user_id');
    }

    public function isDeviceAuthorized(?string $deviceId) : bool
    {
        return $this->device_relation()->where('device_hash', $deviceId)->exists();
    }

    public function canAddDevice() : bool 
    {
        return $this->device_relation()->count() < self::MAX_DEVICE_COUNT;
    }

    public function maxDeviceCount()
    {
        return self::MAX_DEVICE_COUNT;
    }

    // public function user_marketing_department_relation(){
    //     return $this->hasMany(ListList::class, 'sm_e_belongto_d', 'SMEmID', 'SMDeptID')->where('id','=', 98);
    // }

    // public function user_department_relation(){
    //     return $this->belongsToMany(ListList::class, 'sm_e_belongto_d', 'SMEmID', 'SMDeptID')->where('pid','=', 198);
    // }

}
