<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class SLMKRole extends Model
{
    protected $table = 'sm_role';
    protected $fillable = ['name', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    public $timestamps = false;
}
