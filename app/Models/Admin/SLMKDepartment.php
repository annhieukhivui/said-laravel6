<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class SLMKDepartment extends Model
{
    protected $table = 'sm_departments';
    protected $fillable = ['name', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    public $timestamps = false;
}
