<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UserDevice extends Model
{
    protected $fillable = ['device_hash'];

    public function user_relation()
    {
    	return $this->belongsTo(SLMKUser::class);
    }

    public function parsed()
    {
    	return json_decode(base64_decode($this->device_hash));
    }

    public function info()
    {
    	return collect($this->parsed)->mapWithKeys(function($item){
    		return [$item->key => $item->value];
    	});
    }
}
