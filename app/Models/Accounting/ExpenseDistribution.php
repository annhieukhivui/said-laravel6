<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;
use App\Models\Shared\Brand;

class ExpenseDistribution extends Model
{
    protected $table = 'expense_distribution';
    protected $fillable = ['expense_id', 'brand_id', 'percent'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function expense_relation(){
    	return $this->belongsTo(Expense::class, 'expense_id');
    }

    public function distribution_brand_relation(){
        return $this->hasOne(Brand::class, 'id', 'brand_id')->select(['id', 'pid', 'name', 'short_name']);
    	// return $this->belongsTo(Brand::class, 'brand_id', 'BrandID');
    }
}
