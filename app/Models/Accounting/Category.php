<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'expense_category';
    protected $fillable = ['description', 'in_order', 'pid', 'accounting_code', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    public $timestamps = false;
}
