<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

// use App\Models\Accounting\Category;
// use App\Models\Marketing\Vendor;
// use App\Models\Marketing\TradeshowOccurrence;
use App\Models\Shared\Brand;
use App\Models\Shared\Document;
use App\Models\Shared\ListList;

class JMPPeriod extends Model
{
    protected $table = 'jmp_period';
    protected $fillable = ['period', 'period_name', 'from_date', 'to_date', 'submission_date', 'brand_id', 'status', 'active', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function brand_relation(){
    	return $this->hasOne(Brand::class, 'id', 'brand_id')->select(['id', 'name']);
    }

    public function status_relation(){
        return $this->hasOne(ListList::class, 'id', 'status')->select(['id', 'pid', 'description']);
    }

    public function document_relation(){
        return $this->hasMany(Document::class, 'table_id', 'id')->where('table_name','=', 122);
    }
}
