<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

use App\Models\Accounting\Expense;
use App\Models\Accounting\JMPPeriod;
use App\Models\Shared\Brand;
use App\Models\Shared\ListList;

class JMPExpense extends Model
{
    protected $table = 'jmp_expense';
    protected $fillable = ['expense_id', 'period_id', 'percent_submit', 'percent_approval', 'active', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function expense_relation(){
    	return $this->hasOne(Expense::class, 'id', 'expense_id')->select(['id', 'vendor_id', 'category']);
    }

    public function jmpperiod_relation(){
    	return $this->hasOne(JMPPeriod::class, 'id', 'period_id')->select(['id', 'period_name']);
    }
}
