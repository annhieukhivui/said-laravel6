<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

use App\Models\Accounting\Category;
use App\Models\Marketing\Vendor;
use App\Models\Marketing\TradeshowOccurrence;
use App\Models\Shared\Document;
use App\Models\Shared\Note;

class Expense extends Model
{
    protected $table = 'expense';
    protected $fillable = ['vendor_id', 'category', 'occurrence_id', 'invoice', 'invoice_date', 'ns_bill', 'who_paid', 'from_account', 'amount_paid', 'tax_paid', 'tip_paid', 'description', 'active', 'uid_created', 'uid_modified'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function vendor_relation(){
    	return $this->hasOne(Vendor::class, 'id', 'vendor_id')->select(['id', 'name']);
    }

    public function category_relation(){
    	return $this->hasOne(Category::class, 'id', 'category')->select(['id', 'pid', 'description']);
    }

    public function occurrence_relation(){
        return $this->hasOne(TradeshowOccurrence::class, 'id', 'occurrence_id')->select(['id', 'identifier', 'location', 'startdate', 'enddate']);
    }

    public function expense_distribution_relation(){
    	return $this->hasMany(ExpenseDistribution::class, 'expense_id', 'id');
    }

    public function expense_jmp_relation(){
        return $this->belongsToMany(JMPPeriod::class, 'jmp_expense', 'expense_id', 'period_id')->withPivot('percent_submit', 'percent_approval');
    }

    public function document_relation(){
        return $this->hasMany(Document::class, 'table_id', 'id')->where('table_name','=', 121);
    }

    public function note_relation(){
        return $this->hasMany(Note::class, 'note_on_table_id', 'id')->where('note_on_table','=', 121);
    }
}
