<?php

namespace App\Observers;

use App\Models\Marketing\Adspot;
use App\Models\Shared\Changes;
use App\Models\Shared\ChangesDetail;

class AdspotObserver
{
    public $table = 246;
    /**
     * Handle the adspot "created" event.
     *
     * @param  \App\Models\Marketing\Adspot  $adspot
     * @return void
     */
    public function created(Adspot $adspot)
    {
        //
    }

    /**
     * Handle the adspot "updated" event.
     *
     * @param  \App\Models\Marketing\Adspot  $adspot
     * @return void
     */
    public function updated(Adspot $adspot)
    {
        if($adspot->isDirty()){
            $change = new Changes;
            $change->table_id = $this->table;
            $change->row_id = $adspot->id;
            $change->uid_created = $adspot->uid_modified;
            $change->save();
            $change_id = $change->id;

            $changes = $adspot->getChanges();
            foreach ($changes as $column => $value) {
                $detail = new ChangesDetail;
                $detail->change_id = $change_id;
                $detail->column_name = $column;
                $detail->old_value = $adspot->getOriginal($column) == '' ? '' : $adspot->getOriginal($column);
                $detail->new_value = $value == '' ? '' : $value;
                $detail->save();
            }
        }
    }

    /**
     * Handle the adspot "deleted" event.
     *
     * @param  \App\Models\Marketing\Adspot  $adspot
     * @return void
     */
    public function deleted(Adspot $adspot)
    {
        //
    }

    /**
     * Handle the adspot "restored" event.
     *
     * @param  \App\Models\Marketing\Adspot  $adspot
     * @return void
     */
    public function restored(Adspot $adspot)
    {
        //
    }

    /**
     * Handle the adspot "force deleted" event.
     *
     * @param  \App\Models\Marketing\Adspot  $adspot
     * @return void
     */
    public function forceDeleted(Adspot $adspot)
    {
        //
    }
}
