<?php

namespace App\Observers;

use App\Models\Marketing\Contact;
use App\Models\Shared\Changes;
use App\Models\Shared\ChangesDetail;

class MarketingContactObserver
{
    public $table = 126;
    /**
     * Handle the contact "created" event.
     *
     * @param  \App\Models\Marketing\Contact  $contact
     * @return void
     */
    public function created(Contact $contact)
    {
        //
    }

    /**
     * Handle the contact "updated" event.
     *
     * @param  \App\Models\Marketing\Contact  $contact
     * @return void
     */
    public function updated(Contact $contact)
    {
        if($contact->isDirty()){
            
            $change = new Changes;
            $change->table_id = $this->table;
            $change->row_id = $contact->id;
            $change->uid_created = $contact->uid_modified;
            $change->save();
            $change_id = $change->id;

            $changes = $contact->getChanges();
            foreach ($changes as $column => $new_value) {
                $detail = new ChangesDetail;
                $detail->change_id = $change_id;
                $detail->column_name = $column;
                $detail->old_value = $contact->getOriginal($column) == '' ? '' : $contact->getOriginal($column);
                $detail->new_value = $new_value == '' ? '' : $new_value;
                $detail->save();
            }
        }
    }

    /**
     * Handle the contact "deleted" event.
     *
     * @param  \App\Models\Marketing\Contact  $contact
     * @return void
     */
    public function deleted(Contact $contact)
    {
        //
    }

    /**
     * Handle the contact "restored" event.
     *
     * @param  \App\Models\Marketing\Contact  $contact
     * @return void
     */
    public function restored(Contact $contact)
    {
        //
    }

    /**
     * Handle the contact "force deleted" event.
     *
     * @param  \App\Models\Marketing\Contact  $contact
     * @return void
     */
    public function forceDeleted(Contact $contact)
    {
        //
    }
}
