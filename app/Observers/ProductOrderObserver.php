<?php

namespace App\Observers;

use App\Models\Marketing\TEProductOrder;
use App\Models\Shared\Changes;
use App\Models\Shared\ChangesDetail;
use App\Models\Shared\ListList;

class ProductOrderObserver
{
    public $table = 125;
    /**
     * Handle the t e product order "created" event.
     *
     * @param  \App\Models\Marketing\TEProductOrder  $tEProductOrder
     * @return void
     */
    public function created(TEProductOrder $tEProductOrder)
    {
        //
    }
    
    /**
     * Handle the t e product order "updated" event.
     *
     * @param  \App\Models\Marketing\TEProductOrder  $tEProductOrder
     * @return void
     */
    public function updated(TEProductOrder $tEProductOrder)
    {
        if($tEProductOrder->isDirty()){
            
            $change = new Changes;
            $change->table_id = $this->table;
            $change->row_id = $tEProductOrder->id;
            $change->uid_created = $tEProductOrder->uid_modified;
            $change->save();
            $change_id = $change->id;

            $changes = $tEProductOrder->getChanges();
            foreach ($changes as $column => $value) {
                $detail = new ChangesDetail;
                $detail->change_id = $change_id;
                $detail->column_name = $column;
                $detail->old_value = $tEProductOrder->getOriginal($column) == '' ? '' : $tEProductOrder->getOriginal($column);
                $detail->new_value = $value == '' ? '' : $value;
                $detail->save();
            }
        }
    }

    /**
     * Handle the t e product order "deleted" event.
     *
     * @param  \App\Models\Marketing\TEProductOrder  $tEProductOrder
     * @return void
     */
    public function deleted(TEProductOrder $tEProductOrder)
    {
        //
    }

    /**
     * Handle the t e product order "restored" event.
     *
     * @param  \App\Models\Marketing\TEProductOrder  $tEProductOrder
     * @return void
     */
    public function restored(TEProductOrder $tEProductOrder)
    {
        //
    }

    /**
     * Handle the t e product order "force deleted" event.
     *
     * @param  \App\Models\Marketing\TEProductOrder  $tEProductOrder
     * @return void
     */
    public function forceDeleted(TEProductOrder $tEProductOrder)
    {
        //
    }
}
