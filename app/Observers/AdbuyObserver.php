<?php

namespace App\Observers;

use App\Models\Marketing\Adbuy;
use App\Models\Shared\Changes;
use App\Models\Shared\ChangesDetail;

class AdbuyObserver
{
    public $table = 245;
    /**
     * Handle the adbuy "created" event.
     *
     * @param  \App\Models\Marketing\Adbuy  $adbuy
     * @return void
     */
    public function created(Adbuy $adbuy)
    {
        //
    }

    /**
     * Handle the adbuy "updated" event.
     *
     * @param  \App\Models\Marketing\Adbuy  $adbuy
     * @return void
     */
    public function updated(Adbuy $adbuy)
    {
        if($adbuy->isDirty()){
            
            $change = new Changes;
            $change->table_id = $this->table;
            $change->row_id = $adbuy->id;
            $change->uid_created = $adbuy->uid_modified;
            $change->save();
            $change_id = $change->id;

            $changes = $adbuy->getChanges();
            foreach ($changes as $column => $value) {
                $detail = new ChangesDetail;
                $detail->change_id = $change_id;
                $detail->column_name = $column;
                $detail->old_value = $adbuy->getOriginal($column) == '' ? '' : $adbuy->getOriginal($column);
                $detail->new_value = $value == '' ? '' : $value;
                $detail->save();
            }
        }
    }

    /**
     * Handle the adbuy "deleted" event.
     *
     * @param  \App\Models\Marketing\Adbuy  $adbuy
     * @return void
     */
    public function deleted(Adbuy $adbuy)
    {
        //
    }

    /**
     * Handle the adbuy "restored" event.
     *
     * @param  \App\Models\Marketing\Adbuy  $adbuy
     * @return void
     */
    public function restored(Adbuy $adbuy)
    {
        //
    }

    /**
     * Handle the adbuy "force deleted" event.
     *
     * @param  \App\Models\Marketing\Adbuy  $adbuy
     * @return void
     */
    public function forceDeleted(Adbuy $adbuy)
    {
        //
    }
}
