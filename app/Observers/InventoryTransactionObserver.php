<?php

namespace App\Observers;

use App\Models\Marketing\InventoryTransaction;
use App\Models\Shared\Changes;
use App\Models\Shared\ChangesDetail;

class InventoryTransactionObserver
{
    public $table = 247;

    /**
     * Handle the inventory transaction "created" event.
     *
     * @param  \App\Models\Marketing\InventoryTransaction  $inventoryTransaction
     * @return void
     */
    public function created(InventoryTransaction $inventoryTransaction)
    {
        //
    }

    /**
     * Handle the inventory transaction "updated" event.
     *
     * @param  \App\Models\Marketing\InventoryTransaction  $inventoryTransaction
     * @return void
     */
    public function updated(InventoryTransaction $inventoryTransaction)
    {
        if($inventoryTransaction->isDirty()){
            
            $change = new Changes;
            $change->table_id = $this->table;
            $change->row_id = $inventoryTransaction->id;
            $change->uid_created = $inventoryTransaction->uid_modified;
            $change->save();
            $change_id = $change->id;

            $changes = $inventoryTransaction->getChanges();
            foreach ($changes as $column => $value) {
                $detail = new ChangesDetail;
                $detail->change_id = $change_id;
                $detail->column_name = $column;
                $detail->old_value = $inventoryTransaction->getOriginal($column) == '' ? '' : $inventoryTransaction->getOriginal($column);
                $detail->new_value = $value == '' ? '' : $value;
                $detail->save();
            }
        }
    }

    /**
     * Handle the inventory transaction "deleted" event.
     *
     * @param  \App\Models\Marketing\InventoryTransaction  $inventoryTransaction
     * @return void
     */
    public function deleted(InventoryTransaction $inventoryTransaction)
    {
        //
    }

    /**
     * Handle the inventory transaction "restored" event.
     *
     * @param  \App\Models\Marketing\InventoryTransaction  $inventoryTransaction
     * @return void
     */
    public function restored(InventoryTransaction $inventoryTransaction)
    {
        //
    }

    /**
     * Handle the inventory transaction "force deleted" event.
     *
     * @param  \App\Models\Marketing\InventoryTransaction  $inventoryTransaction
     * @return void
     */
    public function forceDeleted(InventoryTransaction $inventoryTransaction)
    {
        //
    }
}
