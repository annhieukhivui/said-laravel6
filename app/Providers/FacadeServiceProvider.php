<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\DeviceAuthorization;

class FacadeServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind('device_authorization', function(){
            return new DeviceAuthorization;
        });
    }
}
