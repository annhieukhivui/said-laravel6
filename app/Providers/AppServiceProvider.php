<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

use App\Models\Marketing\TEProductOrder;
use App\Observers\ProductOrderObserver;

use App\Models\Marketing\Contact;
use App\Observers\MarketingContactObserver;

use App\Models\Marketing\InventoryTransaction;
use App\Observers\InventoryTransactionObserver;

use App\Models\Marketing\Adbuy;
use App\Observers\AdbuyObserver;

use App\Models\Marketing\Adspot;
use App\Observers\AdspotObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function($view){
            $view->with('LoginUser', Auth::user());
        });

        if($this->app->environment('staging')) {
            URL::forceScheme('https');
        }

        TEProductOrder::observe(ProductOrderObserver::class);
        Contact::observe(MarketingContactObserver::class);
        InventoryTransaction::observe(InventoryTransactionObserver::class);
        Adbuy::observe(AdbuyObserver::class);
        Adspot::observe(AdspotObserver::class);
    }
}
