<?php 

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class DeviceAuthorization extends Facade {

    protected static function getFacadeAccessor() 
    { 
    	return 'device_authorization'; 
    }

}