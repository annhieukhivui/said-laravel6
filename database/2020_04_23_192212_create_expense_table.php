<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('vendor_id')->nullable();
            $table->unsignedInteger('category')->nullable();
            $table->unsignedInteger('occurrence_id')->nullable();
            $table->string('invoice', 63)->nullable();
            $table->date('invoice_date')->nullable();
            $table->string('ns_bill', 63)->nullable();
            $table->string('who_paid', 63)->nullable();
            $table->string('from_account', 64)->nullable();
            $table->unsignedDecimal('amount_paid', 20, 2)->nullable();
            $table->unsignedDecimal('tax_paid', 10, 2)->nullable();
            $table->unsignedDecimal('tip_paid', 10, 2)->nullable();
            $table->string('description')->nullable();
            $table->boolean('active')->default(1);
            $table->unsignedInteger('uid_created')->nullable();
            $table->unsignedInteger('uid_modified')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense');
    }
}
