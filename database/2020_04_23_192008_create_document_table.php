<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('table_name')->nullable();
            $table->unsignedInteger('table_id')->nullable();
            $table->string('document_name')->nullable();
            $table->string('description')->nullable();
            $table->unsignedInteger('type')->nullable();
            $table->unsignedInteger('uid_created')->nullable();
            $table->unsignedInteger('uid_modified')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document');
    }
}
