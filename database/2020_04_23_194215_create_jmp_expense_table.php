<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJmpExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jmp_expense', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('expense_id');
            $table->unsignedInteger('period_id');
            $table->unsignedDecimal('percent_submit', 3, 2)->nullable();
            $table->unsignedDecimal('percent_approval', 3, 2)->nullable();
            $table->unsignedInteger('uid_created')->nullable();
            $table->unsignedInteger('uid_modified')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jmp_expense');
    }
}
