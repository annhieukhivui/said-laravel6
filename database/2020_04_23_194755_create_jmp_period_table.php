<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJmpPeriodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jmp_period', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('period', 63)->nullable();
            $table->string('period_name', 63)->nullable();
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->date('submission_date')->nullable();
            $table->unsignedInteger('brand_id')->nullable();
            $table->unsignedInteger('status')->nullable();
            $table->boolean('active')->default(1);
            $table->unsignedInteger('uid_created')->nullable();
            $table->unsignedInteger('uid_modified')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jmp_period');
    }
}
