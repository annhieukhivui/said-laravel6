<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpenseDistributionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_distribution', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('expense_id')->nullable();
            $table->unsignedInteger('brand_id')->nullable();
            $table->unsignedDecimal('percent', 3, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_distribution');
    }
}
