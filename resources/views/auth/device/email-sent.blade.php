<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title >Sellmark All Include</title>
        <!-- Bootstrap -->
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

    </head>

    <body>
        <div id="app" class="container">
            <div class="row">
                <div id="wrapper" style="width:100%; text-align:center; margin-top:5%">
                    <img src='/assets/images/sellmarklogo.png'>
                    <br>
                    <h2 align="center" style="color:#32638E"><strong>S.A.I.D.</strong></h1>
                </div>
            </div>
            <div class="row">
                <div class="offset-md-3 offset-lg-4 col-md-6 col-lg-4" align="center">
                    <div id="center">
                        @if(Auth::user()->canAddDevice())
                        <br />
                        <p>An email has been sent to {{ Auth::user()->email }}, please enter the code below authorize this device.</p>
                        <form method="POST">
                          {{ csrf_field() }}
                          <div class="form-group">
                            <input type="text" name="code" class="form-control" aria-describedby="emailCodeHelp" placeholder="Enter Code">
                          </div>
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                        @else
                        <p>Cannot authorize additional devices, please reach out to IT to modify current devices.</p>
                        @endif
                        <br />
                        <p><strong>Authorized Devices:</strong> {{ Auth::user()->device_relation()->count() }}/{{ Auth::user()->maxDeviceCount() }}</p>
                    </div>
                </div>
            </div>
        </div>
                <!-- <script src="{{ asset('/js/app.js') }}"></script> -->

    </body>
</html>