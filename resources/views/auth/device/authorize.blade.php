<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title >Sellmark All Include</title>
        <!-- Bootstrap -->
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="row">
            <div id="wrapper" style="width:100%; text-align:center; margin-top:5%">
                <img src='/assets/images/sellmarklogo.png'>
                <br>
                <h2 align="center" style="color:#32638E"><strong>S.A.I.D.</strong></h1>
            </div>
        </div>
        <div class="row">
            <div class="offset-sm-3 offset-md-3 offset-lg-3 col-sm-6 col-md-6 col-lg-6" align="center">

                <div id="center">
                    <br>
                    <p>Your device has been authorized.</p>
                    <p><strong>Authorized Devices: {{ Auth::user()->device_relation()->count() }}</strong></p>
                </div>
            </div>
        </div>
    </body>
</html>