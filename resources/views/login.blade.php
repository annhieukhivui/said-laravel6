<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="csrf-token" content="{{ csrf_token() }}">
	    <title >Sellmark All Include</title>
	    <!-- Bootstrap -->
	    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	</head>

	<body>
	<div id="app">
		<div class="row">
	        <div id="wrapper" style="width:100%; text-align:center; margin-top:5%">
	            <img src='assets/images/sellmarklogo.png'>
	            <br>
	            <h2 align="center" style="color:#32638E"><strong>S.A.I.D.</strong></h1>
	        </div>
		</div>
		<div class="row">
			<div class="offset-sm-3 offset-md-3 offset-lg-3 col-sm-6 col-md-6 col-lg-6" align="center">
				<p>Please use the login credentials provided to you to access Sellmark External Resources.</p>
				@if ($message = Session::get('user'))
				   <p style="color: red">{{ $message }}</p>
				@endif

				@foreach($errors->all() as $e)
					<li style="color: red">{{ $e }}</li>
				@endforeach

				@if ($message = Session::get('login_failed'))
				   <p style="color: red">{{ $message }}</p>
				@endif

				<div id="center">
					<form class="form-horizontal" action="login" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group">
		  					<div class="col-sm-10">
		  						<input type="text" id="username" name="username" class="form-control" placeholder="Username">
		  					</div>
		  				</div>
		  				<div class="form-group">
		  					<div class="col-sm-10">
		  						<input type="password" id="password" name="password" class="form-control" placeholder="Password">
		  					</div>
		  				</div>
		  				<div class="form-group">
						    <div class="col-sm-10">
						    	<input type="submit" value="Login" class="btn btn-primary" />
						    </div>
					    </div>
					</form>
				</div>
			</div>
		</div>

		
	</div>
	<script src="{{ asset('/js/app.js') }}"></script>
	</body>
</html>