@extends('layout.master')

@section('content')

<div id="app">
    <accounting-category-index :data="{{$data}}"></accounting-category-index>
</div>

@endsection