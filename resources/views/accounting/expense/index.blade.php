@extends('layout.master')

@section('content')

<div id="app">
    <accounting-expense-datatable :allexpense="{{ $allexpense }}"></accounting-expense-datatable>
</div>

@endsection