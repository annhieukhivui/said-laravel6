@extends('layout.master')

@section('content')

<span id="alert_action"></span>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Update JMP Expense
                    <span class="float-right mt-1">
                        <button type="button" class="btn btn-outline-info btn-sm" title="Add Show" onclick="location.href='accounting/jmp-expense'"><i class="fa fa-arrow-left"></i></button>
                    </span>
                </strong>
            </div>
            <div class="card-body" id="app">
            	<accounting-jmp-expense-form :jmpexpense="{{ $jmpexpense }}"></accounting-jmp-expense-form>
            </div>
        </div>
    </div>
</div>

@endsection

