@extends('layout.master')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body" id="app">
                    Welcome
                    @if (isset($LoginUser->username))
                        {{ $LoginUser->username }}
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>


@endsection

