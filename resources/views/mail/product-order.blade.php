<html>
    <body>
        <strong>A new marketing order has been placed by:</strong> {{$data['owner']}} for {{$data['order_for']}}<br>
        <table border='1'>
            <tr>
                <td>Contact</td>
                <td>{{ json_decode($data['contact'])->text }}</td>
            </tr>
        </table>
        <h4>Order Info</h4>
        <table border='1'>
            <tr>
                <td>Start Date</td>
                <td>{{ $data['startdate'] }}</td>
            </tr>
            <tr>
                <td>Term</td>
                <td>{{ $data['term'] }}</td>
            </tr>
            <tr>
                <td>PO #</td>
                <td>{{ $data['po_number'] }}</td>
            </tr>
            <tr>
                <td>Memo</td>
                <td>{{ $data['memo'] }}</td>
            </tr>
            <tr>
                <td>Signature</td>
                <td>{{ $data['signature_required'] }}</td>
            </tr>
            <tr>
                <td>Feright Inst</td>
                <td>{{ $data['freight_instruction'] }}</td>
            </tr>
        </table>
        <h4>Shipping Info</h4>
        <table border='1'>
            <tr>
                <td>Billing Address</td>
                <td>{{ $data['billing_address'] }}</td>
            </tr>
            <tr>
                <td>Shipping Address</td>
                <td>{{ $data['shipping_address'] }}</td>
            </tr>
            <tr>
                <td>Address Type</td>
                <td>{{ $data['address_type'] }}</td>
            </tr>
            <tr>
                <td>Shipping Carrier</td>
                <td>{{ $data['shipping_carrier'] }}</td>
            </tr>
            <tr>
                <td>Shipping Fee</td>
                <td>{{ $data['shipping_fee'] }}</td>
            </tr>
        </table>
        <h4>Product Info</h4>
        <table border='1'>
            <tr>
                <th>SKU</th>
                <th>Serial</th>
                <th>Quantity</th>
            </tr>
            @if( $data['post_form'] == 'update')
                <tr>
                    <td>{{ $data['sku'] }}</td>
                    <td>{{ $data['serial'] }}</td>
                    <td>{{ $data['quantity'] }}</td>
                </tr>
            @else
                @foreach (json_decode($data['inventory_ids']) as $inventory)
                <tr>
                    <td>{{ $inventory->sku }}</td>
                    <td>{{ $inventory->serial }}</td>
                    <td>{{ $inventory->qty }}</td>
                </tr>
                @endforeach
            @endif
            
        </table>

    </body>
</html>
        