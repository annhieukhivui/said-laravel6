@extends('layout.master')

@section('content')


<div id="app">
    <cms-product-form :brand="'{{ $brand }}'" :nsid="{{ $nsid }}"></cms-product-form>
</div>

@endsection