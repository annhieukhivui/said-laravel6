<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="csrf-token" content="{{ csrf_token() }}">
	    <title>Sellmark All Include</title>

	    <!-- base is to point to public folder-->
	    <base href="{{asset('')}}">

		<!-- general -->
	    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	    <link rel="stylesheet" href="assets/css/jquery-ui.css">
        <link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="assets/css/nav/style.css">

	</head>


<style>


</style>
	<body>
		<!--
		<div id="app">
			<left-panel></left-panel>
			<right-panel></right-panel>
		</div>
		-->
		<aside id="left-panel" class="left-panel">
		    <nav class="navbar navbar-expand-sm navbar-default">

		        <div class="navbar-header">
		            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
		                <i class="fa fa-bars"></i>
		            </button>
		            
		            <a class="navbar-brand" href=""><img src="assets/images/sellmarklogo.png" alt="Logo"></a>
		        </div>

		        <div id="main-menu" class="main-menu collapse navbar-collapse" >
		            <ul class="nav navbar-nav">
		            	{{-- <li>
		                    <a href=""><i class="menu-icon fa fa-dashboard"></i>Dashboard</a>
		                </li> --}}

						<h3 class="menu-title">CMS</h3>
		                    <li class="menu-item-has-children dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-buysellads"></i>Product</a>
		                        <ul class="sub-menu children dropdown-menu">
		                            <li><a href="cms/product/">Main</a></li>
		                        </ul>
		                    </li>		                    

		                <h3 class="menu-title">Marketing</h3>
		                    <li class="menu-item-has-children dropdown">
		                        <a href="marketing/adbuy/" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-buysellads"></i>Media Planning</a>
		                        <ul class="sub-menu children dropdown-menu">
		                        	<li><a href="marketing/media-planning/">Dashboard</a></li>
		                            <li><a href="marketing/adbuy/">Adbuy</a></li>
		                            <li><a href="marketing/adspot/">AdSpot</a></li>
		                            <li><a href="marketing/vendor/">Vendor</a></li>
		                        </ul>
		                    </li>
		                    <li class="menu-item-has-children dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Media Relations</a>
		                        <ul class="sub-menu children dropdown-menu">
		                        	<li><a href="marketing/media-relation/">Dashboard</a></li>
		                        	<li><a href="marketing/product-order/">Product Order</a></li>
		                            <li><a href="marketing/program/">T&E Program</a></li>
		                            <li><a href="marketing/placement/">Placement</a></li>
		                            <li><a href="marketing/contact/">Contact</a></li>
		                        </ul>
		                    </li>
		                    <li class="menu-item-has-children dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-first-order"></i>Inventory</a>
		                        <ul class="sub-menu children dropdown-menu">
		                        	<li><a href="marketing/inventory/">Marketing Inventory</a></li>
		                        	{{-- <li><a href="marketing/asset/">Marketing Asset</a></li> --}}
		                        </ul>
		                    </li>
		                    <li class="menu-item-has-children dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-calendar"></i>Shows & Exhibits</a>
		                        <ul class="sub-menu children dropdown-menu">
		                        	<li><a href="marketing/tradeshow/">Show</a></li>
		                        	<li><a href="marketing/occurrence/">Occurrence</a></li>
		                        </ul>
		                    </li>

		                <h3 class="menu-title">Accounting</h3>
		                    <li class="menu-item-has-children dropdown">
		                        <a href="factory/index.php" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-money"></i>Expenses</a>
		                        <ul class="sub-menu children dropdown-menu">
		                            <li><a href="accounting/expense/">Expense</a></li>
		                            <li><a href="accounting/category/">Category</a></li>
		                        </ul>
		                    </li>
		                    <li class="menu-item-has-children dropdown">
		                        <a href="factory/index.php" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-money"></i>JMP</a>
		                        <ul class="sub-menu children dropdown-menu">
		                            <li><a href="accounting/jmp-expense/">JMP Expense</a></li>
		                            <li><a href="accounting/jmp-period/">JMP Period</a></li>
		                        </ul>
		                    </li>

		                {{-- <h3 class="menu-title">Engineer</h3>
		                	<li class="menu-item-has-children dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-calendar-check-o"></i>Quality Assurance</a>
		                        <ul class="sub-menu children dropdown-menu">
		                        	<li><a href="#">Data</a></li>
		                            <li><a href="#">Defect Report</a></li>
		                            
		                        </ul>
		                    </li> --}}

		                <h3 class="menu-title">Administrator</h3>
		                    {{-- <li class="menu-item-has-children dropdown">
		                        <a href="factory/index.php" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-users"></i>Users</a>
		                        <ul class="sub-menu children dropdown-menu">
		                            <li><i class="menu-icon fa fa-id-card"></i><a href="admin/user/">User</a></li>
		                        </ul>
		                    </li> --}}
		                    <li class="menu-item-has-children dropdown">
		                    	<a href="factory/index.php" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-cogs"></i>Master List</a>
		                        <ul class="sub-menu children dropdown-menu">
		                            {{-- <li><i class="menu-icon fa fa-th-large"></i><a href="admin/masterlist/category/">Category</a></li> --}}
		                            <li><i class="menu-icon fa fa-th"></i><a href="shared/listlist/">list-list</a></li>
		                        </ul>
		                    </li>

		            </ul>
		        </div><!-- /.navbar-collapse -->
		    </nav>
		</aside><!-- /#left-panel -->

		<!-- Right Panel -->
		<div id="right-panel" class="right-panel">
		    <header id="header" class="header"> <!-- Begining of Right Panel Header-->
		        <div class="header-menu">
		            <div class="col-sm-7">
		                <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
		                <div class="page-header float-left">
		                    <div class="page-title">
		                        <ol class="breadcrumb text-left">

		                            
		                        </ol>
		                    </div>
		                </div>
		            </div>
		            
		            <div class="col-sm-5">
	                    <div class="user-area dropdown float-right">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                            <i class="menu-icon fa fa-user"></i>
	                            @if (isset($LoginUser->username))
			                        {{ $LoginUser->username }}
			                    @endif
	                        </a>
	                        <div class="user-menu dropdown-menu">
	                        	@if (isset($LoginUser->username))
		                        <a class="nav-link" href="/user/{{$LoginUser->id}}"><i class="fa fa-eye"></i> My Profile</a>
		                        @endif
		                        <a class="nav-link" href="logout"><i class="fa fa-power-off"></i> Logout</a>
		                    </div>
	                    </div>
	                </div>
		        </div>

		    </header> <!-- End of Right Panel Header-->
		    

		    <div class="content mt-3"> <!-- begin of main content section -->

		    	
