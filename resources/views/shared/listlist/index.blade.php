@extends('layout.master')

@section('content')

<div id="app">
    <shared-listlist-datatable :data="{{$data}}"></shared-listlist-datatable>
</div>

@endsection