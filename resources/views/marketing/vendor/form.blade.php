@extends('layout.master')

@section('content')

<div id="app">
	@if(isset($vendor))
		<marketing-vendor-form :vendor="{{$vendor}}"></marketing-vendor-form>
	@else
		<marketing-vendor-form></marketing-vendor-form>
	@endif
</div>


@endsection

