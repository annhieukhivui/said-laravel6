@extends('layout.master')

@section('content')


<div id="app">
    <marketing-product-order-kpi :orders="{{ $orders }}" :placements="{{ $placements}}"></marketing-product-order-kpi>
</div>

@endsection