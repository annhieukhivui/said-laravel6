@extends('layout.master')

@section('content')


<div id="app">
    <marketing-waitlist-update-queue :waitlists="{{$waitlists}}" :program_id="'{{$program_id}}'"></marketing-waitlist-update-queue>
</div>


@endsection

