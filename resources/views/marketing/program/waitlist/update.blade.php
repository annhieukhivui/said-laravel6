@extends('layout.master')

@section('content')

<div id="app">
    <marketing-waitlist-form :waitlist="{{$waitlist}}"></marketing-waitlist-form>
</div>

@endsection

