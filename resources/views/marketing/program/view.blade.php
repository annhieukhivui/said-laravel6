@extends('layout.master')

@section('content')

<div id="app">
	<marketing-program-view :program="{{$program}}"></marketing-program-view>
</div>

@endsection

