@extends('layout.master')

@section('content')

<span id="alert_action"></span>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Update Contact
                    <span class="float-right mt-1">
                        <button type="button" class="btn btn-outline-info btn-sm" onclick="location.href='marketing/contact'"><i class="fa fa-arrow-left"></i></button>
                    </span>
                </strong>
            </div>
            <div class="card-body" id="app">
                <marketing-contact-form :contact="{{$contact}}"></marketing-contact-form>
            </div>
        </div>
    </div>
</div>

@endsection