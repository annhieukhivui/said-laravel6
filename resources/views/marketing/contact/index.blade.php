@extends('layout.master')

@section('content')

<div id="app">
    <marketing-contact-datatable :allcontacts="{{ $allcontacts }}" :user="'{{ $user }}'"></marketing-contact-datatable>
</div>

@endsection