@extends('layout.master')

@section('content')

<div id="app">
	@if(isset($tradeshow))
		<marketing-tradeshow-form :tradeshow="{{ $tradeshow }}"></marketing-tradeshow-form>
	@else
		<marketing-tradeshow-form></marketing-tradeshow-form>
	@endif
</div>


@endsection

