@extends('layout.master')

@section('content')

<span id="alert_action"></span>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Update "{{ $tradeshow->show_name }}"
                    <span class="float-right mt-1">
                        <button type="button" class="btn btn-outline-info btn-sm" onclick="location.href='marketing/tradeshow'"><i class="fa fa-arrow-left"></i></button>
                    </span>
                </strong>
            </div>
            <div class="card-body" id="app">
            	<marketing-tradeshow-form :tradeshow="{{ $tradeshow }}"></marketing-tradeshow-form>
            </div>
        </div>
    </div>
</div>

@endsection


