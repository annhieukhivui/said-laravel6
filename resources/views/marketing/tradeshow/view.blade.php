@extends('layout.master')

@section('content')

<span id="alert_action"></span>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">View Tradeshow
                    <span class="float-right mt-1">
                        <button type="button" class="btn btn-outline-warning btn-sm" onclick="location.href='/marketing/tradeshow/update/{{$tradeshow->id}}'"><i class="fa fa-pencil"></i></button>
                        <button type="button" class="btn btn-outline-info btn-sm" onclick="window.history.back()"><i class="fa fa-arrow-left"></i></button>
                    </span>
                </strong>
            </div>
            <div class="card-body" id="app">
            	<marketing-tradeshow-view :tradeshow="{{ $tradeshow }}"></marketing-tradeshow-view>
            </div>
        </div>
    </div>
</div>

@endsection

