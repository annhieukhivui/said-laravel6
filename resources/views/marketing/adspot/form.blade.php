@extends('layout.master')

@section('content')

<div id="app">
	@if(isset($adspot))
		<marketing-adspot-form :adspot="{{$adspot}}"></marketing-adspot-form>
	@else
		<marketing-adspot-form></marketing-adspot-form>
	@endif
</div>


@endsection

