@extends('layout.master')

@section('content')

<div id="app">
	<marketing-asset-view :asset="{{$asset}}" :transactions="{{$transactions}}"></marketing-asset-view>
</div>

@endsection

