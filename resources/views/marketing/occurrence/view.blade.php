@extends('layout.master')

@section('content')

<span id="alert_action"></span>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">View Occurrence
                    <span class="float-right mt-1">
                        <button type="button" class="btn btn-outline-warning btn-sm" onclick="location.href='marketing/occurrence/update/{{$occurrence->id}}'"><i class="fa fa-pencil"></i></button>
                        <button type="button" class="btn btn-outline-info btn-sm" onclick="window.history.back()"><i class="fa fa-arrow-left"></i></button>
                    </span>
                </strong>
            </div>
            <div class="card-body" id="app">
            	<marketing-occurrence-view :occurrence="{{ $occurrence }}"></marketing-occurrence-view>
            </div>
        </div>
    </div>
</div>

@endsection

