@extends('layout.master')

@section('content')

<div id="app">
	@if(isset($occurrence))
		<marketing-occurrence-form :occurrence="{{ $occurrence }}"></marketing-occurrence-form>
	@else
	
		<marketing-occurrence-form></marketing-occurrence-form>
	@endif
</div>


@endsection

