@extends('layout.master')

@section('content')

<span id="alert_action"></span>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Placement
                    <span class="float-right mt-1">
                        <button type="button" class="btn btn-outline-info btn-sm" onclick="location.href='marketing/placement/add'"><i class="fa fa-plus"></i></button>
                    </span>
                </strong>
            </div>
            <div class="card-body" id="app">
                <marketing-placement-datatable :placements="{{$placements}}"></marketing-placement-datatable>
            </div>
        </div>
    </div>
</div>

@endsection