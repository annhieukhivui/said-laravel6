@extends('layout.master')

@section('content')

<div id="app">
	<marketing-inventory-view :inventory="{{$inventory}}"></marketing-inventory-view>
</div>

@endsection

