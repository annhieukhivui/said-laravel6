@extends('layout.master')

@section('content')


<div id="app">
    <marketing-inventory-datatable :data="{{ $data }}"></marketing-inventory-datatable>
</div>


@endsection