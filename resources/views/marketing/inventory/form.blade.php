@extends('layout.master')

@section('content')

<div id="app">
	@if(isset($transaction))
		<marketing-inventory-form :transaction="{{$transaction}}"></marketing-inventory-form>
	@else
		<marketing-inventory-form :user="'{{$user}}'"></marketing-inventory-form>
	@endif
</div>

@endsection

