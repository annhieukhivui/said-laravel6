@extends('layout.master')

@section('content')

<div id="app">
	@if(isset($adbuy))
		<marketing-adbuy-form :adbuy="{{$adbuy}}"></marketing-adbuy-form>
	@else
		<marketing-adbuy-form></marketing-adbuy-form>
	@endif
</div>


@endsection

