import Cookie from 'js-cookie';

const Fingerprint2 = require('fingerprintjs2');

const fingerPrintOptions = {
	excludes: {
		userAgent: true, 
		language: true,
		hardwareConcurrency: true,
		timezoneOffset: true,
		timezone: true,
		sessionStorage: true,
		localStorage: true,
		indexedDb: true,
		addBehavior: true,
		openDatabase: true,
		canvas: true,
		webgl: true,
		adBlock: true,
		plugins: true,
		hasLiedLanguages: true,
		hasLiedResolution: true,
		hasLiedOs: true,
		hasLiedBrowser: true,
		fontsFlash: true,
		fonts: true,
		enumerateDevices: true
	}
}

const encode = str => {
	return btoa(JSON.stringify(str));
}

const decode = str => {
	return JSON.parse(atob(str));
}

if (window.requestIdleCallback) {
    requestIdleCallback(function () {
        Fingerprint2.get(fingerPrintOptions, function (components) {
          Cookie.set('SLMK-DID', encode(components));
        })
    })
} else {
    setTimeout(function () {
        Fingerprint2.get(fingerPrintOptions, function (components) {
          Cookie.set('SLMK-DID', encode(components));
        })  
    }, 500)
}

window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     forceTLS: true
// });
