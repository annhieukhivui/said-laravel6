
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'

import { ToastPlugin } from 'bootstrap-vue'
Vue.use(ToastPlugin)

import { ModalPlugin } from 'bootstrap-vue'
Vue.use(ModalPlugin)

import JsonCSV from 'vue-json-csv'
Vue.component('downloadCsv', JsonCSV)

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
window.Vue = require('vue');

window.axios = require('axios');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('user-profile', require('./components/user/profile.vue').default);

Vue.component('mail-product-order-form', require('./components/mail/product-order-form.vue').default);

Vue.component('accounting-expense-datatable', require('./components/accounting/expense/datatable.vue').default);
Vue.component('accounting-expense-form', require('./components/accounting/expense/form.vue').default);
Vue.component('accounting-expense-view', require('./components/accounting/expense/view.vue').default);

Vue.component('accounting-category-index', require('./components/accounting/category/index.vue').default);

Vue.component('accounting-jmp-period-datatable', require('./components/accounting/jmp-period/datatable.vue').default);
Vue.component('accounting-jmp-period-form', require('./components/accounting/jmp-period/form.vue').default);
Vue.component('accounting-jmp-period-view', require('./components/accounting/jmp-period/view.vue').default);

Vue.component('accounting-jmp-expense-datatable', require('./components/accounting/jmp-expense/datatable.vue').default);
Vue.component('accounting-jmp-expense-form', require('./components/accounting/jmp-expense/form.vue').default);

Vue.component('marketing-dashboard-media-relation', require('./components/marketing/dashboard/media-relation.vue').default);
Vue.component('marketing-dashboard-media-planning', require('./components/marketing/dashboard/media-planning.vue').default);

Vue.component('marketing-adbuy-datatable', require('./components/marketing/adbuy/datatable.vue').default);
Vue.component('marketing-adbuy-form', require('./components/marketing/adbuy/form.vue').default);
Vue.component('marketing-adbuy-view', require('./components/marketing/adbuy/view.vue').default);

Vue.component('marketing-adspot-datatable', require('./components/marketing/adspot/datatable.vue').default);
Vue.component('marketing-adspot-form', require('./components/marketing/adspot/form.vue').default);
Vue.component('marketing-adspot-view', require('./components/marketing/adspot/view.vue').default);

Vue.component('marketing-vendor-datatable', require('./components/marketing/vendor/datatable.vue').default);
Vue.component('marketing-vendor-form', require('./components/marketing/vendor/form.vue').default);
Vue.component('marketing-vendor-view', require('./components/marketing/vendor/view.vue').default);

Vue.component('marketing-product-order-datatable', require('./components/marketing/product-order/datatable.vue').default);
Vue.component('marketing-product-order-form', require('./components/marketing/product-order/form.vue').default);
Vue.component('marketing-product-order-form-update', require('./components/marketing/product-order/form-update.vue').default);
Vue.component('marketing-product-order-view', require('./components/marketing/product-order/view.vue').default);
Vue.component('marketing-product-order-kpi', require('./components/marketing/product-order/kpi.vue').default);

Vue.component('marketing-placement-datatable', require('./components/marketing/placement/datatable.vue').default);
Vue.component('marketing-placement-form', require('./components/marketing/placement/form.vue').default);
Vue.component('marketing-placement-view', require('./components/marketing/placement/view.vue').default);

Vue.component('marketing-contact-datatable', require('./components/marketing/contact/datatable.vue').default);
Vue.component('marketing-contact-form', require('./components/marketing/contact/form.vue').default);
Vue.component('marketing-contact-view', require('./components/marketing/contact/view.vue').default);

Vue.component('marketing-inventory-datatable', require('./components/marketing/inventory/datatable.vue').default);
Vue.component('marketing-inventory-form', require('./components/marketing/inventory/form.vue').default);
Vue.component('marketing-inventory-view', require('./components/marketing/inventory/view.vue').default);

Vue.component('marketing-asset-view', require('./components/marketing/asset/view.vue').default);

Vue.component('marketing-program-datatable', require('./components/marketing/program/datatable.vue').default);
Vue.component('marketing-program-form', require('./components/marketing/program/form.vue').default);
Vue.component('marketing-program-view', require('./components/marketing/program/view.vue').default);

Vue.component('marketing-program-order-form', require('./components/marketing/program/order/form.vue').default);

Vue.component('marketing-waitlist-datatable', require('./components/marketing/program/waitlist/datatable.vue').default);
Vue.component('marketing-waitlist-form', require('./components/marketing/program/waitlist/form.vue').default);
Vue.component('marketing-waitlist-update-queue', require('./components/marketing/program/waitlist/update-queue.vue').default);

Vue.component('marketing-tradeshow-datatable', require('./components/marketing/tradeshow/datatable.vue').default);
Vue.component('marketing-tradeshow-form', require('./components/marketing/tradeshow/form.vue').default);
Vue.component('marketing-tradeshow-view', require('./components/marketing/tradeshow/view.vue').default);

Vue.component('marketing-occurrence-datatable', require('./components/marketing/occurrence/datatable.vue').default);
Vue.component('marketing-occurrence-form', require('./components/marketing/occurrence/form.vue').default);
Vue.component('marketing-occurrence-view', require('./components/marketing/occurrence/view.vue').default);

Vue.component('shared-listlist-datatable', require('./components/shared/listlist/datatable.vue').default);


Vue.component('cms-product-productlist', require('./components/cms/product/productlist.vue').default);
Vue.component('cms-product-form', require('./components/cms/product/form.vue').default);

const app = new Vue({
    el: '#app'
});