<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test', 'Admin\UserController@test');

// send slack message when task due (does not required login)
Route::get('taskdue', 'Shared\SlackController@taskdue');

Route::get('login', 'Auth\LoginController@getLogin');
Route::post('login', 'Auth\LoginController@postLogin');
Route::get('logout', 'Auth\LoginController@getLogout');

Route::group(['middleware' => 'islogin', 'namespace' => 'Auth', 'prefix' => 'auth/device'], function(){
	Route::get('email-sent', 'UserDeviceController@getEmailSent')->name('device-email-sent');
	Route::post('email-sent', 'UserDeviceController@postAuthorizeDevice');
	Route::get('{code}', 'UserDeviceController@getAuthorizeDevice')->name('device-authorize');
});

Route::group(['middleware' => ['islogin', 'device']], function(){
	Route::get('/', function () {
	    return view('home');
	});

	Route::group(['prefix'=>'user'], function(){
		Route::get('/{id}', 'User\UserController@getProfile');
		Route::post('/{id}', 'User\UserController@postProfile');
	});

	Route::group(['prefix'=>'marketing'], function(){

		Route::get('media-relation', 'Marketing\MarketingDashboard@media_relation');
		Route::get('media-planning', 'Marketing\MarketingDashboard@media_planning');

		Route::group(['prefix'=>'adbuy'], function(){
			Route::get('/', 'Marketing\AdbuyController@index');
			Route::get('add', 'Marketing\AdbuyController@getAdd');
			Route::post('add', 'Marketing\AdbuyController@postAdd');
			Route::get('update/{id}', 'Marketing\AdbuyController@getUpdate');
			Route::post('update/{id}', 'Marketing\AdbuyController@postUpdate');
			Route::get('view/{id}', 'Marketing\AdbuyController@view');
			Route::get('delete/{id}', 'Marketing\AdbuyController@delete');
			Route::get('data', 'Marketing\AdbuyController@getData');
		});

		Route::group(['prefix'=>'adspot'], function(){
			Route::get('/', 'Marketing\AdspotController@index');
			Route::get('add', 'Marketing\AdspotController@getAdd');
			Route::post('add', 'Marketing\AdspotController@postAdd');
			Route::get('update/{id}', 'Marketing\AdspotController@getUpdate');
			Route::post('update/{id}', 'Marketing\AdspotController@postUpdate');
			Route::get('view/{id}', 'Marketing\AdspotController@view');
			Route::get('delete/{id}', 'Marketing\AdspotController@delete');
			Route::get('data', 'Marketing\AdspotController@getData');
		});

		Route::group(['prefix'=>'vendor'], function(){
			Route::get('/', 'Marketing\VendorController@index');
			Route::get('add', 'Marketing\VendorController@getAdd');
			Route::post('add', 'Marketing\VendorController@postAdd');
			Route::get('update/{id}', 'Marketing\VendorController@getUpdate');
			Route::post('update/{id}', 'Marketing\VendorController@postUpdate');
			Route::get('view/{id}', 'Marketing\VendorController@view');
			Route::get('delete/{id}', 'Marketing\VendorController@delete');
			Route::get('data', 'Marketing\VendorController@getData');
		});

		Route::group(['prefix'=>'product-order'], function(){
			Route::get('/', 'Marketing\ProductOrderController@index');
			Route::get('add', 'Marketing\ProductOrderController@getAdd');
			Route::post('add', 'Marketing\ProductOrderController@postAdd');
			Route::get('update/{id}', 'Marketing\ProductOrderController@getUpdate');
			Route::post('update/{id}', 'Marketing\ProductOrderController@postUpdate');
			Route::get('view/{id}', 'Marketing\ProductOrderController@view');
			Route::get('delete/{id}', 'Marketing\ProductOrderController@delete');
			Route::get('data', 'Marketing\ProductOrderController@getData');
			Route::post('data', 'Marketing\ProductOrderController@postData');
			Route::get('kpi', 'Marketing\ProductOrderController@kpi_order_count');
			Route::get('placement', 'Marketing\ProductOrderController@kpi_placement_count');

		});

		Route::group(['prefix'=>'program'], function(){
			Route::get('/', 'Marketing\TEProgramController@index');
			Route::get('add', 'Marketing\TEProgramController@getAdd');
			Route::post('add', 'Marketing\TEProgramController@postAdd');
			Route::get('update/{id}', 'Marketing\TEProgramController@getUpdate');
			Route::post('update/{id}', 'Marketing\TEProgramController@postUpdate');
			Route::get('view/{id}', 'Marketing\TEProgramController@view');
			Route::get('delete/{id}', 'Marketing\TEProgramController@delete');
			Route::get('data', 'Marketing\TEProgramController@getData');	
			Route::post('data', 'Marketing\TEProgramController@postData');	

			Route::group(['prefix'=>'waitlist'], function(){
				Route::get('add', 'Marketing\WaitListController@getAdd');
				Route::post('add', 'Marketing\WaitListController@postAdd');
				Route::get('update/{id}', 'Marketing\WaitListController@getUpdate');
				Route::post('update/{id}', 'Marketing\WaitListController@postUpdate');
				Route::get('queue/{id}', 'Marketing\WaitListController@getUpdateQueue');
				Route::post('queue/{id}', 'Marketing\WaitListController@postUpdateQueue');
				Route::get('delete/{id}', 'Marketing\WaitListController@delete');
				Route::get('data', 'Marketing\WaitListController@getData');
				Route::post('data', 'Marketing\WaitListController@postData');
				Route::get('test', 'Marketing\WaitListController@test');
			});	

			Route::group(['prefix'=>'order'], function(){
				Route::get('add/{id}/w/{wid}', 'Marketing\ProductOrderController@getProgramAdd');
				Route::post('add/{id}/w/{wid}', 'Marketing\ProductOrderController@postProgramAdd');
			});	

		});

		Route::group(['prefix'=>'placement'], function(){
			Route::get('/', 'Marketing\PlacementController@index');
			Route::get('add', 'Marketing\PlacementController@getAdd');
			Route::post('add', 'Marketing\PlacementController@postAdd');
			Route::get('update/{id}', 'Marketing\PlacementController@getUpdate');
			Route::post('update/{id}', 'Marketing\PlacementController@postUpdate');
			Route::get('view/{id}', 'Marketing\PlacementController@view');
			Route::get('delete/{placement}', 'Marketing\PlacementController@delete');
			Route::get('data', 'Marketing\PlacementController@getData');
		});

		Route::group(['prefix'=>'contact'], function(){
			Route::get('/', 'Marketing\ContactController@index');
			Route::get('add', 'Marketing\ContactController@getAdd');
			Route::post('add', 'Marketing\ContactController@postAdd');
			Route::get('update/{id}', 'Marketing\ContactController@getUpdate');
			Route::post('update/{id}', 'Marketing\ContactController@postUpdate');
			Route::get('view/{id}', 'Marketing\ContactController@view');
			Route::post('note/{id}', 'Marketing\ContactController@addnote');
			Route::get('delete/{id}', 'Marketing\ContactController@delete');
			Route::get('data', 'Marketing\ContactController@getData');
		});

		Route::group(['prefix'=>'channel'], function(){
			Route::get('/', 'Marketing\ChannelController@index');
			Route::get('add', 'Marketing\ChannelController@getAdd');
			Route::post('add', 'Marketing\ChannelController@postAdd');
			Route::get('update/{id}', 'Marketing\ChannelController@getUpdate');
			Route::post('update/{id}', 'Marketing\ChannelController@postUpdate');
			Route::get('view/{id}', 'Marketing\ChannelController@view');
			Route::get('delete/{id}', 'Marketing\ChannelController@delete');
			Route::get('data', 'Marketing\ChannelController@getData');
		});

		Route::group(['prefix'=>'inventory'], function(){
			Route::get('/', 'Marketing\InventoryController@index');
			Route::get('add', 'Marketing\InventoryController@getAdd');
			Route::post('add', 'Marketing\InventoryController@postAdd');
			Route::get('update/{id}', 'Marketing\InventoryController@getUpdate');
			Route::post('update/{id}', 'Marketing\InventoryController@postUpdate');
			Route::get('view/{id}', 'Marketing\InventoryController@view');
			Route::get('delete/{id}', 'Marketing\InventoryController@delete');
			Route::get('data', 'Marketing\InventoryController@getData');
			Route::post('checkserial', 'Marketing\InventoryController@checkSerial');
			Route::get('fullupdate/{id}', 'Marketing\InventoryController@fullUpdate');
		});

		Route::group(['prefix'=>'asset'], function(){
			Route::get('view/{id}', 'Marketing\AssetController@view');
			Route::get('data', 'Marketing\AssetController@getData');
		});

		Route::group(['prefix'=>'tradeshow'], function(){
			Route::get('/', 'Marketing\TradeshowController@index');
			Route::get('add', 'Marketing\TradeshowController@getAdd');
			Route::post('add', 'Marketing\TradeshowController@postAdd');
			Route::get('update/{id}', 'Marketing\TradeshowController@getUpdate');
			Route::post('update/{id}', 'Marketing\TradeshowController@postUpdate');
			Route::get('view/{id}', 'Marketing\TradeshowController@view');
			Route::get('delete/{id}', 'Marketing\TradeshowController@delete');
			Route::get('data', 'Marketing\TradeshowController@getData');
		});

		Route::group(['prefix'=>'occurrence'], function(){
			Route::get('/', 'Marketing\TradeshowOccurrenceController@index');
			Route::get('add', 'Marketing\TradeshowOccurrenceController@getAdd');
			Route::post('add', 'Marketing\TradeshowOccurrenceController@postAdd');
			Route::get('update/{id}', 'Marketing\TradeshowOccurrenceController@getUpdate');
			Route::post('update/{id}', 'Marketing\TradeshowOccurrenceController@postUpdate');
			Route::get('view/{id}', 'Marketing\TradeshowOccurrenceController@view');
			Route::get('delete/{id}', 'Marketing\TradeshowOccurrenceController@delete');
			Route::get('data', 'Marketing\TradeshowOccurrenceController@getData');
		});



	});

	Route::group(['prefix'=>'accounting'], function(){
		Route::group(['prefix'=>'expense'], function(){
			Route::get('/', 'Accounting\ExpenseController@index');
			Route::get('add', 'Accounting\ExpenseController@getAdd');
			Route::post('add', 'Accounting\ExpenseController@postAdd');
			Route::get('update/{id}', 'Accounting\ExpenseController@getUpdate');
			Route::post('update/{id}', 'Accounting\ExpenseController@postUpdate');
			Route::get('view/{id}', 'Accounting\ExpenseController@view');
			Route::get('delete/{id}', 'Accounting\ExpenseController@delete');
			Route::get('data', 'Accounting\ExpenseController@getData');
		});

		Route::group(['prefix'=>'jmp-period'], function(){
			Route::get('/', 'Accounting\JMPPeriodController@index');
			Route::get('add', 'Accounting\JMPPeriodController@getAdd');
			Route::post('add', 'Accounting\JMPPeriodController@postAdd');
			Route::get('update/{id}', 'Accounting\JMPPeriodController@getUpdate');
			Route::post('update/{id}', 'Accounting\JMPPeriodController@postUpdate');
			Route::get('view/{id}', 'Accounting\JMPPeriodController@view');
			Route::get('delete/{id}', 'Accounting\JMPPeriodController@delete');
		});

		Route::group(['prefix'=>'jmp-expense'], function(){
			Route::get('/', 'Accounting\JMPExpenseController@index');
			Route::get('update/{id}', 'Accounting\JMPExpenseController@getUpdate');
			Route::post('update/{id}', 'Accounting\JMPExpenseController@postUpdate');
			Route::get('delete/{id}', 'Accounting\JMPExpenseController@delete');
		});

		Route::group(['prefix'=>'category'], function(){
			Route::get('/', 'Accounting\CategoryController@index');
			Route::get('data', 'Accounting\CategoryController@getData');
			Route::post('form', 'Accounting\CategoryController@submitForm');
			Route::get('delete/{id}', 'Accounting\CategoryController@delete');
		});

		
	});

	Route::group(['prefix'=>'admin'], function(){
		Route::group(['prefix'=>'user'], function(){
			Route::get('/', 'Admin\UserController@index');
			Route::get('data', 'Admin\UserController@getData');
		});
		
		
	});

	Route::group(['prefix'=>'shared'], function(){

		Route::group(['prefix'=>'contact'], function(){
			// Route::get('/', 'Shared\ContactController@index');
			// Route::get('add', 'Shared\ContactController@getAdd');
			// Route::post('add', 'Shared\ContactController@postAdd');
			// Route::get('update/{id}', 'Shared\ContactController@getUpdate');
			// Route::post('update/{id}', 'Shared\ContactController@postUpdate');
			// Route::get('view/{id}', 'Shared\ContactController@view');
			// Route::get('delete/{id}', 'Shared\ContactController@delete');
			Route::get('data', 'Shared\ContactController@getData');
		});
		
		Route::group(['prefix'=>'listlist'], function(){
			Route::get('/', 'Shared\ListListController@index');
			Route::get('data', 'Shared\ListListController@getData');
			Route::post('form', 'Shared\ListListController@submitForm');
			Route::get('delete/{id}', 'Shared\ListListController@delete');
		});

		

		Route::group(['prefix'=>'document'], function(){
			Route::get('/{filename}', 'Shared\DocumentController@getDocument');
			Route::get('delete/{id}', 'Shared\DocumentController@delete');
			

		});

		Route::group(['prefix'=>'brand'], function(){
			Route::get('data', 'Shared\BrandController@getData');
		});

		Route::group(['prefix'=>'product'], function(){
			Route::get('data', 'Shared\ProductController@getData');
		});

		Route::group(['prefix'=>'file'], function(){
			
		});
		
	});


	Route::group(['prefix'=>'cms'], function(){
		Route::group(['prefix'=>'product'], function(){
			Route::get('/', 'CMS\ProductController@index');
			Route::get('brand/{brand}/nsid/{nsid}', 'CMS\ProductController@getUpdate');
			Route::post('brand/{brand}/nsid/{nsid}', 'CMS\ProductController@postUpdate');
		});
	});
});